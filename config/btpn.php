<?php

return [
    'app_version'               => '1.1.6',
    'profile_upload_directory'  => 'uploads/profiles/',
    'document_upload_directory' => 'uploads/documents/',
];