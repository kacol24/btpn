<?php

return [
    'options' => [
        'progressBar'     => true,
        'timeOut'         => 5,
        'extendedtimeOut' => 10,
    ],
];
