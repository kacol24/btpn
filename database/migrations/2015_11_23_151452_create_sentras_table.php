<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSentrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sentras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mms_id')->unsigned();
            $table->string('name', 50);
            $table->string('email', 50);
            $table->string('phone', 20)->nullable();
            $table->string('address')->nullable();
            $table->timestamps();

            $table->foreign('mms_id')
                  ->references('id')
                  ->on('mms')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sentras');
    }
}
