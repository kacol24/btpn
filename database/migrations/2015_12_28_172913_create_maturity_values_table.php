<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaturityValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maturity_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aspect_id')->unsigned();
            $table->foreign('aspect_id')->references('id')->on('aspects')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('level_id')->unsigned();
            $table->foreign('level_id')->references('id')->on('levels')->onUpdate('cascade')->onDelete('cascade');
            $table->string('value');
            $table->timestamps();

            $table->unique(['aspect_id', 'level_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('maturity_values');
    }
}
