<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::create('communities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('facilitator_id')->unsigned()->nullable()->default(null);
            $table->integer('business_field_id')->unsigned();
            $table->integer('mms_id')->unsigned();
            $table->string('name', 30);
            $table->date('activation_date');
            $table->timestamp('deactivation_date')->nullable()->default(null);
            $table->string('deactivation_reason')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('facilitator_id')
                  ->references('id')
                  ->on('facilitators')
                  ->onDelete('cascade');
            $table->foreign('business_field_id')
                  ->references('id')
                  ->on('business_fields')
                  ->onDelete('cascade');
            $table->foreign('mms_id')
                  ->references('id')
                  ->on('mms')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('communities');
        Schema::drop('business_fields');
    }
}
