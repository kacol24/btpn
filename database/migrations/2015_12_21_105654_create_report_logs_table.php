<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('facilitator_id')->unsigned();
            $table->integer('community_id')->unsigned();
            $table->date('period');
            $table->boolean('is_verified')->default(false);
            $table->timestamp('verified_at')->nullable()->default(null);
            $table->timestamp('declined_at')->nullable()->default(null);
            $table->string('declined_reason')->nullable()->default(null);
            $table->integer('report_id');
            $table->string('report_type');
            $table->timestamps();

            $table->foreign('facilitator_id')
                  ->references('id')
                  ->on('facilitators')
                  ->onDelete('cascade');
            $table->foreign('community_id')
                  ->references('id')
                  ->on('communities')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_logs');
    }
}
