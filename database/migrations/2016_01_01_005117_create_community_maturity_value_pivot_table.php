<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommunityMaturityValuePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_maturity_value', function (Blueprint $table) {
            $table->integer('community_id')->unsigned()->index();
            $table->foreign('community_id')->references('id')->on('communities');
            $table->integer('maturity_value_id')->unsigned()->index();
            $table->foreign('maturity_value_id')->references('id')->on('maturity_values');
            $table->date('period');
            $table->primary(['community_id', 'maturity_value_id', 'period'], 'community_maturity_period_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('community_maturity_value');
    }
}
