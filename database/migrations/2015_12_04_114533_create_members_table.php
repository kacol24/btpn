<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sentra_id')->unsigned();
            $table->integer('community_id')->unsigned();
            $table->string('account_number')->unique()->index();
            $table->string('name', 30);
            $table->string('phone', 20);
            $table->string('address');
            $table->boolean('is_active')->default(true);
            $table->timestamp('inactive_at')->nullable()->default(null);
            $table->boolean('is_leader')->default(false);
            $table->timestamps();

            $table->foreign('sentra_id')
                  ->references('id')
                  ->on('sentras')
                  ->onDelete('cascade');
            $table->foreign('community_id')
                  ->references('id')
                  ->on('communities')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('members');
    }
}
