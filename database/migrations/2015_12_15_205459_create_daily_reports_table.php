<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDailyReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activity_type_id')->unsigned();
            $table->string('activity_name', 50);
            $table->string('location', 50);
            $table->time('start_time');
            $table->time('end_time');
            $table->text('achievements');
            $table->text('problems');
            $table->text('plans');
            $table->string('f2_document_url');
            $table->string('f3_document_url')->nullable()->default(null);
            $table->string('supporting_document_url')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('activity_type_id')
                  ->references('id')
                  ->on('activity_types')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('daily_reports');
    }
}
