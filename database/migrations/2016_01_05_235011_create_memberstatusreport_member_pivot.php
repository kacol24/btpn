<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMemberstatusreportMemberPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_member_status_report', function (Blueprint $table) {
            $table->integer('member_status_report_id')->unsigned()->index();
            $table->foreign('member_status_report_id')->references('id')->on('member_status_reports')->onDelete('cascade');
            $table->integer('member_id')->unsigned()->index();
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
            $table->boolean('is_active')->default(true);
            $table->primary(['member_status_report_id', 'member_id'], 'member_msr_member_id_msr_id_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('member_member_status_report');
    }
}
