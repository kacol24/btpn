<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*
 * User factory
 */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->username,
        'password' => bcrypt(str_random(10)),
        'name'     => $faker->name,
        'phone'    => $faker->phoneNumber,
        'email'    => $faker->email,
    ];
});

/*
 * Roles factory
 */
$factory->defineAs(App\Models\Role::class, 'headquarter', function () {
    return [
        'name'         => 'headquarter',
        'display_name' => 'DTK Headquarter',
        'description'  => 'Master User from BTPN Headquarter',
    ];
});
$factory->defineAs(App\Models\Role::class, 'mms', function () {
    return [
        'name'         => 'mms',
        'display_name' => 'MMS',
        'description'  => 'MMS User',
    ];
});
$factory->defineAs(App\Models\Role::class, 'facilitator', function () {
    return [
        'name'         => 'facilitator',
        'display_name' => 'Facilitator',
        'description'  => 'Facilitator User',
    ];
});

/*
 * City factory
 */
$factory->define(App\Models\City::class, function (Faker\Generator $faker) {
    return [
        'username'    => $faker->city,
        'description' => $faker->sentence,
    ];
});

/*
 * Sentra factory
 */
$factory->define(App\Models\Sentra::class, function (Faker\Generator $faker) {
    $mms = factory(App\Models\User::class)->create()->each(function ($user) {
        $user->attachRole(factory(App\Models\Role::class, 'mms')->create());
    });

    return [
        'mms_id'  => $mms->id,
        'name'    => $faker->name,
        'email'   => $faker->email,
        'phone'   => $faker->phoneNumber,
        'address' => $faker->address,
    ];
});

/*
 * Business Fields factory
 */
$factory->define(App\Models\BusinessField::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->sentence(3),
        'description' => $faker->sentence,
    ];
});

/*
 * Community factory
 */
$factory->define(App\Models\Community::class, function (Faker\Generator $faker) {
    $facilitator = factory(App\Models\User::class)->create()->each(function ($user) {
        $user->attachRole(factory(App\Models\Role::class, 'facilitator')->create());
    });
    $business_field = factory(App\Models\BusinessField::class)->create();
    $mms = factory(App\Models\User::class)->create()->each(function ($user) {
        $user->attachRole(factory(App\Models\Role::class, 'mms')->create());
    });

    return [
        'facilitator_id'    => $facilitator->id,
        'business_field_id' => $business_field->id,
        'mms_id'            => $mms->id,
        'name'              => $faker->name,
        'activation_date'   => $faker->date('Y-m'),
    ];
});

/*
 * Activity Types
 */
$factory->define(App\Models\ActivityType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

/*
 * Member factory
 */
$factory->define(App\Models\Member::class, function (Faker\Generator $faker) {
    //$sentra = factory(App\Models\Sentra::class)->create();
    //$community = factory(App\Models\Community::class)->create();

    return [
        'sentra_id'      => 1,
        'community_id'   => 1,
        'account_number' => $faker->creditCardNumber,
        'name'           => $faker->name,
        'phone'          => $faker->phoneNumber,
        'address'        => $faker->address,
        'is_active'      => true,
        'is_leader'      => true,
    ];
});