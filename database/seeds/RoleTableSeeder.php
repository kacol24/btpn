<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $headquarter = new Role();
        $headquarter->name = 'headquarter';
        $headquarter->display_name = 'DTK Headquarter';
        $headquarter->description = 'Master User from BTPN Headquarter';
        $headquarter->save();

        $mms = new Role();
        $mms->name = 'mms';
        $mms->display_name = 'MMS';
        $mms->description = 'MMS User';
        $mms->save();

        $facilitator = new Role();
        $facilitator->name = 'facilitator';
        $facilitator->display_name = 'Facilitator';
        $facilitator->description = 'Facilitator User';
        $facilitator->save();
    }
}
