<?php

use App\Models\Sentra;
use Illuminate\Database\Seeder;

class SentraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sentras')->truncate();

        // pakis
        Sentra::create([
            'mms_id'  => 1,
            'name'    => 'Wancar Boro Bugis',
            'email'   => 'sentramail@btpn.dtk',
            'phone'   => '0987654321',
            'address' => 'Specific sentra address',
        ]);
        Sentra::create([
            'mms_id'  => 1,
            'name'    => 'Sukoanyar New',
            'email'   => 'sentramail@btpn.dtk',
            'phone'   => '0987654321',
            'address' => 'Specific sentra address',
        ]);
        Sentra::create([
            'mms_id'  => 1,
            'name'    => 'Istimewa Plalar',
            'email'   => 'sentramail@btpn.dtk',
            'phone'   => '0987654321',
            'address' => 'Specific sentra address',
        ]);
        Sentra::create([
            'mms_id'  => 1,
            'name'    => 'Sukoanyar Plalar',
            'email'   => 'sentramail@btpn.dtk',
            'phone'   => '0987654321',
            'address' => 'Specific sentra address',
        ]);

        // sukun
        Sentra::create([
            'mms_id'  => 2,
            'name'    => 'Anggrek Gadang 17',
            'email'   => 'sentramail@btpn.dtk',
            'phone'   => '0987654321',
            'address' => 'Specific sentra address',
        ]);
        Sentra::create([
            'mms_id'  => 2,
            'name'    => 'Mugi Lancar Gadang 12',
            'email'   => 'sentramail@btpn.dtk',
            'phone'   => '0987654321',
            'address' => 'Specific sentra address',
        ]);
        Sentra::create([
            'mms_id'  => 2,
            'name'    => 'Setia Kawan Ciptomulyo 6',
            'email'   => 'sentramail@btpn.dtk',
            'phone'   => '0987654321',
            'address' => 'Specific sentra address',
        ]);
        Sentra::create([
            'mms_id'  => 2,
            'name'    => 'Asparaga - Pisang Candi',
            'email'   => 'sentramail@btpn.dtk',
            'phone'   => '0987654321',
            'address' => 'Specific sentra address',
        ]);
        Sentra::create([
            'mms_id'  => 2,
            'name'    => 'Sukun Sejahtera',
            'email'   => 'sentramail@btpn.dtk',
            'phone'   => '0987654321',
            'address' => 'Specific sentra address',
        ]);

        // tengah tani
        Sentra::create([
            'mms_id'  => 3,
            'name'    => 'Si Untung',
            'email'   => 'sentramail@btpn.dtk',
            'phone'   => '0987654321',
            'address' => 'Specific sentra address',
        ]);

        // plumbon
        Sentra::create([
            'mms_id'  => 4,
            'name'    => 'Bodesari',
            'email'   => 'sentramail@btpn.dtk',
            'phone'   => '0987654321',
            'address' => 'Specific sentra address',
        ]);
    }
}
