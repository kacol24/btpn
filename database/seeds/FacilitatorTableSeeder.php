<?php

use App\Models\City;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class FacilitatorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facilitators')->truncate();

        $user = User::create([
            'username' => 'facilitatoruser1',
            'password' => 'password',
            'name'     => 'Anastasia Laksita',
            'Phone'    => '027 5395 428	',
            'email'    => 'qMulyani@Oktaviani.asia	',
        ]);

        $facilitatorRole = Role::whereName('facilitator')->first();
        $user->attachRole($facilitatorRole);

        $city = City::first();
        $user->facilitator()->create([
            'city_id' => $city->id,
            'dob'     => '1987-09-30',
            'gender'  => 'f',
            'address' => 'Jr. Bahagia No. 833, Langsa 87096, Aceh',
        ]);
    }
}
