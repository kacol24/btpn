<?php

use App\Models\City;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class MmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mmsRole = Role::whereName('mms')->first();
        $city = City::first();

        // pakis
        $pakis = User::create([
            'username' => 'mmsuser1',
            'password' => 'password',
            'name'     => 'Pakis',
            'Phone'    => '(+62) 903 4321 8623',
            'email'    => 'pakismms@btpn.com',
        ]);
        $pakis->attachRole($mmsRole);
        $pakis->mms()->create([
            'id'      => 1,
            'city_id' => $city->id,
            'address' => 'MMS Pakis Address',
        ]);

        // sukun
        $sukun = User::create([
            'username' => 'mmsuser2',
            'password' => 'password',
            'name'     => 'Sukun',
            'Phone'    => '(+62) 903 4321 8623',
            'email'    => 'sukunmms@btpn.com',
        ]);
        $sukun->attachRole($mmsRole);
        $sukun->mms()->create([
            'id'      => 2,
            'city_id' => $city->id,
            'address' => 'MMS Sukun Address',
        ]);

        $city = City::orderBy('id', 'desc')->first();

        // plumbon
        $plumbon = User::create([
            'username' => 'mmsuser3',
            'password' => 'password',
            'name'     => 'Plumbon',
            'Phone'    => '(+62) 903 4321 8623',
            'email'    => 'plumbonmms@btpn.com',
        ]);
        $plumbon->attachRole($mmsRole);
        $plumbon->mms()->create([
            'id'      => 3,
            'city_id' => $city->id,
            'address' => 'MMS Plumbon Address',
        ]);

        // tengah tani
        $tengahTani = User::create([
            'username' => 'mmsuser4',
            'password' => 'password',
            'name'     => 'Tengah Tani',
            'Phone'    => '(+62) 903 4321 8623',
            'email'    => 'tengahtanimms@btpn.com',
        ]);
        $tengahTani->attachRole($mmsRole);
        $tengahTani->mms()->create([
            'id'      => 4,
            'city_id' => $city->id,
            'address' => 'MMS Tengah Tani Address',
        ]);
    }
}
