<?php

use App\Models\ActivityType;
use Illuminate\Database\Seeder;

class ActivityTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $meeting = new ActivityType();
        $meeting->name = 'Meeting';
        $meeting->save();

        $visit = new ActivityType();
        $visit->name = 'Visit';
        $visit->save();

        $training = new ActivityType();
        $training->name = 'Training';
        $training->save();

        $exhibition = new ActivityType();
        $exhibition->name = 'Exhibition';
        $exhibition->save();
    }
}
