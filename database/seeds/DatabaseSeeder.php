<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::statement("SET foreign_key_checks=0");

        $this->call(RoleTableSeeder::class);
        $this->call(BusinessFieldTableSeeder::class);
        $this->call(ActivityTypeTableSeeder::class);
        $this->call(HeadquarterUserSeeder::class);

        if (env('APP_ENV') == 'local') {
            $this->call(CityTableSeeder::class);
            $this->call(MmsTableSeeder::class);
            $this->call(SentraTableSeeder::class);
            $this->call(FacilitatorTableSeeder::class);
            $this->call(CommunityTableSeeder::class);
            $this->call(MemberTableSeeder::class);
        }

        Model::reguard();
    }
}
