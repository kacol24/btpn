<?php

use App\Models\City;
use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->truncate();

        City::create([
            'name'        => 'Surabaya',
            'description' => 'Some description of the city',
        ]);
        City::create([
            'name'        => 'Malang',
            'description' => 'Malang covers businesses such as convection and sewing.',
        ]);
        City::create([
            'name'        => 'Cirebon',
            'description' => '	Cirebon covers businesses such as rattan and batik.',
        ]);
    }
}
