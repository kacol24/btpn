<?php

use App\Models\Community;
use App\Models\Member;
use Illuminate\Database\Seeder;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('members')->truncate();

        $community = Community::first();

        $member = new Member();
        $member->sentra_id = 1;
        $member->community_id = $community->id;
        $member->account_number = '9781171887119';
        $member->name = 'Ana Wahyuni S.Sos';
        $member->phone = '0648 3720 254';
        $member->address = 'Ki. Jaksa No. 324, Bau-Bau 61050, Riau';
        $member->is_active = true;
        $member->is_leader = true;
        $member->save();
    }
}
