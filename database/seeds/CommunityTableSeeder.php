<?php

use App\Models\BusinessField;
use App\Models\Community;
use App\Models\Facilitator;
use App\Models\Mms;
use Illuminate\Database\Seeder;

class CommunityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('communities')->truncate();

        $business_field = BusinessField::first();
        $mms = Mms::first();
        $facilitator = Facilitator::first();

        Community::create([
            'facilitator_id'    => $facilitator->id,
            'business_field_id' => $business_field->id,
            'mms_id'            => $mms->id,
            'name'              => 'Bintang Purnama',
            'activation_date'   => '2015-01',
        ]);

    }
}
