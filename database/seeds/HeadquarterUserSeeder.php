<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class HeadquarterUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'username' => env('ADMIN_USERNAME'),
            'password' => env('ADMIN_PASSWORD'),
            'name'     => env('ADMIN_NAME'),
            'phone'    => env('ADMIN_PHONE'),
            'email'    => env('ADMIN_EMAIL'),
        ]);

        $headquarterRole = Role::whereName('headquarter')->first();
        $user->attachRole($headquarterRole);
    }
}
