<?php

use App\Models\BusinessField;
use Illuminate\Database\Seeder;

class BusinessFieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('business_fields')->truncate();

        BusinessField::create([
            'name'        => 'Food and Beverage',
            'description' => 'All food and beverages business fields',
        ]);
    }
}
