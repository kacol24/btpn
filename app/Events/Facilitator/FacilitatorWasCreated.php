<?php

namespace App\Events\Facilitator;

use App\Events\Event;
use App\Models\Facilitator;
use Illuminate\Queue\SerializesModels;

class FacilitatorWasCreated extends Event
{
    use SerializesModels;

    public $id;
    public $name;

    public function __construct(Facilitator $facilitator)
    {
        $this->id = $facilitator->id;
        $this->name = $facilitator->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
