<?php

namespace App\Events\Facilitator;

use App\Events\Event;
use App\Models\Facilitator;
use Illuminate\Queue\SerializesModels;

class FacilitatorWasDeactivated extends Event
{
    use SerializesModels;
    public $name;
    public $id;

    /**
     * Create a new event instance.
     *
     * @param Facilitator $facilitator
     */
    public function __construct(Facilitator $facilitator)
    {
        $this->id = $facilitator->id;
        $this->name = $facilitator->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
