<?php

namespace App\Events\Mms;

use App\Events\Event;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class MmsWasCreated extends Event
{
    use SerializesModels;

    public $id;
    public $name;

    public function __construct(User $mms)
    {
        $this->id = $mms->id;
        $this->name = $mms->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
