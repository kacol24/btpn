<?php

namespace App\Events\Mms;

use App\Events\Event;
use App\Models\Mms;
use Illuminate\Queue\SerializesModels;

class MmsWasUpdated extends Event
{
    use SerializesModels;

    public $id;
    public $name;

    public function __construct(Mms $mms)
    {
        $this->id = $mms->id;
        $this->name = $mms->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
