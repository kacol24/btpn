<?php

namespace App\Events\Reports;

use App\Events\Event;
use App\Models\ProgramUpdateReport;
use Illuminate\Queue\SerializesModels;

class ProgramUpdateReportWasUpdated extends Event
{
    use SerializesModels;
    public $id;

    /**
     * Create a new event instance.
     *
     * @param ProgramUpdateReport $programUpdateReport
     */
    public function __construct(ProgramUpdateReport $programUpdateReport)
    {
        $this->id = $programUpdateReport->id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
