<?php

namespace App\Events\Reports;

use App\Events\Event;
use App\Models\DailyReport;
use Illuminate\Queue\SerializesModels;

class DailyReportWasUpdated extends Event
{
    use SerializesModels;
    public $id;

    /**
     * Create a new event instance.
     *
     * @param DailyReport $dailyReport
     */
    public function __construct(DailyReport $dailyReport)
    {
        $this->id = $dailyReport->id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
