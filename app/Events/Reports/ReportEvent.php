<?php

namespace App\Events\Reports;

use App\Events\Event;
use App\Models\ReportLog;

abstract class ReportEvent extends Event
{
    protected function getReportType(ReportLog $report)
    {
        if ($report->report instanceof \App\Models\DailyReport) {
            $reportType = 'Daily Report';
        } else if ($report->report instanceof \App\Models\ProgramUpdateReport) {
            $reportType = 'Program Update Report';
        } else if ($report->report instanceof \App\Models\MemberStatusReport) {
            $reportType = 'Member Status Report';
        }

        return $reportType;
    }
}