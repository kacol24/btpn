<?php

namespace App\Events\Reports;

use App\Events\Event;
use App\Models\ProgramUpdateReport;
use Illuminate\Queue\SerializesModels;

class ProgramUpdateReportWasCreated extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @param ProgramUpdateReport $programUpdate
     */
    public function __construct(ProgramUpdateReport $programUpdate)
    {
        $this->id = $programUpdate->id;
        $this->community = $programUpdate->meta->community->name;
        $this->facilitator = $programUpdate->meta->facilitator->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
