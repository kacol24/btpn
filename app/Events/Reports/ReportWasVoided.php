<?php

namespace App\Events\Reports;

use App\Models\ReportLog;
use Illuminate\Queue\SerializesModels;

class ReportWasVoided extends ReportEvent
{
    use SerializesModels;
    public $reportType;
    public $id;

    /**
     * Create a new event instance.
     *
     * @param ReportLog $reportLog
     */
    public function __construct(ReportLog $reportLog)
    {
        $this->id = $reportLog->report->id;
        $this->reportType = $this->getReportType($reportLog);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
