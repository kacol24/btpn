<?php

namespace App\Events\Reports;

use App\Models\ReportLog;
use Illuminate\Queue\SerializesModels;

class ReportWasVerified extends ReportEvent
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @param ReportLog $report
     */
    public function __construct(ReportLog $report)
    {
        $this->id = $report->report->id;
        $this->reportType = $this->getReportType($report);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
