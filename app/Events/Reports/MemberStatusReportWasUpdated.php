<?php

namespace App\Events\Reports;

use App\Events\Event;
use App\Models\MemberStatusReport;
use Illuminate\Queue\SerializesModels;

class MemberStatusReportWasUpdated extends Event
{
    use SerializesModels;
    public $id;

    /**
     * Create a new event instance.
     *
     * @param MemberStatusReport $memberStatusReport
     */
    public function __construct(MemberStatusReport $memberStatusReport)
    {
        $this->id = $memberStatusReport->id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
