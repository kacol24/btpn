<?php

namespace App\Events\Reports;

use App\Events\Event;
use App\Models\MemberStatusReport;
use Illuminate\Queue\SerializesModels;

class MemberStatusReportWasCreated extends Event
{
    use SerializesModels;
    public $id;
    public $community;
    public $facilitator;

    /**
     * Create a new event instance.
     *
     * @param MemberStatusReport $memberStatus
     */
    public function __construct(MemberStatusReport $memberStatus)
    {
        $this->id = $memberStatus->id;
        $this->community = $memberStatus->meta->community->name;
        $this->facilitator = $memberStatus->meta->facilitator->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
