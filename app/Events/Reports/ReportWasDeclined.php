<?php

namespace App\Events\Reports;

use App\Models\ReportLog;
use Illuminate\Queue\SerializesModels;

class ReportWasDeclined extends ReportEvent
{
    use SerializesModels;
    public $reportType;
    public $reason;
    public $id;

    /**
     * Create a new event instance.
     *
     * @param ReportLog $report
     * @param           $reason
     */
    public function __construct(ReportLog $report, $reason)
    {
        $this->id = $report->report->id;
        $this->reason = $reason;
        $this->reportType = $this->getReportType($report);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
