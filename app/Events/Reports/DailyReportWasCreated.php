<?php

namespace App\Events\Reports;

use App\Events\Event;
use App\Models\DailyReport;
use Illuminate\Queue\SerializesModels;

/**
 * Class DailyReportWasCreated
 * @package App\Events\Reports
 */
class DailyReportWasCreated extends Event
{
    use SerializesModels;

    /**
     * @var mixed
     */
    public $id;
    /**
     * @var
     */
    public $community;
    /**
     * @var
     */
    public $facilitator;

    /**
     * Create a new event instance.
     *
     * @param DailyReport $dailyReport
     */
    public function __construct(DailyReport $dailyReport)
    {
        $this->id = $dailyReport->id;
        $this->community = $dailyReport->meta->community->name;
        $this->facilitator = $dailyReport->meta->facilitator->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
