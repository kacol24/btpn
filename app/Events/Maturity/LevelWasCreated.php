<?php

namespace App\Events\Maturity;

use App\Events\Event;
use App\Models\Level;
use Illuminate\Queue\SerializesModels;

class LevelWasCreated extends Event
{
    use SerializesModels;

    public $name;
    public $id;

    /**
     * Create a new event instance.
     *
     * @param Level $level
     */
    public function __construct(Level $level)
    {
        $this->id = $level->id;
        $this->name = $level->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
