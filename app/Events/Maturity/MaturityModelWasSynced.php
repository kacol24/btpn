<?php

namespace App\Events\Maturity;

use App\Events\Event;
use App\Models\Community;
use Illuminate\Queue\SerializesModels;

class MaturityModelWasSynced extends Event
{
    use SerializesModels;
    public $name;
    public $id;

    /**
     * Create a new event instance.
     *
     * @param Community $community
     */
    public function __construct(Community $community)
    {
        $this->id = $community->id;
        $this->name = $community->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
