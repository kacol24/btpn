<?php

namespace App\Events\Maturity;

use App\Events\Event;
use App\Models\MaturityValue;
use Illuminate\Queue\SerializesModels;

class MaturityValueWasUpdated extends Event
{
    use SerializesModels;
    public $name;
    public $id;

    /**
     * Create a new event instance.
     *
     * @param MaturityValue $value
     */
    public function __construct(MaturityValue $value)
    {
        $this->id = $value->id;
        $this->name = $value->value;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
