<?php

namespace App\Events\Maturity;

use App\Events\Event;
use App\Models\Aspect;
use Illuminate\Queue\SerializesModels;

class AspectWasCreated extends Event
{
    use SerializesModels;

    public $id;
    public $name;

    /**
     * Create a new event instance.
     *
     * @param Aspect $aspect
     */
    public function __construct(Aspect $aspect)
    {
        $this->id = $aspect->id;
        $this->name = $aspect->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
