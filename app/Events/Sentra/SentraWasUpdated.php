<?php

namespace App\Events\Sentra;

use App\Events\Event;
use App\Models\Sentra;
use Illuminate\Queue\SerializesModels;

class SentraWasUpdated extends Event
{
    use SerializesModels;

    public $id;
    public $name;

    public function __construct(Sentra $sentra)
    {
        $this->id = $sentra->id;
        $this->name = $sentra->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
