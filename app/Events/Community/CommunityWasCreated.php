<?php

namespace App\Events\Community;

use App\Events\Event;
use App\Models\Community;
use Illuminate\Queue\SerializesModels;

class CommunityWasCreated extends Event
{
    use SerializesModels;

    public $id;
    public $name;

    /**
     * Create a new event instance.
     *
     * @param Community $community
     */
    public function __construct(Community $community)
    {
        $this->id = $community->id;
        $this->name = $community->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
