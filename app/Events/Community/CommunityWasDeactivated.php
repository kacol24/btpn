<?php

namespace App\Events\Community;

use App\Events\Event;
use App\Models\Community;
use Illuminate\Queue\SerializesModels;

class CommunityWasDeactivated extends Event
{
    use SerializesModels;

    public $id;
    public $name;

    public function __construct(Community $community)
    {
        $this->id = $community->id;
        $this->name = $community->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
