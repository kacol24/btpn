<?php

namespace App\Events\City;

use App\Events\Event;
use App\Models\City;
use Illuminate\Queue\SerializesModels;

class CityWasCreated extends Event
{
    use SerializesModels;

    public $id;
    public $name;

    public function __construct(City $city)
    {
        $this->id = $city->id;
        $this->name = $city->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
