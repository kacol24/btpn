<?php

namespace App\Events\Member;

use App\Events\Event;
use App\Models\Member;
use Illuminate\Queue\SerializesModels;

class MemberWasCreated extends Event
{
    use SerializesModels;

    public $id;
    public $name;

    public function __construct(Member $member)
    {
        $this->id = $member->id;
        $this->name = $member->name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
