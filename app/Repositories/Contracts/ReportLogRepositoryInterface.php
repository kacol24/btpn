<?php

namespace App\Repositories\Contracts;

    /**
     * Interface ReportLogRepositoryInterface
     * @package App\Repositories\Contracts
     */
/**
 * Interface ReportLogRepositoryInterface
 * @package App\Repositories\Contracts
 */
/**
 * Interface ReportLogRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface ReportLogRepositoryInterface
{
    /**
     * @param $id
     * @param $reason
     *
     * @return mixed
     */
    public function decline($id, $reason);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function verify($id);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id);

    /**
     * @return mixed
     */
    public function getPending();

    /**
     * @param null $facilitator_id
     *
     * @return mixed
     */
    public function getDeclined($facilitator_id = null);

    /**
     * @param      $period
     * @param bool $yearOnly
     * @param bool $verified
     *
     * @return mixed
     */
    public function getDailyReportsWithinPeriod($period, $yearOnly = false, $verified = false);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function void($id);

    /**
     * @param $facilitator_id
     *
     * @return mixed
     */
    public function getWhereFacilitator($facilitator_id);

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param      $community_id
     * @param null $period
     *
     * @return mixed
     */
    public function getMemberStatusReport($community_id, $period = null);

    /**
     * @param $community_id
     *
     * @return mixed
     */
    public function getDailyReportsOfCommunity($community_id);

    /**
     * @param null $period
     * @param bool $yearOnly
     *
     * @return mixed
     */
    public function getAllMonthlyMemberStatusReports($period = null, $yearOnly = false);
}