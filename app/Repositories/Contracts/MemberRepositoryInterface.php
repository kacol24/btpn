<?php

namespace App\Repositories\Contracts;

/**
 * Interface MemberRepositoryInterface
 * @package Repositories\Contracts
 */
interface MemberRepositoryInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param $member_id
     *
     * @return mixed
     */
    public function find($member_id);

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param       $community_id
     * @param array $ids
     *
     * @return mixed
     */
    public function deactivate($community_id, array $ids);

    /**
     * @param $community_id
     *
     * @return mixed
     */
    public function getActiveMembers($community_id);

    /**
     * @param $community_id
     *
     * @return mixed
     */
    public function getMembers($community_id);
}