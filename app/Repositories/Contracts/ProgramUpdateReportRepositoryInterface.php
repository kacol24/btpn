<?php

namespace App\Repositories\Contracts;

/**
 * Interface ProgramUpdateReportRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface ProgramUpdateReportRepositoryInterface
{
    /**
     * @param bool $verified
     *
     * @return mixed
     */
    public function getAll($verified = false);

    /**
     * @param      $community_id
     * @param      $period
     * @param null $activity_type_id
     * @param bool $verified
     *
     * @return mixed
     */
    public function getDailyReports($community_id, $period, $activity_type_id = null, $verified = false);

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data);
}