<?php

namespace App\Repositories\Contracts;

/**
 * Interface DailyReportRepositoryInterface
 * @package App\Repositories\Contracts
 */
/**
 * Interface DailyReportRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface DailyReportRepositoryInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data);
}