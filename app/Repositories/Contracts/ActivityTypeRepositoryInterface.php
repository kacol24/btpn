<?php

namespace App\Repositories\Contracts;

/**
 * Interface ActivityTypeRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface ActivityTypeRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getDropdown();

    /**
     * @return mixed
     */
    public function getAll();
}