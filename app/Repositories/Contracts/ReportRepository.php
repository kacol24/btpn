<?php
/******************************************************************************
 * Copyright (c) 2016 Kevin Chandra.                                          *
 *                                                                            *
 * This file is part of BTPN - Daya Tumbuh Komunitas.                         *
 *                                                                            *
 * For the full copyright and license information, please view the LICENSE    *
 * file that was distributed with this source code.                           *
 ******************************************************************************/

namespace App\Repositories\Contracts;

use Carbon\Carbon;

/**
 * Class ReportRepository
 * @package App\Repositories\Contracts
 */
abstract class ReportRepository
{
    /**
     *
     */
    const DOCUMENT_FILENAME_FORMAT = "{REPORT_TYPE}{REPORT_ID}_{DOCUMENT_TYPE}_{SUBMIT_DATE}.{FILE_EXTENSION}"; // PUR123456_F1_2016-12-30--13-59-30.pdf
    const DOCUMENT_FILENAME_DATEFORMAT = 'Y-m-d--H-i-s';
    const REPORT_PENDING_STATUS = [ // TODO experimental, PHP >= 5.6 only
        'is_verified'     => false,
        'declined_at'     => null,
        'declined_reason' => null,
    ];

    /**
     * @param $reportType
     * @param $reportId
     * @param $documentType
     * @param $community_id
     * @param $file
     *
     * @return string
     */
    protected function uploadFile($reportType, $reportId, $documentType, $community_id, $file)
    {
        $uploadDirectory = public_path(config('btpn.document_upload_directory')) . $community_id;
        $fileName = $this->formatFileName($reportType, $reportId, $documentType, $file);

        $file->move($uploadDirectory, $fileName);

        return $fileName;
    }

    /**
     * @param $reportType
     * @param $reportId
     * @param $documentType
     * @param $file
     *
     * @return string
     */
    private function formatFileName($reportType, $reportId, $documentType, $file)
    {
        $replace = [
            '{REPORT_TYPE}'    => $reportType,
            '{REPORT_ID}'      => str_pad($reportId, 6, 0, STR_PAD_LEFT),
            '{DOCUMENT_TYPE}'  => $documentType,
            '{SUBMIT_DATE}'    => Carbon::now()->format(self::DOCUMENT_FILENAME_DATEFORMAT),
            '{FILE_EXTENSION}' => $file->getClientOriginalExtension(),
        ];

        return str_replace(array_keys($replace), array_values($replace), self::DOCUMENT_FILENAME_FORMAT);
    }
}
