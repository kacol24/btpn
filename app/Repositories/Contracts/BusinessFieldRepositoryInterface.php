<?php

namespace App\Repositories\Contracts;


/**
 * Interface BusinessFieldRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface BusinessFieldRepositoryInterface
{
    /**
     * @param $value
     * @param $key
     *
     * @return mixed
     */
    public function lists($value, $key);

    /**
     * @return mixed
     */
    public function getDropdown();

}