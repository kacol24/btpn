<?php

namespace App\Repositories\Contracts;

/**
 * Interface MemberStatusReportRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface MemberStatusReportRepositoryInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id);

    /**
     * @param $community_id
     * @param $period
     *
     * @return mixed
     */
    public function getMembers($community_id, $period);

    /**
     * @param bool $verified
     *
     * @return mixed
     */
    public function getAll($verified = false);
}