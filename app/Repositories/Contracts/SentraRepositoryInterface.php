<?php

namespace App\Repositories\Contracts;

/**
 * Interface SentraRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface SentraRepositoryInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param $sentra_id
     *
     * @return mixed
     */
    public function find($sentra_id);

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $relations
     *
     * @return mixed
     */
    public function getJoin(array $relations);

    /**
     * @param $mms_id
     *
     * @return mixed
     */
    public function getByMmsId($mms_id);

    /**
     * @param $mms_id
     *
     * @return mixed
     */
    public function getDropdownByMmsId($mms_id);

    /**
     * @return mixed
     */
    public function getAll();
}