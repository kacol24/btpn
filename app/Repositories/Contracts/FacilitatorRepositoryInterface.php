<?php

namespace App\Repositories\Contracts;

/**
 * Interface FacilitatorRepositoryInterface
 * @package Repositories\Contracts
 */
interface FacilitatorRepositoryInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param       $id
     * @param array $relations
     *
     * @return mixed
     */
    public function findJoin($id, array $relations);

    /**
     * @param bool $withDeactivated
     *
     * @return mixed
     */
    public function getAll($withDeactivated = false);

    /**
     * @param $facilitator_id
     *
     * @return mixed
     */
    public function getCommunitiesById($facilitator_id);

    /**
     * @return mixed
     */
    public function getLoggedIn();

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id);

    /**
     * @param                  $id
     * @param array            $data
     *
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function deactivate($id);
}