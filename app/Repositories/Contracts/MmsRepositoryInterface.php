<?php

namespace App\Repositories\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface MmsRepositoryInterface
 * @package Repositories\Contracts
 */
interface MmsRepositoryInterface
{
    /**
     * @param $city_id
     *
     * @return mixed
     */
    public function dropdownFromCityId($city_id);

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param       $id
     * @param array $relations
     *
     * @return mixed
     */
    public function findJoin($id, array $relations);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id);

    /**
     * @param                  $id
     * @param array|Collection $data
     *
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $relations
     *
     * @return mixed
     */
    public function getWithRelations(array $relations);

    /**
     * @return mixed
     */
    public function getDropdown();

    /**
     * @return mixed
     */
    public function getLoggedIn();

    /**
     * @param $mms_id
     *
     * @return mixed
     */
    public function getCommunitiesById($mms_id);
}