<?php
namespace App\Repositories\Contracts;

/**
 * Interface CityRepositoryInterface
 * @package Repositories\City
 */
/**
 * Interface CityRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface CityRepositoryInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id);

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $columns
     *
     * @return mixed
     */
    public function getAll(array $columns = ['*']);

    /**
     * @return mixed
     */
    public function getDropdown();
}