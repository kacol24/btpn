<?php

namespace App\Repositories\Contracts;

/**
 * Interface LevelRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface LevelRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id);

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function delete($id);

    /**
     * @param null $value
     *
     * @return mixed
     */
    public function getDropdown($value = null);

    /**
     * @return mixed
     */
    public function getUnassignedDropdown();
}