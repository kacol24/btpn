<?php

namespace App\Repositories\Contracts;

interface MonthlyReportRepositoryInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);
}