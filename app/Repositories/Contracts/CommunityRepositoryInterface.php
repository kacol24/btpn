<?php

namespace App\Repositories\Contracts;

    /**
     * Interface CommunityRepositoryInterface
     * @package Repositories\Contracts
     */
/**
 * Interface CommunityRepositoryInterface
 * @package App\Repositories\Contracts
 */
use App\Models\Facilitator;

/**
 * Interface CommunityRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface CommunityRepositoryInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id);

    /**
     * @param       $id
     * @param array $relations
     *
     * @return mixed
     */
    public function findWithRelations($id, array $relations);

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $relations
     *
     * @return mixed
     */
    public function getWithRelations(array $relations);

    /**
     * @param $facilitator_id
     * @param $city_id
     *
     * @return mixed
     */
    public function getDropdownOwnedByFacilitatorWhereCityId($facilitator_id, $city_id);

    /**
     * @param bool $activeOnly
     *
     * @return mixed
     */
    public function getAll($activeOnly = false);

    /**
     * @param $mms_id
     *
     * @return mixed
     */
    public function getDropdownWhereMmsId($mms_id);

    /**
     * @param $city_id
     *
     * @return mixed
     */
    public function getDropdownByCityIdWhereFacilitatorNull($city_id);

    /**
     * @param $id
     *
     * @param $reason
     *
     * @return mixed
     */
    public function deactivate($id, $reason);

    /**
     * @param $community_id
     *
     * @return mixed
     */
    public function getMembers($community_id);

    /**
     * @param $community_id
     *
     * @return mixed
     */
    public function getActiveMembers($community_id);

    /**
     * @param array $community_ids
     * @param       $facilitator_id
     *
     * @return mixed
     */
    public function syncFacilitator(array $community_ids, $facilitator_id);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getMaturityValues($id);

    /**
     * @param $id
     * @param $values
     *
     * @return mixed
     */
    public function syncMaturityValue($id, $values);

    /**
     * @param Facilitator $facilitator
     *
     * @return mixed
     */
    public function getDropdownForFacilitator(Facilitator $facilitator);

    /**
     * @param $facilitator_id
     *
     * @return mixed
     */
    public function getDropdownOwnedByFacilitator($facilitator_id);

    /**
     * @param $year
     *
     * @return mixed
     */
    public function getActiveCommunitiesPerMonth($year);

    /**
     * @param $facilitator_id
     *
     * @return mixed
     */
    public function facilitatorDeactivated($facilitator_id);

    /**
     * @param $year
     *
     * @return mixed
     */
    public function getNumberOfPastCommunities($year);

    /**
     * @param $year
     *
     * @return mixed
     */
    public function getInactiveCommunitiesPerMonth($year);
}