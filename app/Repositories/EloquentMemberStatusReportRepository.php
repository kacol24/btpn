<?php

namespace App\Repositories;

use App\Models\MemberStatusReport;
use App\Models\ReportLog;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\MemberRepositoryInterface;
use App\Repositories\Contracts\MemberStatusReportRepositoryInterface;
use App\Repositories\Contracts\ReportLogRepositoryInterface;
use App\Repositories\Contracts\ReportRepository;
use Carbon;
use DB;
use Illuminate\Support\Collection;

/**
 * Class EloquentMemberStatusReportRepository
 * @package App\Repositories
 */
class EloquentMemberStatusReportRepository extends ReportRepository implements MemberStatusReportRepositoryInterface
{
    const REPORT_TYPE = 'MSR';
    /**
     * @var MemberStatusReport
     */
    private $memberStatusReport;
    /**
     * @var Member
     */
    private $members;
    /**
     * @var ReportLog
     */
    private $reportLog;
    /**
     * @var CommunityRepositoryInterface
     */
    private $communities;
    /**
     * @var ReportLog
     */
    private $reportLogModel;

    /**
     * EloquentMemberStatusReportRepository constructor.
     *
     * @param MemberStatusReport           $memberStatusReport
     * @param MemberRepositoryInterface    $members
     * @param ReportLogRepositoryInterface $reportLog
     * @param CommunityRepositoryInterface $communities
     * @param ReportLog                    $reportLogModel
     */
    public function __construct(
        MemberStatusReport $memberStatusReport,
        MemberRepositoryInterface $members,
        ReportLogRepositoryInterface $reportLog,
        CommunityRepositoryInterface $communities,
        ReportLog $reportLogModel
    ) {
        $this->memberStatusReport = $memberStatusReport;
        $this->members = $members;
        $this->reportLog = $reportLog;
        $this->communities = $communities;
        $this->reportLogModel = $reportLogModel;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $file = $data['f4_document_url'];

            array_forget($data, 'f4_document_url'); // skip saving document temp directory
            $report = $this->memberStatusReport->create($data);
            $report->meta()->create($data);
            $this->resyncMemberStatus($report, $data);

            $report->f4_document_url = $this->uploadFile(self::REPORT_TYPE, $report->id, 'F4', $data['community_id'],
                $file);
            $report->save();

            return $report;
        });
    }

    private function resyncMemberStatus($report, array $data)
    {
        $membersStatus = isset($data['active']) ? $data['active'] : [];
        $community_id = $data['community_id'];
        $member_ids = [];

        if (! empty($membersStatus)) { // if not empty, map the status to an array
            foreach ($membersStatus as $id => $member) {
                $member_ids[] = $id;
            }
        }
        $this->members->deactivate($community_id, $member_ids);

        $members = $this->communities->getMembers($community_id);

        $membersData = [];
        foreach ($members as $member) {
            if (isset($membersStatus[$member->id])) {
                $membersData[$member->id] = ['is_active' => true];
            } else {
                $membersData[$member->id] = ['is_active' => false];
            }
        }
        $report->members()->sync($membersData);
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $report = $this->reportLog->find($id);

            try {
                if (isset($data['f4_document_url'])) {
                    $file = $data['f4_document_url'];

                    $data['f4_document_url'] = $this->uploadFile(self::REPORT_TYPE, $report->id, 'F4',
                        $data['community_id'], $file);
                }

                $data = array_merge($data, self::REPORT_PENDING_STATUS); // TODO experimental, PHP >= 5.6 only

                $report->update($data);
                $report->report->update($data);
                $this->resyncMemberStatus($report->report, $data);
            } catch (Exception $e) {

            }

            return $report;
        });
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        $report = $this->memberStatusReport->findOrFail($id);

        return $report;
    }

    /**
     * @param $community_id
     * @param $period
     *
     * @return mixed
     */
    public function getMembers($community_id, $period)
    {
        $report = $this->reportLog->getMemberStatusReport($community_id, $period);

        $members = isset($report->report->members) ? $report->report->members : new Collection();

        return $members;
    }

    /**
     * @param bool $verified
     *
     * @return mixed
     */
    public function getAll($verified = false)
    {
        $reports = $this->reportLogModel->memberStatusReport();
        if ($verified) {
            $reports->verified();
        }

        return $reports->get();
    }
}