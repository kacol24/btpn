<?php

namespace App\Repositories;

use App\Models\City;
use App\Models\Community;
use App\Models\Facilitator;
use App\Repositories\Contracts\CityRepositoryInterface;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use Carbon;
use DB;
use Illuminate\Support\Collection;

/**
 * Class EloquentCommunityRepository
 * @package Repositories
 */
class EloquentCommunityRepository implements CommunityRepositoryInterface
{
    /**
     * @var CityRepositoryInterface
     */
    private $city;
    /**
     * @var Facilitator
     */
    private $facilitator;
    /**
     * @var Community
     */
    private $model;

    /**
     * EloquentCommunityRepository constructor.
     *
     * @param Community               $community
     * @param CityRepositoryInterface $city
     * @param Facilitator             $facilitator
     */
    public function __construct(Community $community, CityRepositoryInterface $city, Facilitator $facilitator)
    {
        $this->model = $community;
        $this->city = $city;
        $this->facilitator = $facilitator;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $model = $this->model->create($data);

            return $model;
        });
    }

    /**
     * @param       $id
     *
     * @param array $relations
     *
     * @return mixed
     */
    public function findWithRelations($id, array $relations)
    {
        return $this->model->with($relations)->findOrFail($id);
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $model = $this->find($id);
            $model->update($data);

            return $model;
        });
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param array $relations
     *
     * @return mixed
     */
    public function getWithRelations(array $relations)
    {
        return $this->model->with($relations)->get();
    }

    /**
     * @param $facilitator_id
     * @param $city_id
     *
     * @return mixed
     */
    public function getDropdownOwnedByFacilitatorWhereCityId($facilitator_id, $city_id)
    {
        $communities = City::findOrFail($city_id)
                           ->communities()
                           ->whereNull('deactivation_date')
                           ->whereNull('communities.facilitator_id')
                           ->orWhere('facilitator_id', $facilitator_id)
                           ->get(['communities.name', 'communities.id'])
                           ->lists('name', 'id');
        $communities->prepend('', '');

        return $communities;
    }

    /**
     * @param $mms_id
     *
     * @return mixed
     */
    public function getDropdownWhereMmsId($mms_id)
    {
        $dropdown = $this->model->whereMmsId($mms_id)->lists('name', 'id');
        $dropdown->prepend('', '');

        return $dropdown;
    }

    /**
     * @param $city_id
     *
     * @return mixed
     */
    public function getDropdownByCityIdWhereFacilitatorNull($city_id)
    {
        return $this->city->find($city_id)
                          ->communities()
                          ->whereNull('deactivation_date')
                          ->whereNull('facilitator_id')
                          ->get(['name', 'communities.id'])
                          ->lists('name', 'id');
    }

    /**
     * @param $id
     * @param $reason
     *
     * @return mixed
     */
    public function deactivate($id, $reason)
    {
        return DB::transaction(function () use ($id, $reason) {
            $community = $this->find($id);
            $community->deactivation_date = Carbon::now();
            $community->deactivation_reason = $reason;
            $community->save();

            return $community;
        });
    }

    /**
     * @param $community_id
     *
     * @return mixed
     */
    public function getMembers($community_id)
    {
        $community = $this->model->find($community_id);
        if (! empty($community)) {
            $members = $community->members;

            return $members;
        }

        return new Collection();
    }

    /**
     * @param array $community_ids
     * @param       $facilitator_id
     *
     * @return mixed
     */
    public function syncFacilitator(array $community_ids, $facilitator_id)
    {
        return DB::transaction(function () use ($community_ids, $facilitator_id) {
            $community = $this->model;
            $this->facilitatorDeactivated($facilitator_id);

            if (isset($community_ids) && ! empty($community_ids)) {
                $community->whereIn('id', $community_ids)
                          ->update([
                              'facilitator_id' => $facilitator_id,
                          ]);
            }

            return $community;
        });
    }

    /**
     * @param $facilitator_id
     *
     * @return mixed
     */
    public function facilitatorDeactivated($facilitator_id)
    {
        $communities = $this->model->whereFacilitatorId($facilitator_id);
        DB::transaction(function () use ($communities) {
            $communities->update([
                'facilitator_id' => null,
            ]);
        });

        return $communities;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getMaturityValues($id)
    {
        $community = $this->find($id);

        // gets only the latest available
        $latestPeriod = $community->maturityValues()
                                  ->orderBy('pivot_period', 'desc')
                                  ->first(); // latest maturity value
        $maturityValues = $community->maturityValues()
                                    ->wherePivot('period', '=',
                                        isset($latestPeriod->pivot->period) ? $latestPeriod->pivot->period : '')
                                    ->get(); // all latest

        return $maturityValues;
        //$community = $this->find($id);
        //
        //return $community->maturityValues();
    }

    /**
     * @param $id
     * @param $values
     *
     * @return mixed
     */
    public function syncMaturityValue($id, $values)
    {
        // on pivot -- get where community id is X and period is Y (many records)
        // if query > 0 (has maturity model), delete all, insert new values
        // if query 0 (no maturity model), insert new values
        $period = Carbon::now()->startOfMonth()->format('Y-m-d');
        $community = $this->model->find($id);

        $maturityModel = $community->maturityValues()->where([ // try to get existing data
            'period' => $period,
        ]); // this return MaturityValue model instance

        // if no data, create new, else delete old ones, and create new
        if ($maturityModel->get()->count() > 0) {
            $community->maturityValues()->newPivotStatement()->where('period', $period)->delete();
        }
        // create new
        foreach ($values as $value) {
            $community->maturityValues()->attach($value, [
                'period' => $period,
            ]);
        }

        return $community;
        //$community = $this->find($id);
        //$community->maturityValues()->sync($values);
        //
        //return $community;
    }

    /**
     * @param $community_id
     *
     * @return mixed
     */
    public function getActiveMembers($community_id)
    {
        $community = $this->find($community_id);
        $members = $community->members()->active()->get();

        return $members;
    }

    /**
     * @param Facilitator $facilitator
     *
     * @return mixed
     */
    public function getDropdownForFacilitator(Facilitator $facilitator)
    {
        $communities = $this->model->active()
                                   ->whereIn('mms_id', $facilitator->city->mms->lists('id'))
                                   ->whereNull('facilitator_id')
                                   ->orWhere('facilitator_id', $facilitator->id)
                                   ->get()
                                   ->lists('name', 'id');
        $communities->prepend('', '');

        return $communities;
    }

    /**
     * @param $facilitator_id
     *
     * @return mixed
     */
    public function getDropdownOwnedByFacilitator($facilitator_id)
    {
        $communities = $this->model->active()
                                   ->ownedBy($facilitator_id)
                                   ->get()
                                   ->lists('name', 'id');
        $communities->prepend('', '');

        return $communities;
    }

    /**
     * @param bool $activeOnly
     *
     * @return mixed
     */
    public function getAll($activeOnly = false)
    {
        if ($activeOnly) {
            return $this->model->active()->get();
        }

        return $this->model->all();
    }

    /**
     * @param $year
     *
     * @return mixed
     */
    public function getActiveCommunitiesPerMonth($year)
    {
        $activeCommunitiesPerMonth = $this->model->whereYear('activation_date', '=', $year)
                                                 ->groupBy('activation_month')
                                                 ->orderBy('activation_month')
                                                 ->get([
                                                     DB::raw('MONTH(activation_date) as activation_month'),
                                                     DB::raw('COUNT(*) as "communities"'),
                                                 ])->lists('communities', 'activation_month');

        return $activeCommunitiesPerMonth;
    }

    /**
     * @param $year
     *
     * @return mixed
     */
    public function getNumberOfPastCommunities($year)
    {
        $pastCommunities = $this->model->whereYear('activation_date', '<', $year)->active();

        return $pastCommunities->count();
    }

    /**
     * @param $year
     *
     * @return mixed
     */
    public function getInactiveCommunitiesPerMonth($year)
    {
        $inactiveCommunitiesPerMonth = $this->model->whereYear('deactivation_date', '=', $year)
                                                   ->groupBy('deactivation_month')
                                                   ->orderBy('deactivation_month')
                                                   ->get([
                                                       DB::raw('MONTH(deactivation_date) as deactivation_month'),
                                                       DB::raw('COUNT(*) as "communities"'),
                                                   ])->lists('communities', 'deactivation_month');

        return $inactiveCommunitiesPerMonth;
    }
}