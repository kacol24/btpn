<?php

namespace App\Repositories;

use App\Models\ReportLog;
use App\Repositories\Contracts\ReportLogRepositoryInterface;
use Carbon\Carbon;
use DB;

class EloquentReportLogRepository implements ReportLogRepositoryInterface
{
    /**
     * @var ReportLog
     */
    private $reportLog;

    /**
     * EloquentReportLogRepository constructor.
     *
     * @param ReportLog $reportLog
     */
    public function __construct(ReportLog $reportLog)
    {
        $this->reportLog = $reportLog;
    }

    /**
     * @param $id
     * @param $reason
     *
     * @return mixed
     */
    public function decline($id, $reason)
    {
        return DB::transaction(function () use ($id, $reason) {
            $report = $this->find($id);
            $report->update([
                'is_verified'     => true,
                'verified_at'     => Carbon::now(),
                'declined_at'     => Carbon::now(),
                'declined_reason' => $reason,
            ]);

            return $report;
        });
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        return $this->reportLog->findOrFail($id);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function verify($id)
    {
        return DB::transaction(function () use ($id) {
            $report = $this->find($id);
            $report->update([
                'is_verified' => true,
                'verified_at' => Carbon::now(),
            ]);

            return $report;
        });
    }

    /**
     * @return mixed
     */
    public function getPending()
    {
        return $this->reportLog->pending()->orderBy('created_at', 'desc')->get();
    }

    /**
     * @param null $facilitator_id
     *
     * @return mixed
     */
    public function getDeclined($facilitator_id = null)
    {
        $declinedReports = $this->reportLog->declined();
        if ($facilitator_id) {
            $declinedReports->where('facilitator_id', $facilitator_id);
        }

        return $declinedReports->orderBy('created_at', 'desc')->get();
    }

    /**
     * @param      $period
     * @param bool $yearOnly
     * @param bool $verified
     *
     * @return mixed
     */
    public function getDailyReportsWithinPeriod($period, $yearOnly = false, $verified = false)
    {
        $dailyReports = $this->reportLog->dailyReports()->period($period, $yearOnly);
        if ($verified) {
            return $dailyReports->verified();
        }

        return $dailyReports;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function void($id)
    {
        return DB::transaction(function () use ($id) {
            $report = $this->find($id);
            $report->report->delete();
            $report->delete();

            return $report;
        });
    }

    /**
     * @param $facilitator_id
     *
     * @return mixed
     */
    public function getWhereFacilitator($facilitator_id)
    {
        $reports = $this->reportLog->where('facilitator_id', $facilitator_id)
                                   ->orderBy('created_at', 'desc');

        return $reports;
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $report = $this->find($id);
            $report->update($data);
            $report->report->update($data);

            return $report;
        });
    }

    /**
     * @param      $community_id
     * @param null $period
     *
     * @return mixed
     */
    public function getMemberStatusReport($community_id, $period = null)
    {
        $report = $this->reportLog->whereCommunityId($community_id)
                                  ->memberStatusReport();
        if ($period) {
            $memberStatusReport = $report->period($period);

            return $memberStatusReport->first();
        }

        return $report->get();
    }

    /**
     * @param $community_id
     *
     * @return mixed
     */
    public function getDailyReportsOfCommunity($community_id)
    {
        $reports = $this->reportLog->verified()
                                   ->dailyReports()
                                   ->where('community_id', $community_id);

        return $reports->get();
    }

    /**
     * @param null $period
     * @param bool $yearOnly
     *
     * @return mixed
     */
    public function getAllMonthlyMemberStatusReports($period = null, $yearOnly = false)
    {
        $reportLogs = $this->reportLog->memberStatusReport();

        if ($period) {
            $memberStatusReports = $reportLogs->verified()
                                              ->period($period, $yearOnly)
                                              ->orderBy('period');

            return $memberStatusReports;
        }

        return $reportLogs;
    }
}