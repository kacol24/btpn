<?php

namespace App\Repositories;

use App\Models\Facilitator;
use App\Models\Role;
use App\Models\User;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\FacilitatorRepositoryInterface;
use App\Repositories\Traits\UploadsProfileImage;
use Auth;
use Carbon;
use DB;
use File;

class EloquentFacilitatorRepository implements FacilitatorRepositoryInterface
{
    use UploadsProfileImage;

    /**
     * @var CommunityRepositoryInterface
     */
    private $communities;
    private $model;

    /**
     * EloquentFacilitatorRepository constructor.
     *
     * @param Facilitator                  $facilitator
     * @param CommunityRepositoryInterface $communities
     */
    public function __construct(Facilitator $facilitator, CommunityRepositoryInterface $communities)
    {
        $this->model = $facilitator;
        $this->communities = $communities;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $facilitatorRole = Role::whereName('facilitator')->first();

            $data = $this->handleImageUpload($data);

            $user = User::create($data);
            $user->attachRole($facilitatorRole);

            $this->model->city_id = $data['city_id'];
            $this->model->dob = $data['dob'];
            $this->model->gender = $data['gender'];
            $this->model->address = $data['address'];
            $facilitator = $user->facilitator()->save($this->model);

            $this->communities->syncFacilitator(isset($data['community_id']) ? $data['community_id'] : [],
                $facilitator->id);

            return $facilitator;
        });
    }

    /**
     * @param       $id
     * @param array $relations
     *
     * @return mixed
     */
    public function findJoin($id, array $relations)
    {
        return $this->model->with($relations)->findOrFail($id);
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     * @internal param CommunityRepositoryInterface $community
     *
     */
    public function update($id, array $data)
    {
        $facilitator = $this->model->findOrFail($id);
        $oldImage = $facilitator->picture;

        $transaction = DB::transaction(function () use ($id, $data, $facilitator) {
            $data = $this->handleImageUpload($data);

            $facilitator->update($data);
            $facilitator->user()->update($data);

            $this->communities->syncFacilitator(isset($data['community_id']) ? $data['community_id'] : [],
                $facilitator->id);

            return $facilitator;
        });
        if ($data['picture']) {
            File::delete(config('btpn.profile_upload_directory') . $oldImage);
        }

        return $transaction;
    }

    /**
     * @param $facilitator_id
     *
     * @return mixed
     */
    public function getCommunitiesById($facilitator_id)
    {
        return $this->model->findOrFail($facilitator_id)->communities()->with('mms')->get();
    }

    /**
     * @return mixed
     */
    public function getLoggedIn()
    {
        return $this->find(Auth::user()->facilitator->id)->first();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param                              $id
     *
     * @return mixed
     */
    public function deactivate($id)
    {
        $facilitator = $this->find($id);
        DB::transaction(function () use ($facilitator) {
            $facilitator->deactivation_date = Carbon::now();
            $facilitator->save();
            $this->communities->facilitatorDeactivated($facilitator->id);
        });

        return $facilitator;
    }

    /**
     * @param bool $withDeactivated
     *
     * @return mixed
     */
    public function getAll($withDeactivated = false)
    {
        $facilitators = $this->model;
        if ($withDeactivated) {
            return $facilitators->all();
        }

        return $facilitators->active()->get();
    }
}