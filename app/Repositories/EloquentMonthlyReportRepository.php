<?php

namespace App\Repositories;

use App\Models\MonthlyReport;
use App\Repositories\Contracts\MonthlyReportRepositoryInterface;
use DB;

class EloquentMonthlyReportRepository implements MonthlyReportRepositoryInterface
{
    /**
     * @var MonthlyReport
     */
    private $monthlyReport;

    /**
     * EloquentMonthlyReportRepository constructor.
     *
     * @param MonthlyReport $monthlyReport
     */
    public function __construct(MonthlyReport $monthlyReport)
    {
        $this->monthlyReport = $monthlyReport;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $report = $this->monthlyReport->create($data);

            return $report;
        });
    }
}