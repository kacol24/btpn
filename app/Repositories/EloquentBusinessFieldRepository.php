<?php

namespace App\Repositories;

use App\Models\BusinessField;
use App\Repositories\Contracts\BusinessFieldRepositoryInterface;

class EloquentBusinessFieldRepository implements BusinessFieldRepositoryInterface
{

    public function __construct(BusinessField $businessField)
    {
        $this->model = $businessField;
    }

    public function lists($value, $key)
    {
        return $this->model->lists('name', 'id');
    }

    /**
     * @return mixed
     */
    public function getDropdown()
    {
        $businessFields = $this->model->lists('name', 'id');
        $businessFields->prepend('', '');

        return $businessFields;
    }
}