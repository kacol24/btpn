<?php

namespace App\Repositories;

use App\Models\Aspect;
use App\Repositories\Contracts\AspectRepositoryInterface;
use App\Repositories\Contracts\LevelRepositoryInterface;
use DB;

/**
 * Class EloquentAspectRepository
 * @package App\Repositories
 */
class EloquentAspectRepository implements AspectRepositoryInterface
{
    /**
     * @var Aspect
     */
    private $aspect;
    /**
     * @var LevelRepositoryInterface
     */
    private $level;

    /**
     * EloquentAspectRepository constructor.
     *
     * @param Aspect                   $aspect
     * @param LevelRepositoryInterface $level
     */
    public function __construct(Aspect $aspect, LevelRepositoryInterface $level)
    {
        $this->aspect = $aspect;
        $this->level = $level;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $aspect = $this->aspect->create($data);

            return $aspect;
        });
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $aspect = $this->find($id);
            $aspect->update($data);

            return $aspect;
        });
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        $aspect = $this->aspect->findOrFail($id);

        return $aspect;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        return DB::transaction(function () use ($id) {
            $aspect = $this->find($id);
            $aspect->delete();

            return $aspect;
        });
    }

    /**
     * @param null $value
     *
     * @return mixed
     */
    public function getDropdown($value = null)
    {
        $aspect = $this->getAll()->lists('name', 'id');
        $aspect->prepend('', '');

        return $aspect;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        $aspect = $this->aspect->all();

        return $aspect;
    }

    public function getUnassignedDropdown()
    {
        $levels = $this->level->getAll()->count();
        $aspects = $this->aspect->has('values', '<', $levels)->get();
        $dropdown = $aspects->lists('name', 'id');
        $dropdown->prepend('', '');

        return $dropdown;
    }
}