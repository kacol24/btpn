<?php

namespace App\Repositories;

use App\Models\Sentra;
use App\Repositories\Contracts\SentraRepositoryInterface;
use DB;

class EloquentSentraRepository implements SentraRepositoryInterface
{
    public function __construct(Sentra $sentra)
    {
        $this->model = $sentra;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $model = $this->model->create($data);

            return $model;
        });
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $sentra = $this->find($id);
            $sentra->update($data);

            return $sentra;
        });
    }

    /**
     * @param $sentra_id
     *
     * @return mixed
     */
    public function find($sentra_id)
    {
        return $this->model->findOrFail($sentra_id);
    }

    /**
     * @param array $relations
     *
     * @return mixed
     */
    public function getJoin(array $relations)
    {
        return $this->model->with($relations)->get();
    }

    /**
     * @param $mms_id
     *
     * @return mixed
     */
    public function getDropdownByMmsId($mms_id)
    {
        $dropdown = $this->getByMmsId($mms_id)->lists('name', 'id');
        $dropdown->prepend('', '');

        return $dropdown;
    }

    /**
     * @param $mms_id
     *
     * @return mixed
     */
    public function getByMmsId($mms_id)
    {
        return $this->model->whereMmsId($mms_id);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $sentra = $this->model->all();

        return $sentra;
    }
}