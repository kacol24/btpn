<?php

namespace App\Repositories;

use App\Models\Level;
use App\Repositories\Contracts\LevelRepositoryInterface;
use DB;

/**
 * Class EloquentLevelRepository
 * @package App\Repositories
 */
class EloquentLevelRepository implements LevelRepositoryInterface
{
    /**
     * @var Level
     */
    private $level;

    /**
     * EloquentLevelRepository constructor.
     *
     * @param Level $level
     */
    public function __construct(Level $level)
    {
        $this->level = $level;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $level = $this->level->create($data);

            return $level;
        });
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $level = $this->find($id);
            $level->update($data);

            return $level;
        });
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        $level = $this->level->findOrFail($id);

        return $level;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        return DB::transaction(function () use ($id) {
            $level = $this->find($id);
            $level->delete();

            return $level;
        });
    }

    /**
     * @param null $value
     */
    public function getDropdown($value = null)
    {
        $level = $this->getAll()->lists('name', 'id');
        $level->prepend('', '');

        return $level;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        $level = $this->level->orderBy('score')->get();

        return $level;
    }

    /**
     * @return mixed
     */
    public function getUnassignedDropdown()
    {
        $levels = $this->level->doesntHave('values')->get();
        $dropdown = $levels->lists('name', 'id');
        $dropdown->prepend('', '');

        return $dropdown;
    }
}