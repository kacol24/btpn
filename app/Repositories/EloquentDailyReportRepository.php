<?php

namespace App\Repositories;

use App\Models\DailyReport;
use App\Models\ReportLog;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\DailyReportRepositoryInterface;
use App\Repositories\Contracts\ReportRepository;
use DB;

/**
 * Class EloquentDailyReportRepository
 * @package App\Repositories
 */
class EloquentDailyReportRepository extends ReportRepository implements DailyReportRepositoryInterface
{
    const REPORT_TYPE = 'DR';
    /**
     * @var DailyReport
     */
    private $dailyReport;
    /**
     * @var CommunityRepositoryInterface
     */
    private $community;
    /**
     * @var CommunityRepositoryInterface
     */
    private $communities;
    /**
     * @var ReportLog
     */
    private $reportLog;

    /**
     * EloquentDailyReportRepository constructor.
     *
     * @param DailyReport                  $dailyReport
     * @param CommunityRepositoryInterface $communities
     *
     * @param ReportLog                    $reportLog
     *
     * @internal param CommunityRepositoryInterface $community
     */
    public function __construct(
        DailyReport $dailyReport,
        CommunityRepositoryInterface $communities,
        ReportLog $reportLog
    ) {
        $this->dailyReport = $dailyReport;
        $this->communities = $communities;
        $this->reportLog = $reportLog;
    }

    /**
     * @param array $data
     *
     * @return mixed
     * @internal param CommunityRepositoryInterface $community
     *
     */
    public function create(array $data)
    {
        $dailyReport = DB::transaction(function () use ($data) {
            try {
                $membersData = [];
                $members = $this->communities->getMembers($data['community_id']);

                foreach ($members as $member) {
                    if (isset($data['attendance'][$member->id])) {
                        $membersData[$member->id] = ['attended' => true];
                    } else {
                        $membersData[$member->id] = ['attended' => false];
                    }
                }

                $f2_document = $data['f2_document_url'];
                $f3_document = isset($data['f3_document_url']) ? $data['f3_document_url'] : null;
                $supporting_document = isset($data['supporting_document_url']) ? $data['supporting_document_url'] : null;

                array_forget($data,
                    ['f2_document_url', 'f3_document_url', 'supporting_document_url']); // don't save temporary file

                $report = $this->dailyReport->create($data);
                $report->meta()->create($data);
                $report->members()->sync($membersData);

                $report->f2_document_url = $this->uploadFile(self::REPORT_TYPE, $report->id, 'F2',
                    $data['community_id'],
                    $f2_document);
                if ($f3_document) {
                    $report->f3_document_url = $this->uploadFile(self::REPORT_TYPE, $report->id, 'F3',
                        $data['community_id'],
                        $f3_document);
                }
                if (isset($supporting_document)) {
                    $report->supporting_document_url = $this->uploadFile(self::REPORT_TYPE, $report->id,
                        'SupportingDocument',
                        $data['community_id'], $supporting_document);
                }
                $report->save();
            } catch (Exception $e) {

            }

            return $report;
        });

        return $dailyReport;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->dailyReport->all();
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        $dailyReport = DB::transaction(function () use ($id, $data) {
            $members = $this->communities->getMembers($data['community_id']);

            $membersData = [];

            foreach ($members as $member) {
                if (isset($data['attendance'][$member->id])) {
                    $membersData[$member->id] = ['attended' => true];
                } else {
                    $membersData[$member->id] = ['attended' => false];
                }
            }

            $f2_document = $data['f2_document_url'];
            $f3_document = isset($data['f3_document_url']) ? $data['f3_document_url'] : null;
            $supporting_document = isset($data['supporting_document_url']) ? $data['supporting_document_url'] : null;

            $report = $this->dailyReport->find($id);
            $data['f2_document_url'] = $this->uploadFile(self::REPORT_TYPE, $report->id, 'F2',
                $data['community_id'],
                $f2_document);
            if ($f3_document) {
                $data['f3_document_url'] = $this->uploadFile(self::REPORT_TYPE, $report->id, 'F3',
                    $data['community_id'],
                    $f3_document);
            }
            if (isset($supporting_document)) {
                $data['supporting_document_url'] = $this->uploadFile(self::REPORT_TYPE, $report->id,
                    'SupportingDocument',
                    $data['community_id'], $supporting_document);
            }
            $report->update($data);

            $reportLog = $this->reportLog->find($report->meta->id);
            $reportLog->update($data);
            $reportLog->update([
                'is_verified'     => false,
                'declined_at'     => null,
                'declined_reason' => null,
            ]);

            $report->members()->sync($membersData);

            return $report;
        });

        return $dailyReport;
    }
}