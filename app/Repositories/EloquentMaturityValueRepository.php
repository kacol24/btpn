<?php

namespace App\Repositories;

use App\Models\MaturityValue;
use App\Repositories\Contracts\MaturityValueRepositoryInterface;
use DB;

class EloquentMaturityValueRepository implements MaturityValueRepositoryInterface
{
    /**
     * @var MaturityValue
     */
    private $maturityValue;

    public function __construct(MaturityValue $maturityValue)
    {
        $this->maturityValue = $maturityValue;
    }

    public function getAll()
    {
        $maturityValues = $this->maturityValue->all();

        return $maturityValues;
    }

    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $maturityValue = $this->maturityValue->create($data);

            return $maturityValue;
        });
    }

    public function update($id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $maturityValue = $this->find($id);
            $maturityValue->update($data);

            return $maturityValue;
        });
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        $maturityValue = $this->maturityValue->findOrFail($id);

        return $maturityValue;
    }

    public function delete($id)
    {
        return DB::transaction(function () use ($id) {
            $maturityValue = $this->find($id);
            $maturityValue->delete();

            return $maturityValue;
        });
    }
}