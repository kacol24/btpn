<?php

namespace App\Repositories;

use App\Models\Mms;
use App\Models\Role;
use App\Models\User;
use App\Repositories\Contracts\MmsRepositoryInterface;
use Auth;
use DB;
use Illuminate\Support\Collection;

class EloquentMmsRepository implements MmsRepositoryInterface
{
    public function __construct(Mms $mms)
    {
        $this->model = $mms;
    }

    /**
     * @param $city_id
     *
     * @return mixed
     */
    public function dropdownFromCityId($city_id)
    {
        $dropdown = $this->model->whereCityId($city_id)->get()->lists('name', 'id');
        $dropdown->prepend('', '');

        return $dropdown;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $mmsRole = Role::whereName('mms')->first();

            $user = User::create([
                'username' => $data['username'],
                'password' => $data['password'],
                'name'     => $data['name'],
                'phone'    => $data['phone'],
                'email'    => $data['email'],
                'picture'  => isset($data['picture']) ? $data['picture'] : null,
            ]);
            $user->mms()->create([
                'city_id' => $data['city_id'],
                'address' => $data['address'],
            ]);
            $user->attachRole($mmsRole);

            return $user;
        });
    }

    /**
     * @param       $id
     * @param array $relations
     *
     * @return mixed
     */
    public function findJoin($id, array $relations)
    {
        return $this->model->with($relations)->findOrFail($id);
    }

    /**
     * @param            $id
     * @param Collection $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $mms = $this->find($id);

            $mms->update($data);
            $mms->user()->update($data);

            return $mms;
        });
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param array $relations
     *
     * @return mixed
     */
    public function getWithRelations(array $relations)
    {
        return $this->model->with($relations)->get();
    }

    /**
     * @return mixed
     */
    public function getDropdown()
    {
        $dropdown = $this->model->get()->lists('name', 'id');
        $dropdown->prepend('', '');

        return $dropdown;
    }

    /**
     * @return mixed
     */
    public function getLoggedIn()
    {
        return $this->find(Auth::user()->mms->id)->first();
    }

    /**
     * @param $mms_id
     *
     * @return mixed
     */
    public function getCommunitiesById($mms_id)
    {
        return $this->model->findOrFail($mms_id)->communities()->with('mms')->get();
    }
}