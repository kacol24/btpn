<?php

namespace App\Repositories;

use App\Models\ProgramUpdateReport;
use App\Models\ReportLog;
use App\Repositories\Contracts\ProgramUpdateReportRepositoryInterface;
use App\Repositories\Contracts\ReportRepository;
use Carbon;
use DB;

/**
 * Class EloquentProgramUpdateReportRepository
 * @package Repositories
 */
class EloquentProgramUpdateReportRepository extends ReportRepository implements ProgramUpdateReportRepositoryInterface
{
    /**
     *
     */
    const REPORT_TYPE = 'PUR';

    /**
     * @var ReportLog
     */
    private $reportLog;
    /**
     * @var ProgramUpdateReport
     */
    private $programUpdateReport;

    /**
     * EloquentProgramUpdateReportRepository constructor.
     *
     * @param ReportLog           $reportLog
     * @param ProgramUpdateReport $programUpdateReport
     */
    public function __construct(ReportLog $reportLog, ProgramUpdateReport $programUpdateReport)
    {
        $this->reportLog = $reportLog;
        $this->programUpdateReport = $programUpdateReport;
    }

    /**
     * @param      $community_id
     * @param      $period
     * @param null $activity_type_id
     * @param bool $verified
     *
     * @return $this|static
     */
    public function getDailyReports($community_id, $period, $activity_type_id = null, $verified = false)
    {
        $dailyReports = $this->reportLog->with('report')
                                        ->where(function ($query) use ($community_id, $period, $verified) {
                                            $period = Carbon::parse($period);
                                            if ($verified) {
                                                $query->where('is_verified', true);
                                            }
                                            $query->where('report_type', 'App\Models\DailyReport');
                                            $query->where('community_id', $community_id);
                                            $query->whereYear('period', '=', $period->year);
                                            $query->whereMonth('period', '=', $period->month);
                                        });

        if ($activity_type_id) {
            $dailyReports = $dailyReports->get()->filter(function ($value) use ($activity_type_id) {
                if ($value->report->activity_type_id == $activity_type_id) {
                    return true;
                }

                return false;
            });
        }

        return $dailyReports;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            try {
                $file = $data['f1_document_url'];

                array_forget($data, 'f1_document_url'); // don't save temporary file
                $report = $this->programUpdateReport->create($data);
                $report->meta()->create($data);

                $report->f1_document_url = $this->uploadFile(self::REPORT_TYPE, $report->id, 'F1',
                    $data['community_id'], $file); // save file after file has been moved
                $report->save();
            } catch (Exception $e) {

            }

            return $report;
        });
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $report = $this->reportLog->findOrFail($id);

            try {
                if (isset($data['f1_document_url'])) {
                    $file = $data['f1_document_url'];

                    $data['f1_document_url'] = $this->uploadFile(self::REPORT_TYPE, $report->id, 'F1',
                        $data['community_id'], $file);
                }

                $data = array_merge($data, self::REPORT_PENDING_STATUS); // TODO experimental, PHP >= 5.6 only

                $report->update($data);
                $report->report->update($data);
            } catch (Exception $e) {

            }

            return $report;
        });
    }

    /**
     * @param bool $verified
     *
     * @return mixed
     */
    public function getAll($verified = false)
    {
        $reports = $this->reportLog->programUpdateReport();
        if ($verified) {
            $reports->verified();
        }

        return $reports->get();
    }
}