<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 12/15/2015
 * Time: 10:42 PM
 */

namespace App\Repositories;


use App\Models\ActivityType;
use App\Repositories\Contracts\ActivityTypeRepositoryInterface;

/**
 * Class EloquentActivityTypeRepository
 * @package App\Repositories
 */
class EloquentActivityTypeRepository implements ActivityTypeRepositoryInterface
{
    /**
     * @var ActivityType
     */
    private $activityType;

    /**
     * EloquentActivityTypeRepository constructor.
     *
     * @param ActivityType $activityType
     */
    public function __construct(ActivityType $activityType)
    {
        $this->activityType = $activityType;
    }

    /**
     * @return mixed
     */
    public function getDropdown()
    {
        $activityTypes = $this->activityType->lists('name', 'id');
        $activityTypes->prepend('', '');

        return $activityTypes;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $activityTypes = $this->activityType->all();

        return $activityTypes;
    }
}