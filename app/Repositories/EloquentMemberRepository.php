<?php

namespace App\Repositories;

use App\Models\Member;
use App\Repositories\Contracts\MemberRepositoryInterface;
use Carbon;
use DB;

/**
 * @property Member model
 */
class EloquentMemberRepository implements MemberRepositoryInterface
{
    /**
     * EloquentMemberRepository constructor.
     *
     * @param Member $member
     */
    public function __construct(Member $member)
    {
        $this->model = $member;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $member = $this->model->create($data);

            return $member;
        });
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $member = $this->find($id);
            $member->update($data);

            return $member;
        });
    }

    /**
     * @param $member_id
     *
     * @return mixed
     */
    public function find($member_id)
    {
        return $this->model->findOrFail($member_id);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * @param       $community_id
     * @param array $ids
     *
     * @return mixed
     */
    public function deactivate($community_id, array $ids)
    {
        return DB::transaction(function () use ($community_id, $ids) {
            $members = $this->model->whereIn('id', $ids);
            $members->update([
                'is_active'   => true,
                'inactive_at' => null,
            ]);
            $members = $this->model->whereCommunityId($community_id)
                                   ->whereNotIn('id', $ids)
                                   ->update([
                                       'is_active'   => false,
                                       'inactive_at' => Carbon::now(),
                                   ]);

            return $members;
        });
    }

    /**
     * @param $community_id
     *
     * @return mixed
     */
    public function getActiveMembers($community_id)
    {
        $memberCount = $this->model->whereCommunityId($community_id)->active();

        return $memberCount;
    }

    /**
     * @param $community_id
     *
     * @return mixed
     */
    public function getMembers($community_id)
    {
        $members = $this->model->whereCommunityId($community_id);

        return $members;
    }
}