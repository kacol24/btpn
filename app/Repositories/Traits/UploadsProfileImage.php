<?php

namespace App\Repositories\Traits;


use Carbon\Carbon;
use Intervention\Image\Facades\Image;

trait UploadsProfileImage
{
    protected function handleImageUpload($data)
    {
        if (isset($data['picture'])) {
            try {
                $image = $data['picture'];
                $image_name = md5(Carbon::now() . '_' . $image->getClientOriginalName());
                $file_name = $image_name . '.' . $image->getClientOriginalExtension();
                $file_path = public_path(config('btpn.profile_upload_directory') . $file_name);

                Image::make($image->getRealPath())->fit(500)->save($file_path);

                $data['picture'] = $file_name;
            } catch (Exception $e) {

            }
        }

        return $data;
    }
}