<?php

namespace App\Repositories;

use App\Models\City;
use App\Repositories\Contracts\CityRepositoryInterface;
use DB;

/**
 * Class EloquentCityRepository
 * @property City model
 * @package Repositories
 */
class EloquentCityRepository implements CityRepositoryInterface
{
    /**
     * @var City
     */
    protected $model;

    /**
     * EloquentCityRepository constructor.
     *
     * @param City $city
     */
    public function __construct(City $city)
    {
        $this->model = $city;
    }

    /**
     * @param array $data
     *
     * @return static
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $city = $this->model->create($data);

            return $city;
        });
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $city = $this->find($id);
            $city->update($data);

            return $city;
        });
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param array $columns
     *
     * @return mixed
     */
    public function getAll(array $columns = ['*'])
    {
        return $this->model->all($columns);
    }

    /**
     * @return mixed
     */
    public function getDropdown()
    {
        $dropdown = $this->model->lists('name', 'id');
        $dropdown->prepend('', '');

        return $dropdown;
    }
}