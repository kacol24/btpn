<?php

namespace App\Providers;

use App\Repositories\City\CityRepositoryInterface;
use App\Repositories\City\EloquentCityRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindInterface('City');
        $this->bindInterface('BusinessField');
        $this->bindInterface('Community');
        $this->bindInterface('Mms');
        $this->bindInterface('Facilitator');
        $this->bindInterface('User');
        $this->bindInterface('Member');
        $this->bindInterface('Sentra');
        $this->bindInterface('DailyReport');
        $this->bindInterface('ActivityType');
        $this->bindInterface('MonthlyReport');
        $this->bindInterface('ReportLog');
        $this->bindInterface('ProgramUpdateReport');
        $this->bindInterface('MemberStatusReport');
        $this->bindInterface('Aspect');
        $this->bindInterface('Level');
        $this->bindInterface('MaturityValue');
    }

    private function bindInterface($name)
    {
        $this->app->bind(
            'App\\Repositories\\Contracts\\' . $name . 'RepositoryInterface',
            'App\\Repositories\\Eloquent' . $name . 'Repository'
        );
    }
}
