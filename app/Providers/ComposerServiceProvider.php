<?php

namespace App\Providers;

use App\Http\Composers\BusinessFieldsDropdownComposer;
use App\Http\Composers\CitiesDropdownComposer;
use App\Http\Composers\CommunitiesDropdownForFacilitator;
use App\Http\Composers\CommunitiesDropdownForFacilitatorComposer;
use App\Http\Composers\MmsDropdownComposer;
use App\Http\Composers\SidebarBadgeComposer;
use Illuminate\Support\ServiceProvider;

/**
 * Class ComposerServiceProvider
 * @package App\Providers
 */
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeCitiesDropdown();

        $this->composeSidebarBadge();

        $this->composeBusinessFieldsDropdown();

        $this->composeCommunitiesDropdownForFacilitator();

        $this->composeMmsDropdown();
    }

    /**
     * Compose the cities dropdown for view partials
     */
    private function composeCitiesDropdown()
    {
        view()->composer(
            [
                'headquarters.communities.partials._form',
                'headquarters.mms.partials._form',
                'headquarters.mms.edit',
                'headquarters.facilitators.partials._form',
                'headquarters.members.partials._form',
                'headquarters.cmm.model.index',
                'headquarters.reports.index',
            ],
            CitiesDropdownComposer::class
        );
    }

    /**
     *
     */
    private function composeSidebarBadge()
    {
        view()->composer('includes._sidebar', SidebarBadgeComposer::class);
    }

    /**
     *
     */
    private function composeBusinessFieldsDropdown()
    {
        view()->composer(
            [
                'headquarters.communities.partials._form',
            ],
            BusinessFieldsDropdownComposer::class
        );
    }

    /**
     *
     */
    private function composeCommunitiesDropdownForFacilitator()
    {
        view()->composer(
            [
                'headquarters.facilitators.edit',
            ],
            CommunitiesDropdownForFacilitatorComposer::class);
    }

    /**
     *
     */
    private function composeMmsDropdown()
    {
        view()->composer(
            [
                'headquarters.sentra.partials._form',
                'headquarters.communities.edit',
            ],
            MmsDropdownComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
