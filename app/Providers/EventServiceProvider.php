<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // City module
        \App\Events\City\CityWasCreated::class                   => [
            \App\Listeners\City\LogCityCreated::class,
        ],
        \App\Events\City\CityWasUpdated::class                   => [
            \App\Listeners\City\LogCityUpdated::class,
        ],

        // Mms Module
        \App\Events\Mms\MmsWasCreated::class                     => [
            \App\Listeners\Mms\LogMmsCreated::class,
        ],
        \App\Events\Mms\MmsWasUpdated::class                     => [
            \App\Listeners\Mms\LogMmsUpdated::class,
        ],

        // Sentra Module
        \App\Events\Sentra\SentraWasCreated::class               => [
            \App\Listeners\Sentra\LogSentraCreated::class,
        ],
        \App\Events\Sentra\SentraWasUpdated::class               => [
            \App\Listeners\Sentra\LogSentraUpdated::class,
        ],

        // Community Module
        \App\Events\Community\CommunityWasCreated::class         => [
            \App\Listeners\Community\LogCommunityCreated::class,
        ],
        \App\Events\Community\CommunityWasUpdated::class         => [
            \App\Listeners\Community\LogCommunityUpdated::class,
        ],
        \App\Events\Community\CommunityWasDeactivated::class     => [
            \App\Listeners\Community\LogCommunityDeactivated::class,
        ],

        // Member Module
        \App\Events\Member\MemberWasCreated::class               => [
            \App\Listeners\Member\LogMemberCreated::class,
        ],
        \App\Events\Member\MemberWasUpdated::class               => [
            \App\Listeners\Member\LogMemberUpdated::class,
        ],

        // Facilitator Module
        \App\Events\Facilitator\FacilitatorWasCreated::class     => [
            \App\Listeners\Facilitator\LogFacilitatorCreated::class,
        ],
        \App\Events\Facilitator\FacilitatorWasUpdated::class     => [
            \App\Listeners\Facilitator\LogFacilitatorUpdated::class,
        ],
        \App\Events\Facilitator\FacilitatorWasDeactivated::class => [
            \App\Listeners\Facilitator\FlashFacilitatorDeactivated::class,
            \App\Listeners\Facilitator\LogFacilitatorDeactivated::class,
        ],

        // Reports
        \App\Events\Reports\DailyReportWasCreated::class         => [
            \App\Listeners\Reports\FlashDailyReportCreated::class,
            \App\Listeners\Reports\LogDailyReportCreated::class,
        ],
        \App\Events\Reports\ProgramUpdateReportWasCreated::class => [
            \App\Listeners\Reports\FlashProgramUpdateReportCreated::class,
            \App\Listeners\Reports\LogProgramUpdateReportCreated::class,
        ],
        \App\Events\Reports\MemberStatusReportWasCreated::class  => [
            \App\Listeners\Reports\FlashMemberStatusReportCreated::class,
            \App\Listeners\Reports\LogMemberStatusReportCreated::class,
        ],
        \App\Events\Reports\DailyReportWasUpdated::class         => [
            \App\Listeners\Reports\FlashDailyReportUpdated::class,
            \App\Listeners\Reports\LogDailyReportUpdated::class,
        ],
        \App\Events\Reports\ProgramUpdateReportWasUpdated::class => [
            \App\Listeners\Reports\FlashProgramUpdateReportUpdated::class,
            \App\Listeners\Reports\LogProgramUpdateReportUpdated::class,
        ],
        \App\Events\Reports\MemberStatusReportWasUpdated::class  => [
            \App\Listeners\Reports\FlashMemberStatusReportUpdated::class,
            \App\Listeners\Reports\LogMemberStatusReportUpdated::class,
        ],
        \App\Events\Reports\ReportWasDeclined::class             => [
            \App\Listeners\Reports\FlashReportDeclined::class,
            \App\Listeners\Reports\LogReportWasDeclined::class,
        ],
        \App\Events\Reports\ReportWasVerified::class             => [
            \App\Listeners\Reports\FlashReportVerified::class,
            \App\Listeners\Reports\LogReportWasVerified::class,
        ],
        \App\Events\Reports\ReportWasVoided::class               => [
            \App\Listeners\Reports\FlashReportVoided::class,
            \App\Listeners\Reports\LogReportVoided::class,
        ],

        // Community Maturity Model
        \App\Events\Maturity\AspectWasCreated::class             => [
            \App\Listeners\Maturity\LogAspectCreated::class,
        ],
        \App\Events\Maturity\LevelWasCreated::class              => [
            \App\Listeners\Maturity\LogLevelCreated::class,
        ],
        \App\Events\Maturity\MaturityValueWasCreated::class      => [
            \App\Listeners\Maturity\LogMaturityValueCreated::class,
        ],
        \App\Events\Maturity\AspectWasDeleted::class             => [
            \App\Listeners\Maturity\LogAspectDeleted::class,
        ],
        \App\Events\Maturity\LevelWasDeleted::class              => [
            \App\Listeners\Maturity\LogLevelDeleted::class,
        ],
        \App\Events\Maturity\MaturityValueWasDeleted::class      => [
            \App\Listeners\Maturity\LogMaturityValueDeleted::class,
        ],
        \App\Events\Maturity\MaturityModelWasSynced::class       => [
            \App\Listeners\Maturity\FlashMaturityModelSynced::class,
            \App\Listeners\Maturity\LogMaturityModelSynced::class,
        ],
        \App\Events\Maturity\AspectWasUpdated::class             => [
            \App\Listeners\Maturity\LogAspectUpdated::class,
        ],
        \App\Events\Maturity\LevelWasUpdated::class              => [
            \App\Listeners\Maturity\LogLevelUpdated::class,
        ],
        \App\Events\Maturity\MaturityValueWasUpdated::class      => [
            \App\Listeners\Maturity\LogMaturityValueUpdated::class,
        ],

        // Error handling
        \App\Events\Errors\CreateHasFailed::class                => [
            \App\Listeners\Errors\FlashCreateHasFailed::class,
            \App\Listeners\Errors\LogCreateHasFailed::class,
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     *
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
