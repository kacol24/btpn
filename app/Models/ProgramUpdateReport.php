<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProgramUpdateReport
 *
 * @property integer        $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramUpdateReport whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramUpdateReport whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProgramUpdateReport whereUpdatedAt($value)
 * @property-read mixed     $report_type
 */
class ProgramUpdateReport extends Model
{
    protected $fillable = ['id', 'f1_document_url', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function meta()
    {
        return $this->morphOne(ReportLog::class, 'report');
    }

    public function getReportTypeAttribute()
    {
        return 'Program Update Report';
    }

    public function dailyReports($period)
    {
        return $this->hasMany(DailyReport::class)->period($period);
    }
}
