<?php

namespace App\Models;

use Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportLog
 *
 * @package App\Models
 * @property integer                      $id
 * @property integer                      $facilitator_id
 * @property integer                      $community_id
 * @property string                       $period
 * @property boolean                      $is_verified
 * @property \Carbon\Carbon               $verified_at
 * @property \Carbon\Carbon               $declined_at
 * @property string                       $declined_reason
 * @property integer                      $report_id
 * @property string                       $report_type
 * @property \Carbon\Carbon               $created_at
 * @property \Carbon\Carbon               $updated_at
 * @property-read \App\Models\ReportLog   $report
 * @property-read \App\Models\Community   $community
 * @property-read \App\Models\Facilitator $facilitator
 * @property-read mixed                   $submitted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog pending()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog notPending()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog verified()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog declined()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog period($period)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog dailyReports()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog memberStatusReport()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog programUpdateReport()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog whereFacilitatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog whereCommunityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog wherePeriod($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog whereIsVerified($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog whereVerifiedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog whereDeclinedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog whereDeclinedReason($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog whereReportId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog whereReportType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReportLog extends Model
{
    /**
     * @var array
     */
    protected $dates = ['verified_at', 'declined_at'];
    /**
     * @var array
     */
    protected $casts = [
        'is_verified' => 'boolean',
    ];
    /**
     * @var array
     */
    protected $fillable = [
        'facilitator_id',
        'community_id',
        'period',
        'is_verified',
        'verified_at',
        'declined_at',
        'declined_reason',
        'report_id',
        'report_type',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function report()
    {
        return $this->morphTo();
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopePending($query)
    {
        return $query->where('is_verified', 0);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeNotPending($query)
    {
        return $query->where('is_verified', 1);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeVerified($query)
    {
        return $query->where('is_verified', 1)->whereNotNull('verified_at')->whereNull('declined_at');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeDeclined($query)
    {
        return $query->where('is_verified', 1)->whereNotNull('declined_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function facilitator()
    {
        return $this->belongsTo(Facilitator::class);
    }

    /**
     * @return string
     */
    public function getSubmittedAtAttribute()
    {
        return $this->created_at->toDateString();
    }

    /**
     * @param $query
     * @param $period
     * @param $yearOnly
     *
     * @return mixed
     */
    public function scopePeriod($query, $period, $yearOnly = false)
    {
        if ($yearOnly) {
            return $query->where(function ($query) use ($period) {
                $query->whereYear('period', '=', $period);
            });
        }

        return $query->where(function ($query) use ($period) {
            $period = Carbon::parse($period);
            $query->whereYear('period', '=', $period->year);
            $query->whereMonth('period', '=', $period->month);
        });
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeDailyReports($query)
    {
        return $query->where('report_type', 'App\Models\DailyReport');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeMemberStatusReport($query)
    {
        return $query->where('report_type', 'App\Models\MemberStatusReport');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeProgramUpdateReport($query)
    {
        return $query->where('report_type', 'App\Models\ProgramUpdateReport');
    }
}
