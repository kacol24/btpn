<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Aspect
 *
 * @property integer        $id
 * @property string         $name
 * @property string         $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MaturityValue[] $values
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Level[] $levels
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Aspect whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Aspect whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Aspect whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Aspect whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Aspect whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Aspect extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany(MaturityValue::class);
    }

    public function levels()
    {
        return $this->hasMany(Level::class);
    }
}
