<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Member
 *
 * @property integer                                                                        $id
 * @property integer                                                                        $sentra_id
 * @property integer                                                                        $community_id
 * @property string                                                                         $name
 * @property string                                                                         $phone
 * @property string                                                                         $address
 * @property boolean                                                                        $is_active
 * @property string                                                                         $inactive_at
 * @property boolean                                                                        $is_leader
 * @property \Carbon\Carbon                                                                 $created_at
 * @property \Carbon\Carbon                                                                 $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereSentraId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereCommunityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereInactiveAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereIsLeader($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereUpdatedAt($value)
 * @property integer                                                                        $city_id
 * @property integer                                                                        $mms_id
 * @property string                                                                         $account_number
 * @property-read Community                                                                 $community
 * @property-read Sentra                                                                    $sentra
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereMmsId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Member whereAccountNumber($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|DailyReport[]                    $daily_reports
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Member active()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MemberStatusReport[] $memberStatusReports
 * @mixin \Eloquent
 */
class Member extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'sentra_id',
        'community_id',
        'account_number',
        'name',
        'phone',
        'address',
        'is_active',
        'inactive_at',
        'is_leader',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
        'is_leader' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $dates = ['inactive_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sentra()
    {
        return $this->belongsTo(Sentra::class);
    }

    /**
     * @return $this
     */
    public function daily_reports()
    {
        return $this->belongsToMany(DailyReport::class)->withPivot('attended');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function memberStatusReports()
    {
        return $this->belongsToMany(MemberStatusReport::class)->withPivot('is_active');
    }

    public function getCityIdAttribute()
    {
        return $this->sentra->mms->city->id;
    }

    public function getMmsIdAttribute()
    {
        return $this->sentra->mms->id;
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }
}
