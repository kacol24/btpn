<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ActivityType
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ActivityType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ActivityType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ActivityType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ActivityType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ActivityType extends Model
{
    
}
