<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MonthlyReport
 *
 * @package App\Models
 * @property integer                                                     $id
 * @property integer                                                     $daily_report_id
 * @property string                                                      $period
 * @property \Carbon\Carbon                                              $created_at
 * @property \Carbon\Carbon                                              $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|DailyReport[] $daily_reports
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MonthlyReport whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MonthlyReport whereDailyReportId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MonthlyReport wherePeriod($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MonthlyReport whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MonthlyReport whereUpdatedAt($value)
 * @property integer $community_id
 * @property string $report_type
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MonthlyReport whereCommunityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MonthlyReport whereReportType($value)
 * @mixin \Eloquent
 */
class MonthlyReport extends Model
{
    protected $fillable = ['community_id', 'period'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function daily_reports()
    {
        return $this->hasMany(DailyReport::class);
    }
}
