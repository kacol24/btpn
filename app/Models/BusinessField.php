<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BusinessField
 *
 * @property integer                                                   $id
 * @property string                                                    $name
 * @property string                                                    $description
 * @property \Carbon\Carbon                                            $created_at
 * @property \Carbon\Carbon                                            $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Community[] $communities
 * @method static \Illuminate\Database\Query\Builder|\App\BusinessField whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BusinessField whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BusinessField whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BusinessField whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BusinessField whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BusinessField extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function communities()
    {
        return $this->hasMany(Community::class);
    }
}
