<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Sentra
 *
 * @property integer                                                $id
 * @property integer                                                $mms_id
 * @property string                                                 $name
 * @property string                                                 $email
 * @property string                                                 $phone
 * @property string                                                 $address
 * @property \Carbon\Carbon                                         $created_at
 * @property \Carbon\Carbon                                         $updated_at
 * @property-read Mms                                               $mms
 * @method static \Illuminate\Database\Query\Builder|\App\Sentra whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sentra whereMmsId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sentra whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sentra whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sentra wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sentra whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sentra whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sentra whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|Member[] $members
 * @mixin \Eloquent
 */
class Sentra extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['mms_id', 'name', 'phone', 'email', 'address'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mms()
    {
        return $this->belongsTo(Mms::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function members()
    {
        return $this->hasMany(Member::class);
    }
}
