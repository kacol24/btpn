<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Mms
 *
 * @property integer                                                   $id
 * @property integer                                                   $user_id
 * @property integer                                                   $city_id
 * @property string                                                    $address
 * @property \Carbon\Carbon                                            $created_at
 * @property \Carbon\Carbon                                            $updated_at
 * @property-read User                                                 $user
 * @property-read City                                                 $city
 * @property-read mixed                                                $name
 * @method static \Illuminate\Database\Query\Builder|\App\Mms whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Mms whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Mms whereCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Mms whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Mms whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Mms whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|Community[] $communities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Sentra[] $sentra
 * @mixin \Eloquent
 */
class Mms extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'city_id', 'address'];

    /**
     * @var array
     */
    protected $with = ['user'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function communities()
    {
        return $this->hasMany(Community::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sentra()
    {
        return $this->hasMany(Sentra::class);
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->user->name;
    }
}
