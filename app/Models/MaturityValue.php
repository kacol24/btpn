<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MaturityValue
 *
 * @package App\Models
 * @property integer                                                               $id
 * @property integer                                                               $aspect_id
 * @property integer                                                               $level_id
 * @property string                                                                $value
 * @property \Carbon\Carbon                                                        $created_at
 * @property \Carbon\Carbon                                                        $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Community[] $communities
 * @property-read \App\Models\Aspect                                               $aspect
 * @property-read \App\Models\Level                                                $level
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MaturityValue whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MaturityValue whereAspectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MaturityValue whereLevelId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MaturityValue whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MaturityValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MaturityValue whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MaturityValue extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['aspect_id', 'level_id', 'value'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function communities()
    {
        return $this->belongsToMany(Community::class)->withPivot('period');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aspect()
    {
        return $this->belongsTo(Aspect::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level()
    {
        return $this->belongsTo(Level::class);
    }
}
