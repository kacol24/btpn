<?php

namespace App\Models;

use Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DailyReport
 *
 * @property integer                                                $id
 * @property integer                                                $facilitator_id
 * @property integer                                                $community_id
 * @property integer                                                $activity_type_id
 * @property string                                                 $activity_name
 * @property string                                                 $location
 * @property string                                                 $date
 * @property string                                                 $start_time
 * @property string                                                 $end_time
 * @property string                                                 $achievements
 * @property string                                                 $problems
 * @property string                                                 $plans
 * @property \Carbon\Carbon                                         $created_at
 * @property \Carbon\Carbon                                         $updated_at
 * @property-read Facilitator                                       $facilitator
 * @property-read Community                                         $community
 * @property-read ActivityType                                      $activity_type
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereFacilitatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereCommunityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereActivityTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereActivityName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereStartTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereEndTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereAchievements($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereProblems($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport wherePlans($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|Member[] $members
 * @property-read MonthlyReport                                     $monthly_report
 * @property-read mixed                                             $report_type
 * @property-read \App\Models\ReportLog                             $report_data
 * @property-read mixed                                             $period
 * @property-read mixed                                             $duration
 * @property-read mixed                                             $participants
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport period($period)
 * @property string $f2_document_url
 * @property string $f3_document_url
 * @property string $supporting_document_url
 * @property-read \App\Models\ReportLog $meta
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereF2DocumentUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereF3DocumentUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DailyReport whereSupportingDocumentUrl($value)
 * @mixin \Eloquent
 */
class DailyReport extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'activity_type_id',
        'activity_name',
        'location',
        'start_time',
        'end_time',
        'achievements',
        'problems',
        'plans',
        'f2_document_url',
        'f3_document_url',
        'supporting_document_url',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function facilitator()
    {
        return $this->belongsTo(Facilitator::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activity_type()
    {
        return $this->belongsTo(ActivityType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function monthly_report()
    {
        return $this->belongsTo(MonthlyReport::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function report_data()
    {
        return $this->hasOne(ReportLog::class);
    }

    /**
     * @return string
     */
    public function getReportTypeAttribute()
    {
        return 'Daily Report';
    }

    /**
     * @return mixed
     */
    public function getPeriodAttribute()
    {
        return $this->meta->period;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function meta()
    {
        return $this->morphOne(ReportLog::class, 'report');
    }

    /**
     * @return int
     */
    public function getDurationAttribute()
    {
        $start_time = Carbon::parse($this->attributes['start_time']);
        $end_time = Carbon::parse($this->attributes['end_time']);

        return $start_time->diffInMinutes($end_time);
    }

    public function getParticipantsAttribute()
    {
        $members = $this->members();

        return $members->wherePivot('attended', 1)->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany(Member::class)->withPivot('attended');
    }

    /**
     * @param $query
     * @param $period
     *
     * @return mixed
     */
    public function scopePeriod($query, $period)
    {
        return $query->where(function ($query) use ($period) {
            $period = Carbon::parse($period);
            $query->whereYear('report_logs.period', '=', $period->year);
            $query->whereMonth('report_logs.period', '=', $period->month);
        });
    }
}
