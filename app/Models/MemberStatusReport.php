<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MemberStatusReport
 *
 * @property integer        $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MemberStatusReport whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MemberStatusReport whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MemberStatusReport whereUpdatedAt($value)
 * @property-read mixed     $report_type
 */
class MemberStatusReport extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id', 'f4_document_url', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function meta()
    {
        return $this->morphOne(ReportLog::class, 'report');
    }

    /**
     * @return string
     */
    public function getReportTypeAttribute()
    {
        return 'Member Status Report';
    }

    /**
     * @param $period
     *
     * @return mixed
     */
    public function dailyReports($period)
    {
        return $this->hasMany(DailyReport::class)->period($period);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany(Member::class)->withPivot('is_active');
    }
}
