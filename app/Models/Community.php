<?php

namespace App\Models;

use Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Community
 *
 * @property integer                                                     $id
 * @property integer                                                     $business_field_id
 * @property integer                                                     $mms_id
 * @property string                                                      $name
 * @property string                                                      $activation_date
 * @property \Carbon\Carbon                                              $deactivation_date
 * @property string                                                      $deactivation_reason
 * @property \Carbon\Carbon                                              $created_at
 * @property \Carbon\Carbon                                              $updated_at
 * @property-read BusinessField                                          $businessField
 * @property-read Facilitator                                            $facilitator
 * @property-read Mms                                                    $mms
 * @property-read mixed                                                  $city_id
 * @method static \Illuminate\Database\Query\Builder|\App\Community whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Community whereBusinessFieldId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Community whereMmsId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Community whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Community whereActivationDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Community whereDeactivationDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Community whereDeactivationReason($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Community whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Community whereUpdatedAt($value)
 * @property integer                                                     $facilitator_id
 * @property-read mixed                                                  $facilitator_name
 * @property-read mixed                                                  $gender
 * @method static \Illuminate\Database\Query\Builder|\App\Community whereFacilitatorId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|Member[]      $members
 * @property-read \Illuminate\Database\Eloquent\Collection|DailyReport[] $daily_reports
 */
class Community extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'mms_id',
        'business_field_id',
        'name',
        'activation_date',
        'deactivation_date',
        'deactivation_reason',
    ];

    /**
     * @var array
     */
    protected $dates = ['deactivation_date', 'activation_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function businessField()
    {
        return $this->belongsTo(BusinessField::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function facilitator()
    {
        return $this->belongsTo(Facilitator::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mms()
    {
        return $this->belongsTo(Mms::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function daily_reports()
    {
        return $this->hasMany(DailyReport::class);
    }

    /**
     * @return mixed
     */
    public function leader()
    {
        return $this->members()->where('is_leader', 1)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function members()
    {
        return $this->hasMany(Member::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function maturityValues()
    {
        return $this->belongsToMany(MaturityValue::class)->withPivot('period');
    }

    /**
     * @return int
     */
    public function getCityIdAttribute()
    {
        return $this->mms->city_id;
    }

    /**
     * @return array
     */
    public function getFacilitatorIdAttribute()
    {
        return $this->attributes['facilitator_id'];
    }

    /**
     * @return mixed
     */
    public function getFacilitatorNameAttribute()
    {
        return $this->facilitator->name;
    }

    /**
     * @return string
     */
    public function getGenderAttribute()
    {
        return $this->facilitator->gender == 'm' ? 'Male' : 'Female';
    }

    /**
     * @return array
     */
    public function getBusinessFieldIdAttribute()
    {
        return $this->attributes['business_field_id'];
    }

    /**
     * @return string
     */
    public function getActivationDateAttribute()
    {
        return Carbon::createFromFormat('Y-m-d', $this->attributes['activation_date'])->format('Y-m');
    }

    /**
     * @param $date
     */
    public function setActivationDateAttribute($date)
    {
        $this->attributes['activation_date'] = Carbon::createFromFormat('Y-m', $date)->format('Y-m-d');
    }


    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->whereNull('deactivation_date');
    }

    /**
     * @param $query
     * @param $facilitator_id
     *
     * @return mixed
     */
    public function scopeOwnedBy($query, $facilitator_id)
    {
        return $query->where('facilitator_id', $facilitator_id);
    }
}
