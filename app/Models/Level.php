<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Level
 *
 * @property integer                                                                   $id
 * @property string                                                                    $name
 * @property string                                                                    $description
 * @property \Carbon\Carbon                                                            $created_at
 * @property \Carbon\Carbon                                                            $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MaturityValue[] $values
 * @property-read \App\Models\Aspect                                                   $aspect
 * @property integer $score
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Level whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Level whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Level whereScore($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Level whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Level whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Level whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Level extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'score', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function values()
    {
        return $this->hasOne(MaturityValue::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function aspect()
    {
        return $this->belongsTo(Aspect::class);
    }
}
