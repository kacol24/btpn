<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Facilitator
 *
 * @property integer                                                                 $id
 * @property integer                                                                 $user_id
 * @property integer                                                                 $city_id
 * @property string                                                                  $dob
 * @property string                                                                  $gender
 * @property string                                                                  $address
 * @property \Carbon\Carbon                                                          $deactivation_date
 * @property \Carbon\Carbon                                                          $created_at
 * @property \Carbon\Carbon                                                          $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Community[]   $communities
 * @property-read \App\Models\User                                                   $user
 * @property-read \App\Models\City                                                   $city
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DailyReport[] $daily_reports
 * @property-read mixed                                                              $name
 * @property-read mixed                                                              $phone
 * @property-read mixed                                                              $email
 * @property-read mixed                                                              $username
 * @property-read mixed                                                              $community_id
 * @property-read mixed                                                              $picture
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Facilitator active()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Facilitator whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Facilitator whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Facilitator whereCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Facilitator whereDob($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Facilitator whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Facilitator whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Facilitator whereDeactivationDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Facilitator whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Facilitator whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Facilitator extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['dob', 'gender', 'address', 'deactivation_date'];

    /**
     * @var array
     */
    protected $with = ['user'];

    /**
     * @var array
     */
    protected $dates = ['deactivation_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function communities()
    {
        return $this->hasMany(Community::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function daily_reports()
    {
        return $this->hasMany(DailyReport::class);
    }


    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->user->name;
    }

    /**
     * @return string
     */
    public function getPhoneAttribute()
    {
        return $this->user->phone;
    }

    /**
     * @return string
     */
    public function getEmailAttribute()
    {
        return $this->user->email;
    }

    /**
     * @return string
     */
    public function getUsernameAttribute()
    {
        return $this->user->username;
    }

    /**
     * @return mixed
     */
    public function getCommunityIdAttribute()
    {
        return $this->communities->lists('id')->all();
    }

    /**
     * @return string
     */
    public function getPictureAttribute()
    {
        return $this->user->picture;
    }

    public function scopeActive($query)
    {
        return $query->whereNull('deactivation_date');
    }
}
