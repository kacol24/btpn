<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\City
 *
 * @property integer                                             $id
 * @property string                                              $name
 * @property string                                              $description
 * @property \Carbon\Carbon                                      $created_at
 * @property \Carbon\Carbon                                      $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Mms[] $mms
 * @method static \Illuminate\Database\Query\Builder|\App\City whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\City whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\City whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\City whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\City whereUpdatedAt($value)
 * @property-read Facilitator                                    $facilitator
 * @mixin \Eloquent
 */
class City extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mms()
    {
        return $this->hasMany(Mms::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function communities()
    {
        return $this->hasManyThrough(Community::class, Mms::class, 'city_id', 'mms_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function facilitator()
    {
        return $this->hasOne(Facilitator::class);
    }
}
