<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['auth'],
    'namespace'  => 'Api',
    'prefix'     => 'api',
    'as'         => 'api.',
], function () {
    Route::get('city/mms', 'ApiCitiesController@mms')->name('city.mms');
    Route::get('city/communities', 'ApiCitiesController@communities')->name('city.communities');
    Route::get('mms/sentra', 'ApiMmsController@sentra')->name('mms.sentra');
    Route::get('mms/communities', 'ApiMmsController@communities')->name('mms.communities');
    Route::get('communities/members',
        'ApiCommunitiesController@members')->name('facilitators.members');

    // Reports
    Route::post('reports/monthly-program-update',
        'ApiReportsController@reportMeta')->name('report.meta'); // we use post just so that the built in ajax validation works
    Route::get('reports/monthly-program-update/data',
        'ApiReportsController@reportData')->name('report.data'); // this is an api for populating datatables
    Route::post('reports/monthly-member-status', 'ApiReportsController@memberReportMeta')->name('member-report.meta');
    Route::get('reports/monthly-member-status/data',
        'ApiReportsController@memberReportData')->name('member-report.data');

    // Dashboard
    Route::group([
        'prefix' => 'dashboard',
        'as'     => 'dashboard.',
    ], function () {
        Route::get('annual-detail-view',
            'ApiDashboardController@annualDetailViewData')->name('annual-detail-view.data');
    });
});