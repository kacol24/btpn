<?php

Route::group([
    'middleware' => ['auth', 'role:headquarter'],
    'namespace'  => 'Headquarters',
], function () {
    /*
     * Cities
     */
    //Route::get('cities', 'CitiesController@index')->name('cities.index');
    //Route::post('cities', 'CitiesController@store')->name('cities.store');
    //Route::get('cities/{id}/edit', 'CitiesController@edit')->name('cities.edit');
    //Route::put('cities/{id}', 'CitiesController@update')->name('cities.update');
    Route::get('cities/datatables', 'CitiesController@getDatatables')->name('cities.datatables');
    Route::resource('cities', 'CitiesController', ['except' => ['create', 'destroy', 'show']]);

    /*
     * MMS
     */
    //Route::get('mms', 'MmsController@index')->name('mms.index');
    //Route::post('mms', 'MmsController@store')->name('mms.store');
    //Route::get('mms/{id}/edit', 'MmsController@edit')->name('mms.edit');
    //Route::put('mms/{id}', 'MmsController@update')->name('mms.update');
    Route::get('mms/datatables', 'MmsController@getDatatables')->name('mms.datatables');
    Route::resource('mms', 'MmsController', ['except' => ['create', 'destroy', 'show']]);

    /*
     * Sentra
     */
    Route::get('sentra/datatables', 'SentraController@getDatatables')->name('sentra.datatables');
    Route::resource('sentra', 'SentraController', ['except' => ['create', 'destroy', 'show']]);

    /*
     * Communities
     */
    Route::get('communities/datatables', 'CommunitiesController@getDatatables')->name('communities.datatables');
    Route::get('communities/{communities}/deactivate',
        'CommunitiesController@getDeactivate')->name('communities.deactivate');
    Route::delete('communities/{communities}/deactivate',
        'CommunitiesController@deleteDeactivate')->name('communities.deactivate');
    Route::resource('communities', 'CommunitiesController', ['except' => ['create', 'destroy']]);

    /*
     * Members
     */
    Route::get('members/datatables', 'MembersController@getDatatables')->name('members.datatables');
    Route::resource('members', 'MembersController', ['except' => ['create', 'destroy', 'show']]);

    /*
     * Facilitators
     */
    Route::get('facilitators/datatables', 'FacilitatorsController@getDatatables')->name('facilitators.datatables');
    Route::get('facilitators/{facilitators}/deactivate',
        'FacilitatorsController@deleteDeactivate')->name('facilitators.deactivate');
    Route::resource('facilitators', 'FacilitatorsController', ['except' => ['create', 'destroy', 'show']]);

    /*
     * Verification
     */
    Route::get('verifications', 'VerificationsController@index')->name('verifications.index');
    Route::get('verifications/{report}',
        'VerificationsController@show')->name('verifications.daily-report.show');
    Route::post('verifications/{report}/verify',
        'VerificationsController@verify')->name('verifications.daily-report.verify');
    Route::post('verifications/{report}/decline',
        'VerificationsController@decline')->name('verifications.daily-report.decline');

    /*
     * Reports
     */
    Route::get('reports', 'ReportsController@index')->name('reports.index');
    Route::post('reports', 'ReportsController@generateReport')->name('reports.generate');

    /*
     * CMM
     */
    Route::group([
        'as'     => 'cmm.',
        'prefix' => 'maturity-model',
    ], function () {
        Route::get('/', 'MaturityController@index')->name('index');

        Route::post('/aspect', 'MaturityController@aspectStore')->name('aspect.store');
        Route::get('/aspect/{aspect}/edit', 'MaturityController@aspectEdit')->name('aspect.edit');
        Route::put('/aspect/{aspect}', 'MaturityController@aspectUpdate')->name('aspect.update');
        Route::delete('/aspect/{aspect}', 'MaturityController@aspectDestroy')->name('aspect.destroy');

        Route::post('/level', 'MaturityController@levelStore')->name('level.store');
        Route::get('/level/{level}/edit', 'MaturityController@levelEdit')->name('level.edit');
        Route::put('/level/{level}', 'MaturityController@levelUpdate')->name('level.update');
        Route::delete('/level/{level}', 'MaturityController@levelDestroy')->name('level.destroy');

        Route::post('/value', 'MaturityController@valueStore')->name('value.store');
        Route::get('/value/{value}/edit', 'MaturityController@valueEdit')->name('value.edit');
        Route::put('/value/{value}', 'MaturityController@valueUpdate')->name('value.update');
        Route::delete('/value/{value}', 'MaturityController@valueDestroy')->name('value.destroy');

        Route::get('community-maturity', 'MaturityController@modelIndex')->name('model.index');
        Route::post('community-maturity', 'MaturityController@modelShow')->name('model.show');
        Route::put('community-maturity/{community}', 'MaturityController@modelSync')->name('model.sync');
    });
});