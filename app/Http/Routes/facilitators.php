<?php

Route::group([
    'middleware' => ['auth', 'role:facilitator'],
    'namespace'  => 'Facilitators',
    'as'         => 'facilitators.',
    'prefix'     => 'facilitators',
], function () {
    /*
     * Communities
     */
    Route::get('communities/datatables',
        'CommunitiesController@getDatatables')->name('communities.datatables');
    Route::get('communities', 'CommunitiesController@index')->name('communities.index');
    Route::get('communities/{communities}', 'CommunitiesController@show')->name('communities.show');

    /*
     * Reports
     */
    Route::group([
        'as'     => 'reports.',
        'prefix' => 'reports',
    ], function () {
        Route::get('{report}/edit', 'ReportsController@edit')->name('edit');

        // Daily Report
        Route::get('daily-report', 'ReportsController@dailyReportIndex')->name('daily-report.index');
        Route::post('daily-report', 'ReportsController@dailyReportStore')->name('daily-report.store');
        Route::put('daily-report/{report}', 'ReportsController@dailyReportUpdate')->name('daily-report.update');

        // Program Update Report
        Route::get('monthly-program-update',
            'ReportsController@monthlyProgramUpdateIndex')->name('monthly-report.program-update.index');
        Route::post('monthly-program-update',
            'ReportsController@monthlyProgramUpdateStore')->name('monthly-report.program-update.store');
        Route::put('monthly-program-update/{report}',
            'ReportsController@programUpdateUpdate')->name('monthly-report.program-update.update');

        // Member Status Report
        Route::get('monthly-member-status',
            'ReportsController@monthlyMemberStatusIndex')->name('monthly-report.member-status.index');
        Route::post('monthly-member-status',
            'ReportsController@monthlyMemberStatusStore')->name('monthly-report.member-status.store');
        Route::put('monthly-member-status/{report}',
            'ReportsController@memberStatusUpdate')->name('monthly-report.member-status.update');

        Route::get('check-document-status',
            'ReportsController@checkDocumentStatusIndex')->name('check-document-status.index');
        Route::delete('check-document-status/{report}', 'ReportsController@destroy')->name('destroy');
    });
});