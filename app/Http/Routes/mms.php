<?php

Route::group([
    'middleware' => ['auth', 'role:mms'],
    'namespace'  => 'Mms',
    'as'         => 'mms.',
    'prefix'     => 'mms',
], function () {
    // Sentra
    Route::get('sentra/datatables', 'SentraController@getDatatables')->name('sentra.datatables');
    Route::get('sentra', 'SentraController@index')->name('sentra.index');

    // Communities
    Route::get('communities/datatables', 'CommunitiesController@getDatatables')->name('communities.datatables');
    Route::get('communities/{communities}', 'CommunitiesController@show')->name('communities.show');
    Route::get('communities', 'CommunitiesController@index')->name('communities.index');

    // Reports
    Route::group([
        'prefix' => 'reports',
        'as'     => 'reports.',
    ], function () {
        Route::get('member-status', 'ReportsController@memberStatusIndex')->name('member-status.index');
        Route::get('program-update', 'ReportsController@programUpdateIndex')->name('program-update.index');
    });
});