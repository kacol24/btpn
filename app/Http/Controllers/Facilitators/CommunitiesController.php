<?php

namespace App\Http\Controllers\Facilitators;

use App\Community;
use App\Facilitator;
use App\Http\Controllers\Contracts\DisplayDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\FacilitatorRepositoryInterface;
use App\Repositories\Contracts\ReportLogRepositoryInterface;
use Auth;
use yajra\Datatables\Datatables;

/**
 * Class CommunitiesController
 * @package App\Http\Controllers\Facilitators
 */
class CommunitiesController extends Controller implements DisplayDatatable
{
    private $community_repo;
    private $repo;
    /**
     * @var ReportLogRepositoryInterface
     */
    private $reportLog;

    /**
     * CommunitiesController constructor.
     *
     * @param FacilitatorRepositoryInterface $facilitator
     * @param CommunityRepositoryInterface   $community
     * @param ReportLogRepositoryInterface   $reportLog
     */
    public function __construct(
        FacilitatorRepositoryInterface $facilitator,
        CommunityRepositoryInterface $community,
        ReportLogRepositoryInterface $reportLog
    ) {
        $this->repo = $facilitator;
        $this->community_repo = $community;
        $this->reportLog = $reportLog;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('facilitators.communities.index', compact('communities'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $facilitator = $this->repo->getLoggedIn();
        $community = $facilitator->communities()->whereId($id)->firstOrFail();
        $community_leader = $community->members->where('is_leader', true)->first();

        $reports = $this->reportLog->getDailyReportsOfCommunity($id);

        return view('facilitators.communities.show', compact('community', 'community_leader', 'reports'));
    }

    /**
     * @param Datatables $datatables
     *
     * @return mixed
     */
    public function getDatatables(Datatables $datatables)
    {
        $facilitator_id = Auth::user()->facilitator->id;
        $communities = $this->repo->getCommunitiesById($facilitator_id);

        return $datatables->of($communities)
                          ->addColumn('actions', function ($communities) {
                              $buttons[] = '<a href="' . route('facilitators.communities.show',
                                      $communities->id) . '" class="btn btn-xs btn-primary ajax-modal-xl" data-title="' . $communities->name . '"><i class="fa fa-eye"></i> View</a>';

                              return implode(' ', $buttons);
                          })
                          ->make(true);
    }
}
