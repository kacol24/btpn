<?php

namespace App\Http\Controllers\Facilitators;

use App\Http\Controllers\Controller;
use App\Jobs\Reports\CreateDailyReportCommand;
use App\Jobs\Reports\CreateMemberStatusReportCommand;
use App\Jobs\Reports\CreateProgramUpdateReportCommand;
use App\Jobs\Reports\UpdateDailyReportCommand;
use App\Jobs\Reports\UpdateMemberStatusReportCommand;
use App\Jobs\Reports\UpdateProgramUpdateReportCommand;
use App\Jobs\Reports\VoidReportCommand;
use App\Models\DailyReport;
use App\Models\MemberStatusReport;
use App\Models\ProgramUpdateReport;
use App\Models\ReportLog;
use App\Repositories\Contracts\ActivityTypeRepositoryInterface;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\DailyReportRepositoryInterface;
use App\Repositories\Contracts\MemberStatusReportRepositoryInterface;
use App\Repositories\Contracts\ProgramUpdateReportRepositoryInterface;
use App\Repositories\Contracts\ReportLogRepositoryInterface;
use Carbon\Carbon;
use Input;
use JavaScript;

/**
 * Class ReportsController
 * @package App\Http\Controllers\Facilitators
 */
class ReportsController extends Controller
{
    /**
     * @var CommunityRepositoryInterface
     */
    private $communities;
    /**
     * @var ActivityTypeRepositoryInterface
     */
    private $activityTypes;
    /**
     * @var ReportLog
     */
    private $reportLog;
    /**
     * @var ProgramUpdateReport
     */
    private $programUpdateReport;
    /**
     * @var DailyReport
     */
    private $dailyReport;
    /**
     * @var MemberStatusReportRepositoryInterface
     */
    private $memberStatusReport;

    /**
     * ReportsController constructor.
     *
     * @param CommunityRepositoryInterface           $communities
     * @param ActivityTypeRepositoryInterface        $activityTypes
     * @param ProgramUpdateReportRepositoryInterface $programUpdateReport
     * @param DailyReportRepositoryInterface         $dailyReport
     * @param ReportLogRepositoryInterface           $reportLog
     * @param MemberStatusReportRepositoryInterface  $memberStatusReport
     */
    public function __construct(
        CommunityRepositoryInterface $communities,
        ActivityTypeRepositoryInterface $activityTypes,
        ProgramUpdateReportRepositoryInterface $programUpdateReport,
        DailyReportRepositoryInterface $dailyReport,
        ReportLogRepositoryInterface $reportLog,
        MemberStatusReportRepositoryInterface $memberStatusReport
    ) {
        $this->communities = $communities;
        $this->activityTypes = $activityTypes;
        $this->programUpdateReport = $programUpdateReport;
        $this->dailyReport = $dailyReport;
        $this->reportLog = $reportLog;
        $this->memberStatusReport = $memberStatusReport;
    }

    /**
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dailyReportIndex()
    {
        $facilitator_id = auth()->user()->facilitator->id;
        $communities = $this->communities->getDropdownOwnedByFacilitator($facilitator_id);

        if ($community_id = Input::get('community_id')) {
            $members = $this->communities->getMembers($community_id);
        }

        $activityTypes = $this->activityTypes->getDropdown();

        $data = [
            'communities'    => $communities,
            'members'        => isset($members) ? $members : false,
            'activity_types' => $activityTypes,
        ];

        return view('facilitators.reports.daily-reports.index', $data);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function dailyReportStore()
    {
        $this->dispatch(new CreateDailyReportCommand());

        return redirect()->back();
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->dispatch(new VoidReportCommand($id));

        return redirect()->back();
    }

    /**
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function monthlyProgramUpdateIndex()
    {
        $activityTypes = $this->activityTypes->getAll();
        $facilitator = auth()->user()->facilitator;
        $communitiesDropdown = $this->communities->getDropdownOwnedByFacilitator($facilitator->id); // TODO extract to composer

        JavaScript::put([
            'activity_types' => $activityTypes->toArray(),
        ]);

        return view('facilitators.reports.monthly-reports.program-update.index',
            compact('activityTypes', 'communitiesDropdown'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function monthlyProgramUpdateStore()
    {
        $this->dispatch(new CreateProgramUpdateReportCommand());

        return redirect()->back();
    }


    /**
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function monthlyMemberStatusIndex()
    {
        $facilitator = auth()->user()->facilitator;
        $communitiesDropdown = $this->communities->getDropdownOwnedByFacilitator($facilitator->id); // TODO extract to composer

        return view('facilitators.reports.monthly-reports.member-status.index', compact('communitiesDropdown'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function monthlyMemberStatusStore()
    {
        $this->dispatch(new CreateMemberStatusReportCommand());

        return redirect()->back();
    }

    /**
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function checkDocumentStatusIndex()
    {
        $facilitator_id = auth()->user()->facilitator->id;
        $reports = $this->reportLog->getWhereFacilitator($facilitator_id)->get();

        $pendingReports = $reports->filter(function ($item) {
            if ($item->is_verified == 0) {
                return true;
            }

            return false;
        });
        $declinedReports = $reports->filter(function ($item) {
            if ($item->is_verified && $item->declined_at) {
                return true;
            }

            return false;
        });
        $verifiedReports = $reports->filter(function ($item) {
            if ($item->is_verified && $item->verified_at && $item->declined_at == null) {
                return true;
            }

            return false;
        });

        return view('facilitators.reports.check-document-status.index',
            compact('pendingReports', 'verifiedReports', 'declinedReports'));
    }

    /**
     * @param $id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $report = $this->reportLog->find($id);

        if ($report->report instanceof DailyReport) {
            $data = $this->dailyReportEditData($report->report);

            return view('facilitators.reports.daily-reports.edit', $data);
        } elseif ($report->report instanceof ProgramUpdateReport) {
            $data = $this->programUpdateEditData($report);

            return view('facilitators.reports.monthly-reports.program-update.edit', $data);
        } elseif ($report->report instanceof MemberStatusReport) {
            $data = $this->memberStatusEditData($report);

            return view('facilitators.reports.monthly-reports.member-status.edit', $data);
        }
    }

    /**
     * @param DailyReport $report
     *
     * @return array
     */
    private function dailyReportEditData(DailyReport $report)
    {
        $facilitator = auth()->user()->facilitator;
        $communities = $this->communities->getDropdownForFacilitator($facilitator);

        if ($community_id = $report->meta->community_id) {
            $members = $this->communities->getMembers($community_id);
        }

        $activityTypes = $this->activityTypes->getDropdown();

        $data = [
            'report'         => $report,
            'communities'    => $communities,
            'members'        => isset($members) ? $members : false,
            'activity_types' => $activityTypes,
        ];

        return $data;
    }

    /**
     * @param ReportLog $report
     *
     * @return array
     */
    private function programUpdateEditData(ReportLog $report)
    {
        $activityTypes = $this->activityTypes->getAll();
        $facilitator = $report->facilitator;
        $community_id = $report->community_id;
        $communitiesDropdown = $this->communities->getDropdownForFacilitator($facilitator);

        $period = Carbon::parse($report->period);

        $activities = $this->programUpdateReport->getDailyReports($community_id, $period);

        $data = [
            'report'              => $report,
            'period'              => $period->format('Y-m'),
            'activityTypes'       => $activityTypes,
            'communitiesDropdown' => $communitiesDropdown,
            'community_leader'    => $report->community->leader()->name,
            'business_field'      => $report->community->businessField->name,
            'member_count'        => $report->community->members->count(),
            'activities'          => $activities->get(),
            'total_activities'    => $activities->count(),
        ];

        return $data;
    }


    /**
     * @param $report
     *
     * @return array
     */
    private function memberStatusEditData($report)
    {
        $period = Carbon::parse($report->period);
        $facilitator = $report->facilitator;
        $communitiesDropdown = $this->communities->getDropdownForFacilitator($facilitator);
        $members = $report->report->members;

        return [
            'report'              => $report,
            'period'              => $period->format('Y-m'),
            'communitiesDropdown' => $communitiesDropdown,
            'members'             => $members,
        ];
    }


    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function dailyReportUpdate($id)
    {
        $this->dispatch(new UpdateDailyReportCommand($id));

        return redirect()->route('facilitators.reports.check-document-status.index');
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function programUpdateUpdate($id)
    {
        $this->dispatch(new UpdateProgramUpdateReportCommand($id));

        return redirect()->route('facilitators.reports.check-document-status.index');
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function memberStatusUpdate($id)
    {
        $this->dispatch(new UpdateMemberStatusReportCommand($id));

        return redirect()->route('facilitators.reports.check-document-status.index');
    }
}
