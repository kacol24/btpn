<?php

namespace App\Http\Controllers\Mms;

use App\Community;
use App\Http\Controllers\Contracts\DisplayDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Mms;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\MmsRepositoryInterface;
use yajra\Datatables\Datatables;

/**
 * Class CommunitiesController
 * @package App\Http\Controllers\Mms
 */
class CommunitiesController extends Controller implements DisplayDatatable
{
    /**
     * CommunitiesController constructor.
     *
     * @param CommunityRepositoryInterface $communities
     * @param MmsRepositoryInterface       $mms
     */
    public function __construct(CommunityRepositoryInterface $communities, MmsRepositoryInterface $mms)
    {
        $this->repo = $communities;
        $this->mms_repo = $mms;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('mms.communities.index');
    }

    /**
     * @param                        $id
     * @param MmsRepositoryInterface $mms_repo
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, MmsRepositoryInterface $mms_repo)
    {
        $mms = $mms_repo->getLoggedIn();
        $community = $mms->communities()->whereId($id)->firstOrFail();
        $community_leader = $community->members->where('is_leader', true)->first();

        return view('mms.communities.show', compact('community', 'community_leader'));
    }

    /**
     * @param Datatables             $datatables
     *
     * @param MmsRepositoryInterface $mms_repo
     *
     * @return mixed
     */
    public function getDatatables(Datatables $datatables)
    {
        $mms_id = auth()->user()->mms->id;
        $communities = $this->mms_repo->getCommunitiesById($mms_id);

        return $datatables->of($communities)
                          ->addColumn('actions', function ($communities) {
                              $buttons[] = '<a href="' . route('mms.communities.show',
                                      $communities->id) . '" class="btn btn-xs btn-primary ajax-modal-lg" data-title="' . $communities->name . '"><i class="fa fa-eye"></i> View</a>';

                              return implode(' ', $buttons);
                          })
                          ->make(true);
    }
}
