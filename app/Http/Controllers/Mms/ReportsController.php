<?php

namespace App\Http\Controllers\Mms;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Community;
use App\Repositories\Contracts\ActivityTypeRepositoryInterface;
use App\Repositories\Contracts\CommunityRepositoryInterface;

class ReportsController extends Controller
{
    /**
     * @var ActivityTypeRepositoryInterface
     */
    private $activityTypes;
    /**
     * @var CommunityRepositoryInterface
     */
    private $communities;

    /**
     * ReportsController constructor.
     *
     * @param ActivityTypeRepositoryInterface $activityTypes
     * @param CommunityRepositoryInterface    $communities
     */
    public function __construct(
        ActivityTypeRepositoryInterface $activityTypes,
        CommunityRepositoryInterface $communities
    ) {
        $this->activityTypes = $activityTypes;
        $this->communities = $communities;
    }

    public function memberStatusIndex()
    {
        $mms_id = auth()->user()->mms->id;

        $communitiesDropdown = Community::whereMmsId($mms_id)->lists('name', 'id');
        $communitiesDropdown->prepend('', ''); // TODO extract to composer

        return view('mms.reports.member-status.index', compact('communitiesDropdown'));
    }

    public function programUpdateIndex()
    {
        $mms_id = auth()->user()->mms->id;

        $activityTypes = $this->activityTypes->getAll();
        $communitiesDropdown = Community::whereMmsId($mms_id)->lists('name', 'id');
        $communitiesDropdown->prepend('', ''); // TODO extract to composer

        return view('mms.reports.program-update.index', compact('activityTypes', 'communitiesDropdown'));
    }
}
