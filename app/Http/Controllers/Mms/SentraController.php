<?php

namespace App\Http\Controllers\Mms;

use App\Http\Controllers\Contracts\DisplayDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\Contracts\SentraRepositoryInterface;
use yajra\Datatables\Datatables;

/**
 * Class SentraController
 * @package App\Http\Controllers\Mms
 */
class SentraController extends Controller implements DisplayDatatable
{
    public function __construct(SentraRepositoryInterface $sentra)
    {
        $this->repo = $sentra;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('mms.sentra.index');
    }

    /**
     * @param Datatables $datatables
     *
     * @return mixed
     */
    public function getDatatables(Datatables $datatables)
    {
        $mms_id = auth()->user()->mms->id;
        $sentra = $this->repo->getByMmsId($mms_id);

        return $datatables->of($sentra)->make(true);
    }
}
