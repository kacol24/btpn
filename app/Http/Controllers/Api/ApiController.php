<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 12/17/2015
 * Time: 3:25 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\HandlesAjax;

abstract class ApiController extends Controller
{
    use HandlesAjax;
}