<?php

namespace App\Http\Controllers\Api;

use App\Community;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\SentraRepositoryInterface;
use App\Sentra;
use Input;

/**
 * Class ApiMmsController
 * @package App\Http\Controllers\Api
 */
class ApiMmsController extends Controller
{
    /**
     * @var SentraRepositoryInterface
     */
    private $sentra_repo;
    /**
     * @var CommunityRepositoryInterface
     */
    private $community_repo;

    /**
     * ApiMmsController constructor.
     *
     * @param SentraRepositoryInterface    $sentra_repo
     * @param CommunityRepositoryInterface $community_repo
     */
    public function __construct(SentraRepositoryInterface $sentra_repo, CommunityRepositoryInterface $community_repo)
    {
        $this->sentra_repo = $sentra_repo;
        $this->community_repo = $community_repo;
    }

    /**
     * @return mixed
     */
    public function sentra()
    {
        $mms_id = Input::get('mms_id');

        return $this->sentra_repo->getDropdownByMmsId($mms_id);
    }

    /**
     * @return mixed
     */
    public function communities()
    {
        $mms_id = Input::get('mms_id');

        return $this->community_repo->getDropdownWhereMmsId($mms_id);
    }
}
