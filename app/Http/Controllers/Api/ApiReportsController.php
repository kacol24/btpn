<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Requests\Reports\MemberStatusParameterRequest;
use App\Http\Requests\Reports\ProgramUpdateParameterRequest;
use App\Repositories\Contracts\ActivityTypeRepositoryInterface;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\DailyReportRepositoryInterface;
use App\Repositories\Contracts\MemberRepositoryInterface;
use App\Repositories\Contracts\ProgramUpdateReportRepositoryInterface;
use Carbon;
use Input;
use yajra\Datatables\Datatables;

/**
 * Class ApiReportsController
 * @package App\Http\Controllers\Api
 */
class ApiReportsController extends ApiController
{
    /**
     * @var CommunityRepositoryInterface
     */
    private $communities;
    /**
     * @var ActivityTypeRepositoryInterface
     */
    private $activityTypes;
    /**
     * @var DailyReportRepositoryInterface
     */
    private $dailyReport;
    /**
     * @var ProgramUpdateReportRepositoryInterface
     */
    private $programUpdate;
    /**
     * @var MemberRepositoryInterface
     */
    private $members;

    /**
     * ApiReportsController constructor.
     *
     * @param CommunityRepositoryInterface           $communities
     * @param ActivityTypeRepositoryInterface        $activityTypes
     * @param DailyReportRepositoryInterface         $dailyReport
     * @param ProgramUpdateReportRepositoryInterface $programUpdate
     * @param MemberRepositoryInterface              $members
     */
    public function __construct(
        CommunityRepositoryInterface $communities,
        ActivityTypeRepositoryInterface $activityTypes,
        DailyReportRepositoryInterface $dailyReport,
        ProgramUpdateReportRepositoryInterface $programUpdate,
        MemberRepositoryInterface $members
    ) {
        $this->communities = $communities;
        $this->activityTypes = $activityTypes;
        $this->dailyReport = $dailyReport;
        $this->programUpdate = $programUpdate;
        $this->members = $members;
    }

    /**
     * @param ProgramUpdateParameterRequest $request
     *
     * @return mixed
     */
    public function reportMeta(ProgramUpdateParameterRequest $request)
    {
        $community_id = $request->community_id;
        $period = Carbon::parse($request->period);

        $community = $this->communities->findWithRelations($community_id, ['businessField', 'members']);

        $verifiedOnly = true;
        $report = $this->programUpdate->getDailyReports($community_id, $period, null, $verifiedOnly);

        $total_activities = $report->count();
        if ($total_activities < 1) {
            return response()->json(['period' => 'There are no reports within this period'], 422);
        }

        $total_participants = 0;
        foreach ($report->get() as $item) {
            $members = $item->report->members;
            foreach ($members as $member) {
                if ($member->pivot->attended) {
                    $total_participants++;
                }
            }
        }

        return $this->transform([
            'business_field'     => $community->businessField,
            'member_count'       => $community->members->count(),
            'community_leader'   => $community->members()->where('is_leader', 1)->first() ?: [],
            'community_city'     => $community->mms->city->name,
            'facilitator_name'   => $community->facilitator->name,
            'total_activities'   => $total_activities,
            'total_participants' => $total_participants,
        ]);
    }

    /**
     * @param Datatables $datatables
     *
     * @return mixed
     */
    public function reportData(Datatables $datatables)
    {
        $community_id = Input::get('community_id');
        $activity_type_id = Input::get('activity_type_id');
        $period = Input::get('period');

        $report = $this->programUpdate->getDailyReports($community_id, $period, $activity_type_id, true);

        return $datatables->of($report)
                          ->addColumn('duration', function ($report) {
                              $start_time = Carbon::createFromFormat('H:i:s', $report->report->start_time);
                              $end_time = Carbon::createFromFormat('H:i:s', $report->report->end_time);
                              $duration = $end_time->diffInMinutes($start_time);

                              return $duration;
                          })
                          ->addColumn('participant_count', function ($report) {
                              $members = $report->report->members;
                              $member_count = 0;
                              foreach ($members as $member) {
                                  if ($member->pivot->attended) {
                                      $member_count++;
                                  }
                              }

                              return $member_count;
                          })
                          ->editColumn('date', function ($report) {
                              return $report->period;
                          })
                          ->make(true);
    }

    /**
     * @param MemberStatusParameterRequest $request
     *
     * @return mixed
     */
    public function memberReportMeta(MemberStatusParameterRequest $request)
    {
        $community_id = $request->community_id;
        $members = $this->communities->getActiveMembers($community_id)->count();

        $community = $this->communities->find($community_id);

        return $this->transform([
            'active_members_count' => $members,
            'facilitator_name'     => $community->facilitator->name,
            'city'                 => $community->mms->city->name,
        ]);
    }

    /**
     * @param Datatables $datatables
     *
     * @return mixed
     */
    public function memberReportData(Datatables $datatables)
    {
        $community_id = Input::get('community_id');
        $member_status = Input::get('member_status');

        $members = $this->communities->getMembers($community_id);

        return $datatables->of($members)
                          ->addColumn('sentra', function ($members) {
                              return $members->sentra->name;
                          })
                          ->addColumn('status', function ($members) use ($member_status) {
                              if ($members->is_active) {
                                  if ($member_status) {
                                      return '<span class="label label-success">Active</span>';
                                  }

                                  return '<input form="report_parameter_form" type="checkbox" name="active[' . $members->id . ']" class="icheck" value="1" checked>';
                              }

                              if ($member_status) {
                                  return '<span class="label label-danger">Inactive</span>';
                              }

                              return '<input form="report_parameter_form" type="checkbox" name="active[' . $members->id . ']" class="icheck" value="1">';
                          })
                          ->make(true);
    }
}
