<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use Input;
use yajra\Datatables\Datatables;

/**
 * Class ApiCommunitiesController
 * @package App\Http\Controllers\Api
 */
class ApiCommunitiesController extends Controller
{
    /**
     * @var CommunityRepositoryInterface
     */
    private $communities;

    /**
     * ApiCommunitiesController constructor.
     *
     * @param CommunityRepositoryInterface $communities
     */
    public function __construct(CommunityRepositoryInterface $communities)
    {

        $this->communities = $communities;
    }

    /**
     * @param            $community_id
     * @param Datatables $datatables
     */
    public function members(Datatables $datatables)
    {
        $community_id = Input::get('community_id');
        $report_id = Input::get('report_id', 0);
        $members = $this->communities->getMembers($community_id);

        return $datatables->of($members)
                          ->addColumn('status', function ($members) {
                              if ($members->is_active) {
                                  return '<span class="label label-success">Active</span>';
                              }

                              return '<span class="label label-danger">Inactive</span>';
                          })
                          ->addColumn('attendance', function ($members) use ($report_id) {
                              if ($report_id != 0) {
                                  $member_data = $members->daily_reports()->find($report_id);
                              }
                              if (isset($member_data) && $member_data->pivot->attended) {
                                  return '<input type="checkbox" name="attendance[' . $members->id . ']" class="icheck" value="1" checked>';
                              }

                              return '<input type="checkbox" name="attendance[' . $members->id . ']" class="icheck" value="1">';
                          })
                          ->make(true);
    }
}
