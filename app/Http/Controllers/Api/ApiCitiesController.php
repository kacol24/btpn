<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Mms;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\MmsRepositoryInterface;
use Input;

/**
 * Class ApiCitiesController
 * @package App\Http\Controllers\Api
 */
class ApiCitiesController extends Controller
{
    /**
     * @var MmsRepositoryInterface
     */
    private $mms_repo;
    /**
     * @var CommunityRepositoryInterface
     */
    private $community_repo;

    /**
     * ApiCitiesController constructor.
     *
     * @param MmsRepositoryInterface       $mms_repo
     * @param CommunityRepositoryInterface $community_repo
     */
    public function __construct(MmsRepositoryInterface $mms_repo, CommunityRepositoryInterface $community_repo)
    {
        $this->mms_repo = $mms_repo;
        $this->community_repo = $community_repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $city_id
     *
     * @return \Illuminate\Http\Response
     */
    public function mms()
    {
        $city_id = Input::get('city_id');

        $mms = $this->mms_repo->dropdownFromCityId($city_id);
        $mms->prepend('', '');

        return $mms;
    }

    /**
     * @return array
     */
    public function communities()
    {
        $city_id = Input::get('city_id');
        if ($city_id == '') {
            return [];
        }

        return $this->community_repo->getDropdownByCityIdWhereFacilitatorNull($city_id);
    }
}
