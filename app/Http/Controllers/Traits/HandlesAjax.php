<?php

namespace App\Http\Controllers\Traits;

trait HandlesAjax
{
    public function transform($data)
    {
        $return['data'] = $data;

        return $return;
    }
}