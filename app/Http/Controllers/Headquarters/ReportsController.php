<?php

namespace App\Http\Controllers\Headquarters;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\ReportLog;
use App\Repositories\Contracts\ActivityTypeRepositoryInterface;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\MemberStatusReportRepositoryInterface;
use App\Repositories\Contracts\ProgramUpdateReportRepositoryInterface;
use App\Repositories\Contracts\ReportLogRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JavaScript;

class ReportsController extends Controller
{
    /**
     * @var CommunityRepositoryInterface
     */
    private $communities;
    /**
     * @var ActivityTypeRepositoryInterface
     */
    private $activityTypes;
    /**
     * @var ReportLog
     */
    private $reportLog;
    /**
     * @var ProgramUpdateReportRepositoryInterface
     */
    private $programUpdateReport;
    /**
     * @var MemberStatusReportRepositoryInterface
     */
    private $memberStatusReport;

    /**
     * ReportsController constructor.
     *
     * @param CommunityRepositoryInterface           $communities
     * @param ActivityTypeRepositoryInterface        $activityTypes
     * @param ProgramUpdateReportRepositoryInterface $programUpdateReport
     * @param MemberStatusReportRepositoryInterface  $memberStatusReport
     * @param ReportLogRepositoryInterface           $reportLog
     *
     */
    public function __construct(
        CommunityRepositoryInterface $communities,
        ActivityTypeRepositoryInterface $activityTypes,
        ProgramUpdateReportRepositoryInterface $programUpdateReport,
        MemberStatusReportRepositoryInterface $memberStatusReport,
        ReportLogRepositoryInterface $reportLog
    ) {
        $this->communities = $communities;
        $this->activityTypes = $activityTypes;
        $this->programUpdateReport = $programUpdateReport;
        $this->memberStatusReport = $memberStatusReport;
        $this->reportLog = $reportLog;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('headquarters.reports.index');
    }

    public function generateReport(Request $request)
    {
        $community_id = $request->community_id;
        $period = Carbon::parse($request->period);

        $community = $this->communities->find($community_id);

        $activities = $this->programUpdateReport->getDailyReports($community_id, $period, null, true);
        if (! $activities->count()) {
            return redirect()->back()->withInput($request->all())->withErrors(['period' => 'There are no reports within this period']);
        }

        $members = $this->memberStatusReport->getMembers($community_id, $period);

        $activity_types = $this->activityTypes->getAll();
        $activity_data = [];
        foreach ($activity_types as $activity_type) {
            $counter = array_where($activities->get()->toArray(), function ($key, $value) use ($activity_type) {
                return $value['report']['activity_type_id'] == $activity_type->id;
            });
            $activity_data[] = count($counter);
        }

        $data = [
            'community'      => $community,
            'period'         => $period,
            'activity_types' => $activity_types,
            'activities'     => $activities->get(),
            'members'        => $members,
            'facilitator'    => $activities->first()->facilitator,
        ];

        JavaScript::put([
            'activities' => [
                'headers' => $activity_types->lists('name'),
                'pieData' => $this->formatDataForPieChart($activity_data, $activity_types),
                'barData' => $this->formatDataForBarChart($activity_data, $activity_types),
            ],
        ]);

        if ($request->report_type == 'generate') {
            $data['preview'] = true;

            return view('headquarters.reports.show', $data);
        }
        $data['preview'] = false;

        return view('headquarters.reports.print', $data);
    }

    protected function formatDataForPieChart($activityData, $activityTypes)
    {
        $data = $activityTypes->map(function ($item, $key) use ($activityData) {
            return [
                'name' => $item->name,
                'y'    => $activityData[$key],
            ];
        });

        return $data;
    }

    protected function formatDataForBarChart($activityData, $activityTypes)
    {
        $data = $activityTypes->map(function ($item, $key) use ($activityData) {
            return [
                'name' => $item->name,
                'data' => [$activityData[$key]],
            ];
        });

        return $data;
    }
}
