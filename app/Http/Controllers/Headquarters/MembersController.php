<?php

namespace App\Http\Controllers\Headquarters;

use App\Community;
use App\Http\Controllers\Contracts\DisplayDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\Member\CreateMemberCommand;
use App\Jobs\Member\UpdateMemberCommand;
use App\Member;
use App\Mms;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\MemberRepositoryInterface;
use App\Repositories\Contracts\MmsRepositoryInterface;
use App\Repositories\Contracts\SentraRepositoryInterface;
use App\Sentra;
use yajra\Datatables\Datatables;

/**
 * Class MembersController
 * @package App\Http\Controllers\Headquarters
 */
class MembersController extends Controller implements DisplayDatatable
{
    /**
     * MembersController constructor.
     *
     * @param MemberRepositoryInterface $member
     */
    public function __construct(MemberRepositoryInterface $member)
    {
        $this->repo = $member;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mms = [];
        $sentra = [];
        $communities = [];

        return view('headquarters.members.index', compact('mms', 'sentra', 'communities'));
    }

    /**
     * Store a newly created resource in storage.
     * @return \Illuminate\Http\Response
     * @internal param \Illuminate\Http\Request $request
     *
     */
    public function store()
    {
        $this->dispatch(new CreateMemberCommand());

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                         $id
     * @param MmsRepositoryInterface       $mms_repo
     * @param CommunityRepositoryInterface $community_repo
     *
     * @param SentraRepositoryInterface    $sentra_repo
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(
        $id,
        MmsRepositoryInterface $mms_repo,
        CommunityRepositoryInterface $community_repo,
        SentraRepositoryInterface $sentra_repo
    ) {
        $member = $this->repo->find($id);

        // TODO extract these to composer
        $mms = $mms_repo->dropdownFromCityId($member->sentra->mms->city->id);

        $sentra = $sentra_repo->getDropdownByMmsId($member->sentra->mms->id);

        $communities = $community_repo->getDropdownWhereMmsId($member->sentra->mms->id);

        return view('headquarters.members.edit', compact('member', 'mms', 'sentra', 'communities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @internal param MembersRequest|\Illuminate\Http\Request $request
     */
    public function update($id)
    {
        $this->dispatch(new UpdateMemberCommand($id));

        return redirect()->back();
    }

    /**
     * @param Datatables $datatables
     *
     * @return mixed
     */
    public function getDatatables(Datatables $datatables)
    {
        $members = $this->repo->getAll();

        return $datatables->of($members)
                          ->addColumn('actions', function ($members) {
                              return '<a
                                    href="' . route('members.edit', $members->id) . '" class="btn btn-xs btn-success ajax-modal bootstrap-modal-form-open"
                                    data-title="' . $members->name . '">
                                    <i class="fa fa-edit"></i> Edit
                                    </a>';
                          })
                          ->make(true);
    }
}
