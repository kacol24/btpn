<?php

namespace App\Http\Controllers\Headquarters;

use App\City;
use App\Http\Controllers\Contracts\Datatable;
use App\Http\Controllers\Contracts\DisplayDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\City\CreateCityCommand;
use App\Jobs\City\UpdateCityCommand;
use App\Repositories\Contracts\CityRepositoryInterface;
use yajra\Datatables\Datatables;

/**
 * Class CitiesController
 * @package App\Http\Controllers\Headquarters
 */
class CitiesController extends Controller implements DisplayDatatable
{
    public function __construct(CityRepositoryInterface $city)
    {
        $this->repo = $city;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('headquarters.cities.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->dispatch(new CreateCityCommand());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = $this->repo->find($id);

        return view('headquarters.cities.edit', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @internal param \Illuminate\Http\Request $request
     */
    public function update($id)
    {
        $this->dispatch(new UpdateCityCommand($id));
    }

    /**
     * @param Datatables $datatables
     *
     * @return mixed
     */
    public function getDatatables(Datatables $datatables)
    {
        $cities = $this->repo->getAll();

        return $datatables->of($cities)
                          ->addColumn('actions', function ($cities) {
                              return '<a
                                    href="' . route('cities.edit', $cities->id) . '" class="btn btn-xs btn-success ajax-modal bootstrap-modal-form-open"
                                    data-title="' . $cities->name . '">
                                    <i class="fa fa-edit"></i> Edit
                                    </a>';
                          })
                          ->make(true);
    }
}
