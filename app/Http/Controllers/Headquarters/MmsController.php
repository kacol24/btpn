<?php

namespace App\Http\Controllers\Headquarters;

use App\City;
use App\Http\Controllers\Contracts\Datatable;
use App\Http\Controllers\Contracts\DisplayDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\Mms\CreateMmsCommand;
use App\Jobs\Mms\UpdateMmsCommand;
use App\Mms;
use App\Repositories\Contracts\MmsRepositoryInterface;
use App\User;
use yajra\Datatables\Datatables;

/**
 * Class MmsController
 * @package App\Http\Controllers\Headquarters
 */
class MmsController extends Controller implements DisplayDatatable
{
    private $repo;

    /**
     * MmsController constructor.
     *
     * @param MmsRepositoryInterface $mms
     */
    public function __construct(MmsRepositoryInterface $mms)
    {
        $this->repo = $mms;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('headquarters.mms.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->dispatch(new CreateMmsCommand());

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mms = $this->repo->findJoin($id, ['city']);

        return view('headquarters.mms.edit', compact('mms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->dispatch(new UpdateMmsCommand($id));

        return redirect()->back();
    }

    /**
     * @param Datatables $datatables
     *
     * @return mixed
     */
    public function getDatatables(Datatables $datatables)
    {
        $mms = $this->repo->getWithRelations(['city']);

        return $datatables->of($mms)
                          ->addColumn('actions', function ($mms) {
                              return '<a
                                    href="' . route('mms.edit', $mms->id) . '" class="btn btn-xs btn-success ajax-modal bootstrap-modal-form-open"
                                    data-title="' . $mms->user->name . '">
                                    <i class="fa fa-edit"></i> Edit
                                    </a>';
                          })
                          ->make(true);
    }
}
