<?php

namespace App\Http\Controllers\Headquarters;

use App\Http\Controllers\Contracts\DisplayDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\Facilitator\CreateFacilitatorCommand;
use App\Jobs\Facilitator\DeactivateFacilitatorCommand;
use App\Jobs\Facilitator\UpdateFacilitatorCommand;
use App\Repositories\Contracts\FacilitatorRepositoryInterface;
use yajra\Datatables\Datatables;

/**
 * Class FacilitatorsController
 * @package App\Http\Controllers\Headquarters
 */
class FacilitatorsController extends Controller implements DisplayDatatable
{
    /**
     * @var FacilitatorRepositoryInterface
     */
    protected $repo;

    /**
     * FacilitatorsController constructor.
     *
     * @param FacilitatorRepositoryInterface $facilitator
     */
    public function __construct(FacilitatorRepositoryInterface $facilitator)
    {
        $this->repo = $facilitator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $communities = [];

        return view('headquarters.facilitators.index', compact('communities'));
    }

    /**
     *
     */
    public function store()
    {
        $this->dispatch(new CreateFacilitatorCommand());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function edit($id)
    {
        $facilitator = $this->repo->findJoin($id, ['communities']);

        return view('headquarters.facilitators.edit', compact('facilitator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->dispatch(new UpdateFacilitatorCommand($id));
    }

    public function deleteDeactivate($id)
    {
        $this->dispatch(new DeactivateFacilitatorCommand($id));

        return redirect()->back();
    }

    /**
     * @param Datatables $datatables
     *
     * @return mixed
     */
    public function getDatatables(Datatables $datatables)
    {
        $facilitators = $this->repo->getAll(true);

        return $datatables->of($facilitators)
                          ->setRowClass(function ($facilitators) {
                              return $facilitators->deactivation_date != null ? 'bg-danger' : '';
                          })
                          ->addColumn('actions', function ($facilitators) {
                              $buttons[] = '
                              <a href="' . route('facilitators.edit', $facilitators->id) . '" class="btn btn-xs btn-success ajax-modal-lg bootstrap-modal-form-open"
                                    data-title="' . $facilitators->user->name . '">
                                    <i class="fa fa-edit"></i> Edit
                                    </a>
                              ';
                              if (! $facilitators->deactivation_date) {
                                  $buttons[] = '
                                  <a href="' . route('facilitators.deactivate',
                                          $facilitators->id) . '"class="btn btn-xs btn-danger"><i class="fa fa-close"></i> Deactivate</a>
                                  ';
                              }

                              return implode(' ', $buttons);
                          })
                          ->make(true);
    }
}
