<?php

namespace App\Http\Controllers\Headquarters;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\HandlesAjax;
use App\Http\Requests;
use App\Jobs\Reports\DeclineReportCommand;
use App\Jobs\Reports\VerifyReportCommand;
use App\Models\DailyReport;
use App\Models\MemberStatusReport;
use App\Models\ProgramUpdateReport;
use App\Models\ReportLog;
use App\Repositories\Contracts\ActivityTypeRepositoryInterface;
use App\Repositories\Contracts\ReportLogRepositoryInterface;
use Carbon;

/**
 * Class VerificationsController
 * @package App\Http\Controllers\Headquarters
 */
class VerificationsController extends Controller
{
    use HandlesAjax;

    /**
     * @var ReportLogRepositoryInterface
     */
    private $reportLog;

    /**
     * VerificationsController constructor.
     *
     * @param ReportLogRepositoryInterface $reportLog
     */
    public function __construct(ReportLogRepositoryInterface $reportLog)
    {
        $this->reportLog = $reportLog;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ReportLog $reportLog
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ReportLog $reportLog)
    {
        $pendingReports = $reportLog->pending()->orderBy('created_at', 'desc')->get();
        $verifiedReports = $reportLog->notPending()->orderBy('created_at', 'desc')->get();

        return view('headquarters.verifications.index', compact('pendingReports', 'verifiedReports'));
    }

    /**
     * @param                                 $id
     *
     * @param ActivityTypeRepositoryInterface $activityTypes
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, ActivityTypeRepositoryInterface $activityTypes)
    {
        $report = $this->reportLog->find($id);

        if ($report->is_verified) {
            return back();
        }
        if ($report->report instanceof DailyReport) {
            $data = $this->dailyReportShowData($report);

            $view = view('headquarters.verifications.show.daily-report');
        } else if ($report->report instanceof ProgramUpdateReport) {
            $data = $this->programUpdateReportShowData($activityTypes, $report);

            $view = view('headquarters.verifications.show.program-update');
        } else if ($report->report instanceof MemberStatusReport) {
            $data = $this->memberStatusReportShowData($report);

            $view = view('headquarters.verifications.show.member-status');
        }

        return $view->with($data);
    }

    /**
     * @param $report
     *
     * @return array
     */
    private function dailyReportShowData($report)
    {
        $data = [
            'report' => $report,
        ];

        return $data;
    }

    /**
     * @param ActivityTypeRepositoryInterface $activityTypes
     * @param                                 $report
     *
     * @return array
     */
    private function programUpdateReportShowData(ActivityTypeRepositoryInterface $activityTypes, $report)
    {
        $dailyReports = $this->reportLog
            ->getDailyReportsWithinPeriod($report->period)->get();
        $activity_type = $activityTypes->getAll();

        $data = [
            'report'        => $report,
            'activityTypes' => $activity_type,
            'dailyReports'  => $dailyReports,
        ];

        return $data;
    }

    /**
     * @param $report
     *
     * @return array
     */
    private function memberStatusReportShowData($report)
    {
        $members = $report->report->members;
        $data = [
            'report'        => $report,
            'members'       => $members,
            'activeMembers' => $members->where('is_active', true)->count(),
        ];

        return $data;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify($id)
    {
        $this->dispatch(new VerifyReportCommand($id));

        return redirect()->route('verifications.index');
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function decline($id)
    {
        $this->dispatch(new DeclineReportCommand($id));

        return $this->transform(['url' => route('verifications.index')]);
    }
}
