<?php

namespace App\Http\Controllers\Headquarters;

use App\Http\Controllers\Contracts\DisplayDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\Sentra\CreateSentraCommand;
use App\Jobs\Sentra\UpdateSentraCommand;
use App\Mms;
use App\Repositories\Contracts\SentraRepositoryInterface;
use App\Sentra;
use yajra\Datatables\Datatables;

/**
 * Class SentraController
 * @package App\Http\Controllers\Headquarters
 */
class SentraController extends Controller implements DisplayDatatable
{
    /**
     * @var SentraRepositoryInterface
     */
    private $repo;

    /**
     * SentraController constructor.
     *
     * @param SentraRepositoryInterface $sentra
     */
    public function __construct(SentraRepositoryInterface $sentra)
    {
        $this->repo = $sentra;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     *
     */
    public function index()
    {
        return view('headquarters.sentra.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->dispatch(new CreateSentraCommand());

        return redirect()->back();
    }

    /**
     * @param $id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $sentra = $this->repo->find($id);

        return view('headquarters.sentra.edit', compact('sentra'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $this->dispatch(new UpdateSentraCommand($id));

        return redirect()->back();
    }

    /**
     * @param Datatables $datatables
     *
     * @return mixed
     */
    public function getDatatables(Datatables $datatables)
    {
        $sentras = $this->repo->getJoin(['mms', 'mms.user']);

        return $datatables->of($sentras)
                          ->addColumn('actions', function ($sentras) {
                              return '<a
                                  href="' . route('sentra.edit', $sentras->id) . '"
                                  class="btn btn-xs btn-success ajax-modal bootstrap-modal-form-open"
                                  data-title="' . $sentras->name . '">
                                        <i class="fa fa-edit"></i> Edit
                                  </a>';
                          })
                          ->make(true);
    }
}
