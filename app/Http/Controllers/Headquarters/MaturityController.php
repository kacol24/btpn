<?php

namespace App\Http\Controllers\Headquarters;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\Maturity\CreateAspectCommand;
use App\Jobs\Maturity\CreateLevelCommand;
use App\Jobs\Maturity\CreateMaturityValueCommand;
use App\Jobs\Maturity\DeleteAspectCommand;
use App\Jobs\Maturity\DeleteLevelCommand;
use App\Jobs\Maturity\DeleteMaturityValueCommand;
use App\Jobs\Maturity\SyncMaturityModelCommand;
use App\Jobs\Maturity\UpdateAspectCommand;
use App\Jobs\Maturity\UpdateLevelCommand;
use App\Jobs\Maturity\UpdateMaturityValueCommand;
use App\Repositories\Contracts\AspectRepositoryInterface;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\LevelRepositoryInterface;
use App\Repositories\Contracts\MaturityValueRepositoryInterface;
use App\Repositories\Contracts\MmsRepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class MaturityController
 * @package App\Http\Controllers\Headquarters
 */
class MaturityController extends Controller
{
    /**
     * @var Aspect
     */
    private $aspect;
    /**
     * @var Level
     */
    private $level;
    /**
     * @var MaturityValue
     */
    private $maturityValue;
    /**
     * @var MmsRepositoryInterface
     */
    private $mms;
    /**
     * @var CommunityRepositoryInterface
     */
    private $communities;

    /**
     * MaturityController constructor.
     *
     * @param AspectRepositoryInterface        $aspect
     * @param LevelRepositoryInterface         $level
     * @param MaturityValueRepositoryInterface $maturityValue
     * @param MmsRepositoryInterface           $mms
     * @param CommunityRepositoryInterface     $communities
     */
    public function __construct(
        AspectRepositoryInterface $aspect,
        LevelRepositoryInterface $level,
        MaturityValueRepositoryInterface $maturityValue,
        MmsRepositoryInterface $mms,
        CommunityRepositoryInterface $communities
    ) {
        $this->aspect = $aspect;
        $this->level = $level;
        $this->maturityValue = $maturityValue;
        $this->mms = $mms;
        $this->communities = $communities;
    }

    /**
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $aspects = $this->aspect->getAll();
        $levels = $this->level->getAll();
        $values = $this->maturityValue->getAll();

        $aspectsDropdown = $this->aspect->getUnassignedDropdown();
        $levelsDropdown = $this->level->getDropdown();

        return view('headquarters.cmm.index',
            compact('aspects', 'levels', 'values', 'aspectsDropdown', 'levelsDropdown'));
    }

    /**
     *
     */
    public function aspectStore()
    {
        $this->dispatch(new CreateAspectCommand());
    }

    /**
     * @param $id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function aspectEdit($id)
    {
        $aspect = $this->aspect->find($id);

        return view('headquarters.cmm.aspect.edit', compact('aspect'));
    }

    /**
     * @param $id
     */
    public function aspectUpdate($id)
    {
        $this->dispatch(new UpdateAspectCommand($id));
    }

    /**
     * @param $id
     */
    public function aspectDestroy($id)
    {
        $this->dispatch(new DeleteAspectCommand($id));
    }

    /**
     *
     */
    public function levelStore()
    {
        $this->dispatch(new CreateLevelCommand());
    }

    /**
     * @param $id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function levelEdit($id)
    {
        $level = $this->level->find($id);

        return view('headquarters.cmm.level.edit', compact('level'));
    }

    /**
     * @param $id
     */
    public function levelUpdate($id)
    {
        $this->dispatch(new UpdateLevelCommand($id));
    }

    /**
     * @param $id
     */
    public function levelDestroy($id)
    {
        $this->dispatch(new DeleteLevelCommand($id));
    }

    /**
     *
     */
    public function valueStore()
    {
        $this->dispatch(new CreateMaturityValueCommand());
    }

    /**
     * @param $id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function valueEdit($id)
    {
        $aspectsDropdown = $this->aspect->getDropdown($id);
        $levelsDropdown = $this->level->getDropdown($id);

        $value = $this->maturityValue->find($id);

        return view('headquarters.cmm.value.edit', compact('value', 'aspectsDropdown', 'levelsDropdown'));
    }

    /**
     * @param $id
     */
    public function valueUpdate($id)
    {
        $this->dispatch(new UpdateMaturityValueCommand($id));
    }

    /**
     * @param $id
     */
    public function valueDestroy($id)
    {
        $this->dispatch(new DeleteMaturityValueCommand($id));
    }

    /**
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function modelIndex()
    {
        return view('headquarters.cmm.model.index');
    }

    /**
     * @param Request $request
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function modelShow(Request $request)
    {
        $aspects = $this->aspect->getAll();
        $levels = $this->level->getAll();
        $values = $this->maturityValue->getAll();

        $mms = $this->mms->dropdownFromCityId($request->city_id);
        $communities = $this->communities->getDropdownWhereMmsId($request->mms_id);
        $maturity = $this->communities->getMaturityValues($request->community_id);

        $data = [
            'aspects'      => $aspects,
            'levels'       => $levels,
            'values'       => $values,
            'city_id'      => $request->city_id,
            'mms_id'       => $request->mms_id,
            'community_id' => $request->community_id,
            'mms'          => $mms,
            'communities'  => $communities,
            'maturity'     => $maturity,
        ];

        return view('headquarters.cmm.model.index', $data);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modelSync($id)
    {
        $this->dispatch(new SyncMaturityModelCommand($id));

        return redirect()->back();
    }
}
