<?php

namespace App\Http\Controllers\Headquarters;

use App\BusinessField;
use App\Community;
use App\Http\Controllers\Contracts\DisplayDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\Community\CreateCommunityCommand;
use App\Jobs\Community\DeactivateCommunityCommand;
use App\Jobs\Community\UpdateCommunityCommand;
use App\Mms;
use App\Repositories\Contracts\AspectRepositoryInterface;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\LevelRepositoryInterface;
use App\Repositories\Contracts\MaturityValueRepositoryInterface;
use App\Repositories\Contracts\ReportLogRepositoryInterface;
use Carbon;
use yajra\Datatables\Datatables;

/**
 * Class CommunitiesController
 * @package App\Http\Controllers\Headquarters
 */
class CommunitiesController extends Controller implements DisplayDatatable
{
    protected $repo;
    /**
     * @var ReportLogRepositoryInterface
     */
    private $reportLog;
    /**
     * @var AspectRepositoryInterface
     */
    private $aspect;
    /**
     * @var LevelRepositoryInterface
     */
    private $level;
    /**
     * @var MaturityValueRepositoryInterface
     */
    private $maturityValue;

    /**
     * CommunitiesController constructor.
     *
     * @param CommunityRepositoryInterface     $community
     * @param ReportLogRepositoryInterface     $reportLog
     * @param AspectRepositoryInterface        $aspect
     * @param LevelRepositoryInterface         $level
     * @param MaturityValueRepositoryInterface $maturityValue
     */
    public function __construct(
        CommunityRepositoryInterface $community,
        ReportLogRepositoryInterface $reportLog,
        AspectRepositoryInterface $aspect,
        LevelRepositoryInterface $level,
        MaturityValueRepositoryInterface $maturityValue
    ) {
        $this->repo = $community;
        $this->reportLog = $reportLog;
        $this->aspect = $aspect;
        $this->level = $level;
        $this->maturityValue = $maturityValue;
    }

    /**
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $mmsDropdown = [];
        $businessFields = [];

        return view('headquarters.communities.index', compact('mmsDropdown', 'businessFields'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function store()
    {
        $this->dispatch(new CreateCommunityCommand());

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aspects = $this->aspect->getAll();
        $levels = $this->level->getAll();
        $values = $this->maturityValue->getAll();
        $maturity = $this->repo->getMaturityValues($id);

        $community = $this->repo->find($id);
        $facilitator = $community->facilitator;
        $community_leader = $community->members->where('is_leader', true)->first();
        $members = $community->members;

        $reports = $this->reportLog->getDailyReportsOfCommunity($id);

        return view('headquarters.communities.show', compact(
            'community',
            'facilitator',
            'community_leader',
            'members',
            'reports',
            'aspects',
            'levels',
            'values',
            'maturity'
        ));
    }

    /**
     * @param $id
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $community = $this->repo->findWithRelations($id, ['mms', 'businessField', 'mms.city']);

        return view('headquarters.communities.edit', compact('community'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @internal param CommunityRequest $request
     */
    public function update($id)
    {
        $this->dispatch(new UpdateCommunityCommand($id));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getDeactivate($id)
    {
        $community = $this->repo->find($id);

        return view('headquarters.communities.deactivate', compact('community'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteDeactivate($id)
    {
        $this->dispatch(new DeactivateCommunityCommand($id));

        return redirect()->back();
    }

    /**
     * @param Datatables $datatables
     *
     * @return mixed
     */
    public function getDatatables(Datatables $datatables)
    {
        $communities = $this->repo->getWithRelations(['facilitator', 'mms', 'businessField']);

        return $datatables->of($communities)
                          ->addColumn('actions', function ($communities) {
                              $buttons[] = '<a href="' . route('communities.show',
                                      $communities->id) . '" class="btn btn-xs btn-primary ajax-modal-xl" data-title="' . $communities->name . '"><i class="fa fa-eye"></i> View</a>';
                              $buttons[] = '<a href="' . route('communities.edit',
                                      $communities->id) . '" class="btn btn-xs btn-success ajax-modal bootstrap-modal-form-open" data-title="' . $communities->name . '"><i class="fa fa-edit"></i> Edit</a>';
                              if (! $communities->deactivation_date) {
                                  $buttons[] = '<a href="' . route('communities.deactivate',
                                          $communities->id) . '"class="btn btn-xs btn-danger ajax-modal" data-title="Deactivate ' . $communities->name . '?"><i class="fa fa-close"></i> Deactivate</a>';
                              }

                              return implode(' ', $buttons);
                          })
                          ->setRowClass(function ($communities) {
                              return $communities->deactivation_date != null ? 'bg-danger' : '';
                          })
                          ->make(true);
    }
}
