<?php

namespace App\Http\Controllers\Contracts;

use yajra\Datatables\Datatables;

/**
 * Interface DisplayDatatable
 * @package App\Http\Controllers\Contracts
 */
interface DisplayDatatable
{
    /**
     * @param Datatables $datatables
     *
     * @return mixed
     */
    public function getDatatables(Datatables $datatables);
}