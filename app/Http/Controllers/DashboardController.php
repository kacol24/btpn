<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\Contracts\ActivityTypeRepositoryInterface;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use App\Repositories\Contracts\LevelRepositoryInterface;
use App\Repositories\Contracts\MemberRepositoryInterface;
use App\Repositories\Contracts\MemberStatusReportRepositoryInterface;
use App\Repositories\Contracts\ProgramUpdateReportRepositoryInterface;
use App\Repositories\Contracts\ReportLogRepositoryInterface;
use App\Repositories\Contracts\SentraRepositoryInterface;
use Carbon;
use Input;
use JavaScript;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * @var CommunityRepositoryInterface
     */
    private $communities;
    /**
     * @var SentraRepositoryInterface
     */
    private $sentra;
    /**
     * @var ProgramUpdateReportRepositoryInterface
     */
    private $programUpdateReport;
    /**
     * @var MemberStatusReportRepositoryInterface
     */
    private $memberStatusReport;
    /**
     * @var LevelRepositoryInterface
     */
    private $levels;
    /**
     * @var ReportLogRepositoryInterface
     */
    private $reportLogs;
    /**
     * @var ActivityTypeRepositoryInterface
     */
    private $activityTypes;
    /**
     * @var MemberRepositoryInterface
     */
    private $members;

    /**
     * DashboardController constructor.
     *
     * @param CommunityRepositoryInterface           $communities
     * @param SentraRepositoryInterface              $sentra
     * @param ProgramUpdateReportRepositoryInterface $programUpdateReport
     * @param MemberStatusReportRepositoryInterface  $memberStatusReport
     * @param LevelRepositoryInterface               $levels
     * @param ReportLogRepositoryInterface           $reportLogs
     * @param ActivityTypeRepositoryInterface        $activityTypes
     * @param MemberRepositoryInterface              $members
     */
    public function __construct(
        CommunityRepositoryInterface $communities,
        SentraRepositoryInterface $sentra,
        ProgramUpdateReportRepositoryInterface $programUpdateReport,
        MemberStatusReportRepositoryInterface $memberStatusReport,
        LevelRepositoryInterface $levels,
        ReportLogRepositoryInterface $reportLogs,
        ActivityTypeRepositoryInterface $activityTypes,
        MemberRepositoryInterface $members
    ) {
        $this->communities = $communities;
        $this->sentra = $sentra;
        $this->programUpdateReport = $programUpdateReport;
        $this->memberStatusReport = $memberStatusReport;
        $this->levels = $levels;
        $this->reportLogs = $reportLogs;
        $this->activityTypes = $activityTypes;
        $this->members = $members;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     * @internal param Community $community
     *
     */
    public function index()
    {
        $data = [];

        $currentYear = Carbon::now()->year;

        $communities = $this->communities->getAll();

        $data['communities'] = $communities;
        $data['sentra'] = $this->sentra->getAll();
        $data['programUpdateReports'] = $this->programUpdateReport->getAll();
        $data['memberStatusReports'] = $this->memberStatusReport->getAll();

        if (auth()->user()->hasRole('headquarter')) {
            // chart data
            JavaScript::put([
                'monthsOfTheYear'               => $this->getMonthsOfTheYear(),
                'annualActiveCommunityData'     => $this->getAnnualActiveCommunityData($currentYear),
                'membersPerCommunityData'       => $this->getMembersPerCommunityData(),
                'totalCommunityActivities'      => $this->getTotalCommunityActivities(),
                'communityActivityTypes'        => $this->getTotalCommunityActivityTypes(),
                'communityActivityParticipants' => $this->getCommunityActivityParticipants(),
                'participantsPerActivity'       => $this->getParticipantsPerActivityType(),
                'annualCommunityMembers'        => $this->getCommunityMembersData(),
                'communityMaturityData'         => [
                    'graph' => [
                        'y' => $this->levels->getAll()->lists('name'),
                        'x' => 'months',
                    ],
                ],
            ]);
        }

        return view('dashboard.index', $data);
    }

    private function getMonthsOfTheYear()
    {
        return [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ];
    }

    private function getAnnualActiveCommunityData($year)
    {
        // get count of all past data before this year (initial data)
        $pastData = $this->communities->getNumberOfPastCommunities($year);
        $originalData = array_fill(1, 12, 0);

        // get this year's data per month
        $activeCommunities = $this->communities->getActiveCommunitiesPerMonth($year);
        $inactiveCommunities = $this->communities->getInactiveCommunitiesPerMonth($year);

        // increment each month using array_map (?) from initial data adding each month to it
        $counter = $pastData;
        $preparedData = array_replace($originalData, $activeCommunities->all());
        $preparedInactivesData = array_replace($originalData, $inactiveCommunities->all());
        foreach ($preparedData as $key => $item) {
            $counter += $item;
            $counter -= $preparedInactivesData[$key];
            if ($key <= Carbon::now()->month || $year < Carbon::now()->year) {
                $originalData[$key] = $counter;
            }
        }

        // serve data
        if ($activeCommunities->count()) {
            return array_flatten($originalData);
        }

        return [];
    }

    private function getMembersPerCommunityData()
    {
        $communities = $this->communities->getAll(true);

        $chartData = [];
        foreach ($communities as $community) {
            $chartData[] = [
                'name' => $community->name,
                'data' => $this->getMembersOfCommunityPerMonth($community),
            ];
        }

        return $chartData;
    }

    private function getMembersOfCommunityPerMonth($community)
    {
        $members = $community->members;
        $originalData = array_fill(1, 12, 0);
        $groupedMembers = $members->groupBy(function ($item, $key) {
            return $item->created_at->month;
        });
        $preparedData = [];
        foreach ($groupedMembers as $index => $groupedMember) {
            $preparedData[$index] = count($groupedMember);
        }

        $finalData = array_replace($originalData, $preparedData);

        return array_flatten($finalData);
    }

    public function getTotalCommunityActivities()
    {
        $year = Input::get('year', Carbon::now()->year);
        $reportLogs = $this->reportLogs->getDailyReportsWithinPeriod($year, true, true);
        $reportLogsData = $this->getTotalActivitiesData($reportLogs->get());

        return $reportLogsData;
    }

    private function getTotalActivitiesData($reportLogs)
    {
        $originalData = array_fill(1, 12, 0);
        $groupedReportLogs = $reportLogs->groupBy(function ($item, $key) {
            return Carbon::parse($item->period)->month;
        });
        $preparedData = [];
        foreach ($groupedReportLogs as $index => $groupedReportLog) {
            $preparedData[$index] = count($groupedReportLog);
        }

        $finalData = array_replace($originalData, $preparedData);

        return array_flatten($finalData);
    }

    public function getTotalCommunityActivityTypes()
    {
        $year = Input::get('year', Carbon::now()->year);
        $reportLogs = $this->reportLogs->getDailyReportsWithinPeriod($year, true, true);
        $reportLogsData = $this->getTotalCommunityActivityTypesData($reportLogs->get());

        return $reportLogsData;
    }

    private function getTotalCommunityActivityTypesData($reportLogs)
    {
        $activityTypes = $this->activityTypes->getAll();

        $preparedData = [];
        foreach ($activityTypes as $activityType) {
            $preparedData[] = [
                'name' => $activityType->name,
                'data' => $this->filterReportLogsByActivityType($reportLogs, $activityType->id),
            ];
        }

        return $preparedData;
    }

    private function filterReportLogsByActivityType($reportLogs, $id)
    {
        $originalData = array_fill(1, 12, 0);
        $filteredData = $reportLogs->filter(function ($item) use ($id) {
            return $item->report->activity_type_id == $id;
        });
        $groupedReportLogs = $filteredData->groupBy(function ($item, $key) {
            return Carbon::parse($item->period)->month;
        });
        $preparedData = [];
        foreach ($groupedReportLogs as $index => $groupedReportLog) {
            $preparedData[$index] = count($groupedReportLog);
        }
        $finalData = array_replace($originalData, $preparedData);

        return array_flatten($finalData);
    }

    public function getCommunityActivityParticipants()
    {
        $year = Input::get('year', Carbon::now()->year);
        $reportLogs = $this->reportLogs->getDailyReportsWithinPeriod($year, true, true);
        $reportLogsData = $this->getTotalParticipants($reportLogs->get());

        return $reportLogsData;
    }

    private function getTotalParticipants($reportLogs)
    {
        $originalData = array_fill(1, 12, 0);
        $groupedReportLogs = $reportLogs->groupBy(function ($item, $key) {
            return Carbon::parse($item->period)->month;
        });
        $preparedData = [];
        foreach ($groupedReportLogs as $index => $groupedReportLog) {
            $preparedData[$index] = $groupedReportLog->sum(function ($reportLog) {
                return $reportLog->report->participants;
            });
        }

        $finalData = array_replace($originalData, $preparedData);

        return array_flatten($finalData);
    }

    public function getParticipantsPerActivityType()
    {
        $year = Input::get('year', Carbon::now()->year);
        $reportLogs = $this->reportLogs->getDailyReportsWithinPeriod($year, true, true);
        $reportLogsData = $this->getTotalParticipantsPerActivityTypeData($reportLogs->get());

        return $reportLogsData;
    }

    private function getTotalParticipantsPerActivityTypeData($reportLogs)
    {
        $activityTypes = $this->activityTypes->getAll();

        $preparedData = [];
        foreach ($activityTypes as $activityType) {
            $preparedData[] = [
                'name' => $activityType->name,
                'data' => $this->filterParticipantsByActivityType($reportLogs, $activityType->id),
            ];
        }

        return $preparedData;
    }

    private function filterParticipantsByActivityType($reportLogs, $id)
    {
        $originalData = array_fill(1, 12, 0);
        $filteredData = $reportLogs->filter(function ($item) use ($id) {
            return $item->report->activity_type_id == $id;
        });
        $groupedReportLogs = $filteredData->groupBy(function ($item, $key) {
            return Carbon::parse($item->period)->month;
        });
        $preparedData = [];
        foreach ($groupedReportLogs as $index => $groupedReportLog) {
            $preparedData[$index] = $groupedReportLog->sum(function ($reportLog) {
                return $reportLog->report->participants;
            });
        }
        $finalData = array_replace($originalData, $preparedData);

        return array_flatten($finalData);
    }

    public function getCommunityMembersData()
    {
        $year = Input::get('year', Carbon::now()->year);

        $activeMembers = array_fill(1, 12, 0);
        $inactiveMembers = array_fill(1, 12, 0);

        $reportLogs = $this->reportLogs->getAllMonthlyMemberStatusReports($year, $yearOnly = true)->get();

        if ($reportLogs->count()) {
            $memberStatusReports = $reportLogs->groupBy(function ($item, $key) {
                return Carbon::parse($item->period)->month;
            });
            foreach ($memberStatusReports as $key => $item) {
                foreach ($item as $reportLog) {
                    $memberStatusReport = $reportLog->report;
                    foreach ($memberStatusReport->members as $member) {
                        if ($member->is_active) {
                            $activeMembers[$key]++;
                        } else {
                            $inactiveMembers[$key]++;
                        }
                    }
                }
            }
        }

        return [
            [
                'name' => 'Not Active',
                'data' => array_flatten($activeMembers),
            ],
            [
                'name' => 'Active',
                'data' => array_flatten($inactiveMembers),
            ],
        ];
    }

    public function activeCommunitiesChart()
    {
        $year = Input::get('year', Carbon::now()->year);
        $activeCommunitiesData = $this->getAnnualActiveCommunityData($year);

        return $activeCommunitiesData;
    }
}
