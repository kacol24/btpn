<?php
namespace App\Http\Composers;

use App\City;
use App\Repositories\Contracts\BusinessFieldRepositoryInterface;
use Illuminate\Contracts\View\View;

/**
 * Class BusinessFieldsDropdownComposer
 * @package App\Http\Composers
 */
class BusinessFieldsDropdownComposer
{
    /**
     * @var BusinessFieldRepositoryInterface
     */
    private $businessFieldRepository;

    /**
     * BusinessFieldsDropdownComposer constructor.
     *
     * @param BusinessFieldRepositoryInterface $businessFieldRepository
     */
    public function __construct(BusinessFieldRepositoryInterface $businessFieldRepository)
    {
        $this->businessFieldRepository = $businessFieldRepository;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $dropdown = $this->businessFieldRepository->getDropdown();
        $view->with('businessFields', $dropdown);
    }
}