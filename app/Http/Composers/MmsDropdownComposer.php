<?php
namespace App\Http\Composers;

use App\City;
use App\Repositories\Contracts\MmsRepositoryInterface;
use Illuminate\Contracts\View\View;

/**
 * Class MmsDropdownComposer
 * @package App\Http\Composers
 */
class MmsDropdownComposer
{
    /**
     * @var MmsRepositoryInterface
     */
    private $mms;

    /**
     * MmsDropdownComposer constructor.
     *
     * @param MmsRepositoryInterface $mms
     */
    public function __construct(MmsRepositoryInterface $mms)
    {
        $this->mms = $mms;
    }

    public function compose(View $view)
    {
        $mms = $this->mms->getDropdown();
        $view->with('mmsDropdown', $mms);
    }
}