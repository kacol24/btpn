<?php

namespace App\Http\Composers;

use App\Repositories\Contracts\CommunityRepositoryInterface;
use Illuminate\Contracts\View\View;

class CommunitiesDropdownForFacilitatorComposer
{
    /**
     * @var CommunityRepositoryInterface
     */
    private $communities;

    public function __construct(CommunityRepositoryInterface $communities)
    {
        $this->communities = $communities;
    }

    public function compose(View $view)
    {
        $viewData = $view->getData();

        $facilitator = $viewData['facilitator'];
        $dropdown = $this->communities->getDropdownForFacilitator($facilitator);

        $view->with('communities', $dropdown);
    }
}