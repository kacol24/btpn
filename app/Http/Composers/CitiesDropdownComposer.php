<?php
namespace App\Http\Composers;

use App\City;
use App\Repositories\Contracts\CityRepositoryInterface;
use Illuminate\Contracts\View\View;

/**
 * Class CitiesDropdownComposer
 * @package App\Http\Composers
 */
class CitiesDropdownComposer
{
    public function __construct(CityRepositoryInterface $city_repo)
    {
        $this->repo = $city_repo;
    }

    /**
     * @param View                    $view
     * @param CityRepositoryInterface $city_repo
     */
    public function compose(View $view)
    {
        $cities = $this->repo->getDropdown();
        $view->with('cities', $cities);
    }
}