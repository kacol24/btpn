<?php

namespace App\Http\Composers;

use App\Repositories\Contracts\ReportLogRepositoryInterface;
use Auth;
use Illuminate\Contracts\View\View;

class SidebarBadgeComposer
{
    /**
     * @var ReportLogRepositoryInterface
     */
    private $reportLog;

    public function __construct(ReportLogRepositoryInterface $reportLog)
    {
        $this->reportLog = $reportLog;
    }

    public function compose(View $view)
    {
        $declinedReportsCount = null;

        $pendingReportsCount = $this->reportLog->getPending()->count();
        if (Auth::user()->hasRole('facilitator')) {
            $declinedReportsCount = $this->reportLog->getDeclined(Auth::user()->facilitator->id)->count();
        }

        $view->with([
            'pendingReportsCount'  => $pendingReportsCount,
            'declinedReportsCount' => $declinedReportsCount,
        ]);
    }
}