<?php

// Dashboard
Route::get('/', 'DashboardController@index')->name('dashboard.index')->middleware(['auth']);
Route::group([
    'as'         => 'dashboard.',
    'prefix'     => 'dashboard',
    'middleware' => ['auth'],
], function () {
    Route::get('/active-communities-chart',
        'DashboardController@activeCommunitiesChart')->name('active-communities-chart');
    Route::get('total-community-activities',
        'DashboardController@getTotalCommunityActivities')->name('total-community-activities');
    Route::get('community-activity-types',
        'DashboardController@getTotalCommunityActivityTypes')->name('community-activity-types');
    Route::get('community-activity-participants',
        'DashboardController@getCommunityActivityParticipants')->name('community-activity-participants');
    Route::get('participants-per-activity-type',
        'DashboardController@getParticipantsPerActivityType')->name('participants-per-activity-type');
    Route::get('community-membes-chart',
        'DashboardController@getCommunityMembersData')->name('community-members-chart');
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin')->name('auth.login');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('auth.logout');