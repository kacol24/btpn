<?php

namespace App\Http\Requests\Reports;

use App\Http\Requests\Request;
use Auth;
use Carbon;

class MemberStatusParameterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && (Auth::user()->hasRole('facilitator') || Auth::user()->hasRole('mms'))) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (auth()->user()->hasRole('facilitator')) {
            return [
                'period'       => 'required|unique:report_logs,period,NULL,id,community_id,' . $this->community_id . ',report_type,App\Models\MemberStatusReport',
                'community_id' => 'required',
            ];
        }

        return [
            'period'       => 'required|exists:report_logs,period,community_id,' . $this->community_id . ',report_type,App\Models\MemberStatusReport',
            'community_id' => 'required|exists:report_logs,community_id,period,' . $this->period . ',report_type,App\Models\MemberStatusReport',
        ];
    }

    public function messages()
    {
        return [
            'period.unique'       => 'You have already submitted a report for this period',
            'period.exists'       => 'There is no report for the given period',
            'community_id.exists' => 'There is no report for this community',
        ];
    }

    protected function getValidatorInstance()
    {
        $request = $this->all();
        if ($request['period'] != '') {
            $request['period'] = Carbon::parse($request['period'])->toDateString();
        }
        $this->replace($request);

        return parent::getValidatorInstance();
    }
}
