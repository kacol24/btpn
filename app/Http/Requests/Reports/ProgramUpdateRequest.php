<?php

namespace App\Http\Requests\Reports;

use App\Http\Requests\Request;
use Auth;
use Carbon;

class ProgramUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && Auth::user()->hasRole('facilitator')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'period'          => 'required|unique:report_logs,period,NULL,id,community_id,' . $this->community_id . ',report_type,App\Models\ProgramUpdateReport',
            'community_id'    => 'required|integer',
            'report_type'     => 'in:program_update',
            'f1_document_url' => 'required|mimes:jpeg,jpg,png,pdf',
        ];
    }

    public function messages()
    {
        return [
            'period.unique' => 'You have already submitted a report for this period',
        ];
    }

    protected function getValidatorInstance()
    {
        $request = $this->all();
        if (isset($request['period']) && $request['period'] != '') {
            $request['period'] = Carbon::parse($request['period'])->toDateString();
        }
        $this->replace($request);

        return parent::getValidatorInstance();
    }
}
