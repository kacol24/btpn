<?php

namespace App\Http\Requests\Reports;

use App\Http\Requests\Request;
use Auth;

class DailyReportRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && Auth::user()->hasRole('facilitator')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'community_id'            => 'required|integer',
            // TODO change date with something
            'period'                  => 'required|unique:report_logs,period,NULL,id,report_type,App\Models\DailyReport,community_id,' . $this->community_id,
            'start_time'              => 'required|different:end_time',
            'end_time'                => 'required',
            'location'                => 'required',
            'activity_type_id'        => 'required|integer',
            'activity_name'           => 'required',
            'f2_document_url'         => 'required|mimes:jpeg,jpg,png,pdf',
            'f3_document_url'         => 'mimes:jpeg,jpg,png,pdf',
            'supporting_document_url' => 'mimes:jpeg,jpg,png,pdf,zip',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'date.unique' => 'You have already submitted a report for this date',
        ];
    }
}
