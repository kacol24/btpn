<?php

namespace App\Http\Requests;

use Auth;

class CommunityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && Auth::user()->hasRole('headquarter')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|max:30',
            'city_id'           => 'required|integer',
            'mms_id'            => 'required|integer',
            'business_field_id' => 'required|integer',
            'activation_date'   => 'required|date_format:Y-m',
        ];
    }
}
