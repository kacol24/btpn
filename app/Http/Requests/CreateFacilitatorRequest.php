<?php

namespace App\Http\Requests;

use Auth;

class CreateFacilitatorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && Auth::user()->hasRole('headquarter')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'     => 'required|alpha_num|max:20|unique:users',
            'password'     => 'required|confirmed|min:6',
            'email'        => 'required|email|max:250|unique:users',
            'name'         => 'required|max:50',
            'phone'        => 'max:20',
            'address'      => 'max:250',
            'picture'      => 'image',
            'dob'          => 'required|date_format:Y-m-d',
            'city_id'      => 'required|integer',
            'gender'       => 'required|in:m,f',
            'community_id' => 'array',
        ];
    }
}
