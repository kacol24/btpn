<?php

namespace App\Http\Requests;

use Auth;

class EditMemberRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && Auth::user()->hasRole('headquarter')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id'        => 'required|integer',
            'mms_id'         => 'required|integer',
            'sentra_id'      => 'required|integer',
            'community_id'   => 'required|integer',
            'account_number' => 'required|unique:members,account_number,' . $this->members,
            'name'           => 'required|max:30',
            'phone'          => 'max:20',
            'address'        => 'max:250',
            'is_leader'      => 'unique:members,is_leader,' . $this->members . ',id,community_id,' . $this->community_id,
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'is_leader.unique' => 'The assigned community already has a leader',
        ];
    }
}
