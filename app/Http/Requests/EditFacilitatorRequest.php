<?php

namespace App\Http\Requests;

use Auth;
use Carbon;

class EditFacilitatorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && Auth::user()->hasRole('headquarter')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = $this->request->get('user_id');

        $request = $this;
        if ($request->password == '') {
            $request->offsetUnset('password');
        }

        return [
            'username' => 'required|alpha_num|max:20|unique:users,username,' . $user_id,
            'password' => 'confirmed|min:6',
            'email'    => 'required|email|max:250|unique:users,email,' . $user_id,
            'name'     => 'required|max:50',
            'phone'    => 'max:20',
            'address'  => 'max:250',
            'picture'  => 'image',
            'dob'      => 'required|date_format:Y-m-d',
            'gender'   => 'required|in:m,f',
        ];
    }
}
