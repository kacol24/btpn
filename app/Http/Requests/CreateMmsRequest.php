<?php

namespace App\Http\Requests;

use Auth;

class CreateMmsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && Auth::user()->hasRole('headquarter')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id'  => 'required|integer',
            'username' => 'required|alpha_num|max:20|unique:users',
            'password' => 'required|confirmed|min:6',
            'email'    => 'required|email|max:250|unique:users',
            'name'     => 'required|max:50',
            'phone'    => 'max:20',
            'address'  => 'max:250',
            'picture'  => 'image',
        ];
    }
}
