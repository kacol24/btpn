<?php

namespace App\Http\Requests;

use Auth;

class SentraRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && Auth::user()->hasRole('headquarter')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|max:50',
            'mms_id'  => 'required|integer',
            'phone'   => 'max:20',
            'email'   => 'required|email|max:50',
            'address' => 'max:250',
        ];
    }
}
