<?php

namespace App\Http\Requests;

class ValueRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->check() && auth()->user()->hasRole('headquarter')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $value_id = $this->route('value');

        return [
            'aspect_id' => "required|integer|unique:maturity_values,aspect_id,{$value_id},id,level_id,{$this->level_id}",
            'level_id'  => "required|integer|unique:maturity_values,level_id,{$value_id},id,aspect_id,{$this->aspect_id}",
            'value'     => 'required',
        ];
    }
}
