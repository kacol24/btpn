<?php

/*
 * Dashboard
 */
Breadcrumbs::register('dashboard.index', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('dashboard.index'));
});

/*
 * Cities
 */
Breadcrumbs::register('cities.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Cities', route('cities.index'));
});

/*
 * MMS
 */
Breadcrumbs::register('mms.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('MMS', route('mms.index'));
});

/*
 * Sentra
 */
Breadcrumbs::register('sentra.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Sentra', route('sentra.index'));
});
Breadcrumbs::register('mms.sentra.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Sentra in ' . Auth::user()->name, route('mms.sentra.index'));
});

/*
 * Communities
 */
Breadcrumbs::register('communities.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Communities', route('communities.index'));
});
Breadcrumbs::register('facilitators.communities.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Communities', route('facilitators.communities.index'));
});
Breadcrumbs::register('mms.communities.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Communities in ' . Auth::user()->name, route('mms.communities.index'));
});

/*
 * Members
 */
Breadcrumbs::register('members.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Members', route('members.index'));
});

/*
 * Facilitators
 */
Breadcrumbs::register('facilitators.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Facilitators', route('facilitators.index'));
});

/*
 * Verifications
 */
Breadcrumbs::register('verifications.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Verifications', route('verifications.index'));
});
Breadcrumbs::register('verifications.daily-report.show', function ($breadcrumbs) {
    $breadcrumbs->parent('verifications.index');
    $breadcrumbs->push('Daily Report', route('verifications.daily-report.show'));
});

/*
 * Reports
 */
Breadcrumbs::register('reports.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Generate Report', route('reports.index'));
});
Breadcrumbs::register('reports.generate', function ($breadcrumbs) {
    $breadcrumbs->parent('reports.index');
    $breadcrumbs->push('Document Preview', route('reports.generate'));
});
Breadcrumbs::register('facilitators.reports.daily-report.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Submit Daily Report', route('facilitators.reports.daily-report.index'));
});
Breadcrumbs::register('facilitators.reports.monthly-report.program-update.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Monthly Program Update Report',
        route('facilitators.reports.monthly-report.program-update.index'));
});
Breadcrumbs::register('facilitators.reports.monthly-report.member-status.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Monthly Member Status Report',
        route('facilitators.reports.monthly-report.member-status.index'));
});
Breadcrumbs::register('facilitators.reports.check-document-status.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Check Document Status', route('facilitators.reports.check-document-status.index'));
});
Breadcrumbs::register('facilitators.reports.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('facilitators.reports.check-document-status.index');
    $breadcrumbs->push('Revise Document', route('facilitators.reports.edit'));
});
Breadcrumbs::register('mms.reports.member-status.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Member Status Report', route('mms.reports.member-status.index'));
});
Breadcrumbs::register('mms.reports.program-update.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Program Update Report', route('mms.reports.program-update.index'));
});

/*
 * CMM
 */
Breadcrumbs::register('cmm.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Maturity Model', route('cmm.index'));
});
Breadcrumbs::register('cmm.model.index', function ($breadcrumbs) {
    $breadcrumbs->parent('cmm.index');
    $breadcrumbs->push('Community Maturity', route('cmm.model.index'));
});
Breadcrumbs::register('cmm.model.show', function ($breadcrumbs) {
    $breadcrumbs->parent('cmm.index');
    $breadcrumbs->push('Community Maturity', route('cmm.model.index'));
});