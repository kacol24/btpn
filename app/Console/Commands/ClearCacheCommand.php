<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearCacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:clear-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear application cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bar = $this->output->createProgressBar(count(7));

        $this->info('Clearing application cache..');
        $bar->advance();

        $this->callSilent('clear-compiled');
        $bar->advance();

        $this->callSilent('cache:clear');
        $bar->advance();

        $this->callSilent('config:clear');
        $bar->advance();

        $this->callSilent('route:clear');
        $bar->advance();

        $this->callSilent('view:clear');
        $bar->advance();

        $this->callSilent('optimize');
        $bar->finish();

        $this->info('Application cache cleared..');
    }
}
