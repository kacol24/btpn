<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache the system files for better performance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bar = $this->output->createProgressBar(count(5));

        $this->info('Caching your application..');
        $bar->advance();

        $this->callSilent('clear-compiled');
        $bar->advance();

        $this->callSilent('config:cache');
        $bar->advance();

        $this->callSilent('route:cache');
        $bar->advance();

        $this->callSilent('optimize');
        $bar->finish();

        $this->info('Application cached..');
    }
}
