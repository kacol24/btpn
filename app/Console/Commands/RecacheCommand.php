<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RecacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:recache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Re-cache application cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bar = $this->output->createProgressBar(count(3));

        $this->info('Re-cahing your application');
        $bar->advance();

        $this->callSilent('app:clear-cache');
        $bar->advance();

        $this->callSilent('app:cache');
        $bar->finish();

        $this->info('Application cache has been rereshed');
    }
}
