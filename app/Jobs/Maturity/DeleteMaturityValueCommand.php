<?php

namespace App\Jobs\Maturity;

use App\Events\Maturity\MaturityValueWasDeleted;
use App\Jobs\Job;
use App\Repositories\Contracts\MaturityValueRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class DeleteMaturityValueCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param                                  $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param MaturityValueRepositoryInterface $maturityValue
     *
     * @return mixed
     */
    public function handle(MaturityValueRepositoryInterface $maturityValue)
    {
        $value = $maturityValue->delete($this->id);
        event(new MaturityValueWasDeleted($value));

        return $value;
    }
}
