<?php

namespace App\Jobs\Maturity;

use App\Jobs\Job;
use App\Repositories\Contracts\AspectRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class DeleteAspectCommand
 * @package App\Jobs\Maturity
 */
class DeleteAspectCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * DeleteAspectCommand constructor.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param AspectRepositoryInterface $aspect
     *
     * @return AspectRepositoryInterface|mixed
     */
    public function handle(AspectRepositoryInterface $aspect)
    {
        $aspect = $aspect->delete($this->id);
        event(new AspectWasDeleted($aspect));

        return $aspect;
    }
}
