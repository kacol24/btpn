<?php

namespace App\Jobs\Maturity;

use App\Events\Maturity\MaturityValueWasUpdated;
use App\Http\Requests\ValueRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\MaturityValueRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class UpdateMaturityValueCommand
 * @package App\Jobs\Maturity
 */
class UpdateMaturityValueCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * UpdateMaturityValueCommand constructor.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param ValueRequest                     $request
     * @param MaturityValueRepositoryInterface $maturityValue
     *
     * @return mixed
     */
    public function handle(ValueRequest $request, MaturityValueRepositoryInterface $maturityValue)
    {
        $value = $maturityValue->update($this->id, $request->all());
        event(new MaturityValueWasUpdated($value));

        return $value;
    }
}
