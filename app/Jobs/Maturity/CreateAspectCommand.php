<?php

namespace App\Jobs\Maturity;

use App\Events\Maturity\AspectWasCreated;
use App\Http\Requests\AspectRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\AspectRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateAspectCommand extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @param AspectRequest             $request
     * @param AspectRepositoryInterface $aspect
     *
     * @return mixed
     */
    public function handle(AspectRequest $request, AspectRepositoryInterface $aspect)
    {
        $aspect = $aspect->create($request->all());
        event(new AspectWasCreated($aspect));

        return $aspect;
    }
}
