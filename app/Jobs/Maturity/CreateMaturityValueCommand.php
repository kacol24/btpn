<?php

namespace App\Jobs\Maturity;

use App\Events\Maturity\MaturityValueWasCreated;
use App\Http\Requests\ValueRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\MaturityValueRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateMaturityValueCommand extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @param ValueRequest                     $request
     * @param MaturityValueRepositoryInterface $maturityValue
     *
     * @return mixed
     */
    public function handle(ValueRequest $request, MaturityValueRepositoryInterface $maturityValue)
    {
        $value = $maturityValue->create($request->all());
        event(new MaturityValueWasCreated($value));

        return $value;
    }
}
