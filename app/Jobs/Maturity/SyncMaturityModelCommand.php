<?php

namespace App\Jobs\Maturity;

use App\Events\Maturity\MaturityModelWasSynced;
use App\Http\Requests\SyncMaturityModelRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class SyncMaturityModelCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param                              $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param SyncMaturityModelRequest     $request
     * @param CommunityRepositoryInterface $communities
     *
     * @return mixed
     */
    public function handle(SyncMaturityModelRequest $request, CommunityRepositoryInterface $communities)
    {
        $model = $communities->syncMaturityValue($this->id, array_values($request->value ?: []));
        event(new MaturityModelWasSynced($model));

        return $model;
    }
}
