<?php

namespace App\Jobs\Maturity;

use App\Events\Maturity\LevelWasUpdated;
use App\Http\Requests\LevelRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\LevelRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateLevelCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param                          $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param LevelRequest             $request
     * @param LevelRepositoryInterface $level
     *
     * @return LevelRepositoryInterface
     */
    public function handle(LevelRequest $request, LevelRepositoryInterface $level)
    {
        $level = $level->update($this->id, $request->all());
        event(new LevelWasUpdated($level));

        return $level;
    }
}
