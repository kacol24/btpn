<?php

namespace App\Jobs\Maturity;

use App\Events\Maturity\LevelWasDeleted;
use App\Jobs\Job;
use App\Repositories\Contracts\LevelRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class DeleteLevelCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param LevelRepositoryInterface $level
     *
     * @return LevelRepositoryInterface
     */
    public function handle(LevelRepositoryInterface $level)
    {
        $level = $level->delete($this->id);
        event(new LevelWasDeleted($level));

        return $level;
    }
}
