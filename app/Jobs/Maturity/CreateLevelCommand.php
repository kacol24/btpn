<?php

namespace App\Jobs\Maturity;

use App\Events\Maturity\LevelWasCreated;
use App\Http\Requests\LevelRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\LevelRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class CreateLevelCommand
 * @package App\Jobs\Maturity
 */
class CreateLevelCommand extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     */
    public function __construct()
    {

    }

    /**
     * @param LevelRequest             $request
     * @param LevelRepositoryInterface $level
     *
     * @return LevelRepositoryInterface
     */
    public function handle(LevelRequest $request, LevelRepositoryInterface $level)
    {
        $level = $level->create($request->all());
        event(new LevelWasCreated($level));

        return $level;
    }
}
