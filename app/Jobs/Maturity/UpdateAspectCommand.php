<?php

namespace App\Jobs\Maturity;

use App\Events\Maturity\AspectWasUpdated;
use App\Http\Requests\AspectRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\AspectRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class UpdateAspectCommand
 * @package App\Jobs\Maturity
 */
class UpdateAspectCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param AspectRequest             $request
     * @param AspectRepositoryInterface $aspect
     *
     * @return mixed
     */
    public function handle(AspectRequest $request, AspectRepositoryInterface $aspect)
    {
        $aspect = $aspect->update($this->id, $request->all());
        event(new AspectWasUpdated($aspect));

        return $aspect;
    }
}
