<?php

namespace App\Jobs\Reports;

use App\Events\Reports\ReportWasVerified;
use App\Http\Requests\Reports\VerifyReportsRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\ReportLogRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class VerifyReportCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param ReportLogRepositoryInterface $reportLog
     * @param VerifyReportsRequest         $request
     *
     * @return mixed
     */
    public function handle(ReportLogRepositoryInterface $reportLog, VerifyReportsRequest $request)
    {
        $report = $reportLog->verify($this->id);
        event(new ReportWasVerified($report));

        return $report;
    }
}
