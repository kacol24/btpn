<?php

namespace App\Jobs\Reports;

use App\Events\Reports\DailyReportWasCreated;
use App\Http\Requests\Reports\DailyReportRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\DailyReportRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class CreateDailyReportCommand
 * @package App\Jobs
 */
class CreateDailyReportCommand extends Job implements SelfHandling
{
    /**
     * CreateDailyReportCommand constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param DailyReportRequest             $request
     * @param DailyReportRepositoryInterface $dailyReport
     *
     * @return mixed
     */
    public function handle(DailyReportRequest $request, DailyReportRepositoryInterface $dailyReport)
    {
        $report = $dailyReport->create($request->all());
        event(new DailyReportWasCreated($report));

        return $report;
    }
}
