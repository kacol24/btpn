<?php

namespace App\Jobs\Reports;

use App\Events\Reports\ReportWasVoided;
use App\Jobs\Job;
use App\Repositories\Contracts\ReportLogRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class VoidReportCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param ReportLogRepositoryInterface $reportLog
     *
     * @return mixed
     */
    public function handle(ReportLogRepositoryInterface $reportLog) // TODO use request to check for auth
    {
        $report = $reportLog->void($this->id);
        event(new ReportWasVoided($report));

        return $report;
    }
}
