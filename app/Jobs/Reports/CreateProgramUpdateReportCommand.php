<?php

namespace App\Jobs\Reports;

use App\Events\Reports\ProgramUpdateReportWasCreated;
use App\Http\Requests\Reports\ProgramUpdateRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\ProgramUpdateReportRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateProgramUpdateReportCommand extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param ProgramUpdateRequest                   $request
     * @param ProgramUpdateReportRepositoryInterface $programUpdateReport
     *
     * @return mixed
     */
    public function handle(ProgramUpdateRequest $request, ProgramUpdateReportRepositoryInterface $programUpdateReport)
    {
        $request->offsetSet('facilitator_id', auth()->user()->facilitator->id);
        $report = $programUpdateReport->create($request->all());
        event(new ProgramUpdateReportWasCreated($report));

        return $report;
    }
}
