<?php

namespace App\Jobs\Reports;

use App\Events\Reports\MemberStatusReportWasCreated;
use App\Http\Requests\Reports\MemberStatusRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\MemberStatusReportRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateMemberStatusReportCommand extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param MemberStatusRequest                   $request
     * @param MemberStatusReportRepositoryInterface $memberStatusReport
     *
     * @return mixed
     */
    public function handle(MemberStatusRequest $request, MemberStatusReportRepositoryInterface $memberStatusReport)
    {
        $request->offsetSet('facilitator_id', auth()->user()->facilitator->id);
        $report = $memberStatusReport->create($request->all());
        event(new MemberStatusReportWasCreated($report));

        return $report;
    }
}
