<?php

namespace App\Jobs\Reports;

use App\Events\Reports\DailyReportWasUpdated;
use App\Jobs\Job;
use App\Repositories\Contracts\DailyReportRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Http\Request;

class UpdateDailyReportCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param Request                        $request
     * @param DailyReportRepositoryInterface $dailyReport
     *
     * @return mixed
     */
    public function handle(Request $request, DailyReportRepositoryInterface $dailyReport)
    {
        // TODO use update request, so that the period/date is checked for duplicate
        $report = $dailyReport->update($this->id, $request->all());
        event(new DailyReportWasUpdated($report));

        return $report;
    }
}
