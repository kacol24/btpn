<?php

namespace App\Jobs\Reports;

use App\Events\Reports\ReportWasDeclined;
use App\Http\Requests\Reports\VerifyReportsRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\ReportLogRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class DeclineReportCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * DeclineReportCommand constructor.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param VerifyReportsRequest         $request
     * @param ReportLogRepositoryInterface $reportLog
     *
     * @return mixed
     */
    public function handle(VerifyReportsRequest $request, ReportLogRepositoryInterface $reportLog)
    {
        $report = $reportLog->decline($this->id, $request->reason);
        event(new ReportWasDeclined($report, $request->reason));

        return $report;
    }
}
