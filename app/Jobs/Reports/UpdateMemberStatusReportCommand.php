<?php

namespace App\Jobs\Reports;

use App\Events\Reports\MemberStatusReportWasUpdated;
use App\Jobs\Job;
use App\Repositories\Contracts\MemberStatusReportRepositoryInterface;
use Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Http\Request;

class UpdateMemberStatusReportCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param MemberStatusReportRepositoryInterface $memberStatusReport
     * @param Request                               $request
     *
     * @return mixed
     */
    public function handle(MemberStatusReportRepositoryInterface $memberStatusReport, Request $request)
    {
        // TODO use a form request to check for duplicate period/date
        $request->offsetSet('facilitator_id', auth()->user()->facilitator->id);
        $request->offsetSet('period', Carbon::parse($request->period)->format('Y-m-d'));
        $report = $memberStatusReport->update($this->id, $request->all());
        event(new MemberStatusReportWasUpdated($report->report));

        return $report;
    }
}
