<?php

namespace App\Jobs\City;

use App\Events\City\CityWasUpdated;
use App\Http\Requests\CityRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\CityRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateCityCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * UpdateCityCommand constructor.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param CityRepositoryInterface $repo
     * @param CityRequest             $request
     *
     * @return mixed
     */
    public function handle(CityRepositoryInterface $repo, CityRequest $request)
    {
        $city = $repo->update($this->id, $request->all());
        event(new CityWasUpdated($city));

        return $city;
    }
}
