<?php

namespace App\Jobs\City;

use App\Events\City\CityWasCreated;
use App\Events\Errors\CreateHasFailed;
use App\Http\Requests\CityRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\CityRepositoryInterface;
use Exception;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class CreateCityCommand
 * @package App\Jobs
 */
class CreateCityCommand extends Job implements SelfHandling
{
    /**
     * CreateCityCommand constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param CityRequest             $request
     * @param CityRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(CityRequest $request, CityRepositoryInterface $repo)
    {
        try {
            $city = $repo->create($request->all());
            event(new CityWasCreated($city));

            return $city;
        } catch (Exception $e) {
            if (! $request->ajax()) {
                event(new CreateHasFailed());
            }
        }

        return null;
    }
}
