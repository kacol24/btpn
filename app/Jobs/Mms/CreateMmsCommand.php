<?php

namespace App\Jobs\Mms;

use App\Events\Errors\CreateHasFailed;
use App\Events\Mms\MmsWasCreated;
use App\Http\Requests\CreateMmsRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\MmsRepositoryInterface;
use Exception;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class CreateMmsCommand
 * @package App\Jobs\Mms
 */
class CreateMmsCommand extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param CreateMmsRequest       $request
     * @param MmsRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(CreateMmsRequest $request, MmsRepositoryInterface $repo)
    {
        try {
            $mms = $repo->create($request->all());
            event(new MmsWasCreated($mms));

            return $mms;
        } catch (Exception $e) {
            event(new CreateHasFailed());
        }
    }
}
