<?php

namespace App\Jobs\Mms;

use App\Events\Mms\MmsWasUpdated;
use App\Http\Requests\EditMmsRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\MmsRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class UpdateMmsCommand
 * @package App\Jobs\Mms
 */
class UpdateMmsCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * UpdateMmsCommand constructor.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @param EditMmsRequest         $request
     * @param MmsRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(EditMmsRequest $request, MmsRepositoryInterface $repo)
    {
        $mms = $repo->update($this->id, $request->all());
        event(new MmsWasUpdated($mms));

        return $mms;
    }
}
