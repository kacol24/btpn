<?php

namespace App\Jobs\Facilitator;

use App\Events\Facilitator\FacilitatorWasCreated;
use App\Http\Requests\CreateFacilitatorRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\FacilitatorRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use Intervention\Image\Facades\Image;

class CreateFacilitatorCommand extends Job implements SelfHandling
{
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param CreateFacilitatorRequest       $request
     * @param FacilitatorRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(CreateFacilitatorRequest $request, FacilitatorRepositoryInterface $repo)
    {
        $facilitator = $repo->create($request->all());
        event(new FacilitatorWasCreated($facilitator));

        return $facilitator;
    }
}
