<?php
/******************************************************************************
 * Copyright (c) 2016 Kevin Chandra.                                          *
 *                                                                            *
 * This file is part of BTPN - Daya Tumbuh Komunitas.                         *
 *                                                                            *
 * For the full copyright and license information, please view the LICENSE    *
 * file that was distributed with this source code.                           *
 ******************************************************************************/

namespace App\Jobs\Facilitator;

use App\Events\Facilitator\FacilitatorWasDeactivated;
use App\Jobs\Job;
use App\Repositories\Contracts\FacilitatorRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class DeactivateFacilitatorCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param FacilitatorRepositoryInterface $facilitators
     *
     * @return mixed
     */
    public function handle(FacilitatorRepositoryInterface $facilitators)
    {
        $facilitator = $facilitators->deactivate($this->id);
        event(new FacilitatorWasDeactivated($facilitator));

        return $facilitator;
    }
}
