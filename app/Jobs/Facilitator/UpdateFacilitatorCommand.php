<?php

namespace App\Jobs\Facilitator;

use App\Events\Facilitator\FacilitatorWasUpdated;
use App\Http\Requests\EditFacilitatorRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\FacilitatorRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateFacilitatorCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param EditFacilitatorRequest         $request
     * @param FacilitatorRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(EditFacilitatorRequest $request, FacilitatorRepositoryInterface $repo)
    {
        $facilitator = $repo->update($this->id, $request->all());
        event(new FacilitatorWasUpdated($facilitator));

        return $facilitator;
    }
}
