<?php

namespace App\Jobs\Community;

use App\Events\Community\CommunityWasDeactivated;
use App\Http\Requests\DeactivateCommunityRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class DeactivateCommunityCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param DeactivateCommunityRequest   $request
     * @param CommunityRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(DeactivateCommunityRequest $request, CommunityRepositoryInterface $repo)
    {
        $community = $repo->deactivate($this->id, $request->deactivation_reason);
        event(new CommunityWasDeactivated($community));

        return $community;
    }
}
