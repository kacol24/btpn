<?php

namespace App\Jobs\Community;

use App\Events\Community\CommunityWasCreated;
use App\Http\Requests\CommunityRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateCommunityCommand extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param CommunityRequest             $request
     * @param CommunityRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(CommunityRequest $request, CommunityRepositoryInterface $repo)
    {
        $community = $repo->create($request->all());
        event(new CommunityWasCreated($community));

        return $community;
    }
}
