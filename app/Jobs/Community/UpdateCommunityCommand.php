<?php

namespace App\Jobs\Community;

use App\Events\Community\CommunityWasUpdated;
use App\Http\Requests\CommunityRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\CommunityRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateCommunityCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param CommunityRequest             $request
     * @param CommunityRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(CommunityRequest $request, CommunityRepositoryInterface $repo)
    {
        $community = $repo->update($this->id, $request->all());
        event(new CommunityWasUpdated($community));

        return $community;
    }
}
