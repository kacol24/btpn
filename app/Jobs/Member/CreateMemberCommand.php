<?php

namespace App\Jobs\Member;

use App\Events\Member\MemberWasCreated;
use App\Http\Requests\CreateMemberRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\MemberRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateMemberCommand extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param CreateMemberRequest       $request
     * @param MemberRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(CreateMemberRequest $request, MemberRepositoryInterface $repo)
    {
        $member = $repo->create($request->all());
        event(new MemberWasCreated($member));

        return $member;
    }
}
