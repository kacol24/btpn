<?php

namespace App\Jobs\Member;

use App\Events\Member\MemberWasUpdated;
use App\Http\Requests\EditMemberRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\MemberRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateMemberCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param EditMemberRequest         $request
     * @param MemberRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(EditMemberRequest $request, MemberRepositoryInterface $repo)
    {
        $member = $repo->update($this->id, $request->all());
        event(new MemberWasUpdated($member));

        return $member;
    }
}
