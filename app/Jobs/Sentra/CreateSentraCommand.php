<?php

namespace App\Jobs\Sentra;

use App\Events\Sentra\SentraWasCreated;
use App\Http\Requests\SentraRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\SentraRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateSentraCommand extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param SentraRequest             $request
     * @param SentraRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(SentraRequest $request, SentraRepositoryInterface $repo)
    {
        $sentra = $repo->create($request->all());
        event(new SentraWasCreated($sentra));

        return $sentra;
    }
}
