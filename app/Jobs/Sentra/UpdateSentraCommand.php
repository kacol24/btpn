<?php

namespace App\Jobs\Sentra;

use App\Events\Sentra\SentraWasUpdated;
use App\Http\Requests\SentraRequest;
use App\Jobs\Job;
use App\Repositories\Contracts\SentraRepositoryInterface;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateSentraCommand extends Job implements SelfHandling
{
    /**
     * @var
     */
    private $id;

    /**
     * Create a new job instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param SentraRequest             $request
     * @param SentraRepositoryInterface $repo
     *
     * @return mixed
     */
    public function handle(SentraRequest $request, SentraRepositoryInterface $repo)
    {
        $sentra = $repo->update($this->id, $request->all());
        event(new SentraWasUpdated($sentra));

        return $sentra;
    }
}
