<?php

namespace App\Listeners\Member;

use App\Events\Member\MemberWasCreated;
use Debugbar;

class LogMemberCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MemberWasCreated $event
     *
     * @return void
     */
    public function handle(MemberWasCreated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("MEMBER: [{$event->id}]{$event->name} has been successfully created!");
        }
    }
}
