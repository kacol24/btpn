<?php

namespace App\Listeners\Member;

use App\Events\Member\MemberWasUpdated;
use Debugbar;

class LogMemberUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MemberWasUpdated $event
     *
     * @return void
     */
    public function handle(MemberWasUpdated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("MEMBER: [{$event->id}]{$event->name} has been successfully updated!");
        }
    }
}
