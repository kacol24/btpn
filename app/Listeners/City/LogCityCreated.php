<?php

namespace App\Listeners\City;

use App\Events\City\CityWasCreated;
use Debugbar;

class LogCityCreated
{
    /**
     * LogCityCreated constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CityWasCreated $event
     *
     * @return void
     */
    public function handle(CityWasCreated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("[{$event->id}]{$event->name} has been successfully created!");
        }
    }
}
