<?php

namespace App\Listeners\City;

use App\Events\City\CityWasUpdated;
use Debugbar;

/**
 * Class LogCityUpdated
 * @package App\Listeners\City
 */
class LogCityUpdated
{
    /**
     * LogCityUpdated constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CityWasUpdated $event
     *
     * @return void
     */
    public function handle(CityWasUpdated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("[{$event->id}]{$event->name} has been successfully updated!");
        }
    }
}
