<?php

namespace App\Listeners\Mms;

use App\Events\Mms\MmsWasUpdated;
use Debugbar;

class LogMmsUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MmsWasUpdated $event
     *
     * @return void
     */
    public function handle(MmsWasUpdated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("MMS: [{$event->id}]{$event->name} has been successfully updated!");
        }
    }
}
