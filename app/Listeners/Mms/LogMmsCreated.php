<?php

namespace App\Listeners\Mms;

use App\Events\Mms\MmsWasCreated;
use Debugbar;

class LogMmsCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MmsWasCreated $event
     *
     * @return void
     */
    public function handle(MmsWasCreated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("MMS: [{$event->id}]{$event->name} has been successfully created!");
        }
    }
}
