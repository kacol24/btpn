<?php

namespace App\Listeners\Reports;

use App\Events\Reports\ProgramUpdateReportWasUpdated;
use Toastr;

class FlashProgramUpdateReportUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProgramUpdateReportWasUpdated $event
     *
     * @return void
     */
    public function handle(ProgramUpdateReportWasUpdated $event)
    {
        Toastr::success("PROGRAM UPDATE REPORT: [{$event->id}] has been successfully updated!", 'Success!');
    }
}
