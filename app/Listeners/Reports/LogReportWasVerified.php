<?php

namespace App\Listeners\Reports;

use App\Events\Reports\ReportWasVerified;
use Debugbar;

class LogReportWasVerified
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReportWasVerified $event
     *
     * @return void
     */
    public function handle(ReportWasVerified $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info(ucwords($event->reportType) . ": [{$event->id}] has been successfully verified!");
        }
    }
}
