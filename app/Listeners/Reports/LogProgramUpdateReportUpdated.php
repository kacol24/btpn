<?php

namespace App\Listeners\Reports;

use App\Events\Reports\ProgramUpdateReportWasUpdated;
use Debugbar;

class LogProgramUpdateReportUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProgramUpdateReportWasUpdated $event
     *
     * @return void
     */
    public function handle(ProgramUpdateReportWasUpdated $event)
    {
        if (env('APP_DEBUG') && env('APP_ENV') == 'local') {
            Debugbar::info("PROGRAM UPDATE REPORT: [{$event->id}] has been successfully updated!");
        }
    }
}
