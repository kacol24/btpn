<?php

namespace App\Listeners\Reports;

use App\Events\Reports\ProgramUpdateReportWasCreated;
use Debugbar;

class LogProgramUpdateReportCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProgramUpdateReportWasCreated $event
     *
     * @return void
     */
    public function handle(ProgramUpdateReportWasCreated $event)
    {
        if (env('APP_DEBUG') && env('APP_ENV') == 'local') {
            Debugbar::info("PROGRAM UPDATE REPORT: [{$event->id}] has been successfully created!");
        }
    }
}
