<?php

namespace App\Listeners\Reports;

use App\Events\Reports\ReportWasDeclined;
use Debugbar;

class LogReportWasDeclined
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReportWasDeclined $event
     *
     * @return void
     */
    public function handle(ReportWasDeclined $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info(ucwords($event->reportType) . ": [{$event->id}] was declined with reason '{$event->reason}'!");
        }
    }
}
