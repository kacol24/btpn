<?php

namespace App\Listeners\Reports;

use App\Events\Reports\MemberStatusReportWasCreated;
use Debugbar;

class LogMemberStatusReportCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MemberStatusReportWasCreated $event
     *
     * @return void
     */
    public function handle(MemberStatusReportWasCreated $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("MEMBER STATUS REPORT: [{$event->id}] for community [{$event->community}] has been successfully created by [{$event->facilitator}]!");
        }
    }
}
