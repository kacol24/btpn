<?php

namespace App\Listeners\Reports;

use App\Events\Reports\DailyReportWasUpdated;
use Toastr;

class FlashDailyReportUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DailyReportWasUpdated $event
     *
     * @return void
     */
    public function handle(DailyReportWasUpdated $event)
    {
        Toastr::success("DAILY REPORT: [{$event->id}] has been successfully updated!", 'Success!');
    }
}
