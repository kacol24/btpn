<?php

namespace App\Listeners\Reports;

use App\Events\Reports\ProgramUpdateReportWasCreated;
use Toastr;

class FlashProgramUpdateReportCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProgramUpdateReportWasCreated $event
     *
     * @return void
     */
    public function handle(ProgramUpdateReportWasCreated $event)
    {
        Toastr::success("PROGRAM UPDATE REPORT: [{$event->id}] has been successfully created!", 'Success!');
    }
}
