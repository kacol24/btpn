<?php

namespace App\Listeners\Reports;

use App\Events\Reports\DailyReportWasCreated;
use Toastr;

class FlashDailyReportCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DailyReportWasCreated $event
     *
     * @return void
     */
    public function handle(DailyReportWasCreated $event)
    {
        Toastr::success("DAILY REPORT: [{$event->id}] for community [{$event->community}] has been successfully created by [{$event->facilitator}]!",
            'Success!');
    }
}
