<?php

namespace App\Listeners\Reports;

use App\Events\Reports\DailyReportWasUpdated;
use Debugbar;

class LogDailyReportUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DailyReportWasUpdated $event
     *
     * @return void
     */
    public function handle(DailyReportWasUpdated $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("DAILY REPORT: [{$event->id}] has been successfully updated!");
        }
    }
}
