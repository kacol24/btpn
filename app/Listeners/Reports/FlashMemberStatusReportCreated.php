<?php

namespace App\Listeners\Reports;

use App\Events\Reports\MemberStatusReportWasCreated;
use Toastr;

class FlashMemberStatusReportCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MemberStatusReportWasCreated $event
     *
     * @return void
     */
    public function handle(MemberStatusReportWasCreated $event)
    {
        Toastr::success("MEMBER STATUS REPORT: [{$event->id}] for community [{$event->community}] has been successfully created by [{$event->facilitator}]!",
            'Success!');
    }
}
