<?php

namespace App\Listeners\Reports;

use App\Events\Reports\ReportWasVerified;
use Toastr;

class FlashReportVerified
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReportWasVerified $event
     *
     * @return void
     */
    public function handle(ReportWasVerified $event)
    {
        Toastr::success(ucwords($event->reportType) . ": [{$event->id}] has been successfully verified!", 'Success!');
    }
}
