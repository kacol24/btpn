<?php

namespace App\Listeners\Reports;

use App\Events\Reports\DailyReportWasCreated;
use Debugbar;

/**
 * Class LogDailyReportCreated
 * @package App\Listeners\Reports
 */
class LogDailyReportCreated
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DailyReportWasCreated $event
     *
     * @return void
     */
    public function handle(DailyReportWasCreated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("DAILY REPORT: [{$event->id}] for community [{$event->community}] has been successfully created by [{$event->facilitator}]!");
        }
    }
}
