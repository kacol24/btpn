<?php

namespace App\Listeners\Reports;

use App\Events\Reports\ReportWasDeclined;
use Toastr;

class FlashReportDeclined
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReportWasDeclined $event
     *
     * @return void
     */
    public function handle(ReportWasDeclined $event)
    {
        Toastr::success(ucwords($event->reportType) . ": [{$event->id}] was declined with reason '{$event->reason}'!",
            'Success!');
    }
}
