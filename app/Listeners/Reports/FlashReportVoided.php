<?php

namespace App\Listeners\Reports;

use App\Events\Reports\ReportWasVoided;
use Toastr;

class FlashReportVoided
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReportWasVoided $event
     *
     * @return void
     */
    public function handle(ReportWasVoided $event)
    {
        Toastr::success(ucwords($event->reportType) . ": [{$event->id}] was voided!", 'Success!');
    }
}
