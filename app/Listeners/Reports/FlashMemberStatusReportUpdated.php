<?php

namespace App\Listeners\Reports;

use App\Events\Reports\MemberStatusReportWasUpdated;
use Toastr;

class FlashMemberStatusReportUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MemberStatusReportWasUpdated $event
     *
     * @return void
     */
    public function handle(MemberStatusReportWasUpdated $event)
    {
        Toastr::success("MEMBER STATUS REPORT: [{$event->id}] has been successfully updated!", 'Success!');
    }
}
