<?php

namespace App\Listeners\Reports;

use App\Events\Reports\MemberStatusReportWasUpdated;
use Debugbar;

class LogMemberStatusReportUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MemberStatusReportWasUpdated $event
     *
     * @return void
     */
    public function handle(MemberStatusReportWasUpdated $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("MEMBER STATUS REPORT: [{$event->id}] has been successfully updated!");
        }
    }
}
