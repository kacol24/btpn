<?php

namespace App\Listeners\Reports;

use App\Events\Reports\ReportWasVoided;
use Debugbar;

class LogReportVoided
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReportWasVoided $event
     *
     * @return void
     */
    public function handle(ReportWasVoided $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info(ucwords($event->reportType) . ": [{$event->id}] was voided!");
        }
    }
}
