<?php

namespace App\Listeners\Maturity;

use App\Events\Maturity\AspectWasDeleted;
use Debugbar;

class LogAspectDeleted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AspectWasDeleted $event
     *
     * @return void
     */
    public function handle(AspectWasDeleted $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("ASPECT: [{$event->id}]{$event->name} has been successfully deleted!");
        }
    }
}
