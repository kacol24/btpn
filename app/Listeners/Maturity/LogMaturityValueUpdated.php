<?php

namespace App\Listeners\Maturity;

use App\Events\Maturity\MaturityValueWasUpdated;
use Debugbar;

class LogMaturityValueUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MaturityValueWasUpdated $event
     *
     * @return void
     */
    public function handle(MaturityValueWasUpdated $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("MATURITY VALUE: [{$event->id}]{$event->name} has been successfully updated!");
        }
    }
}
