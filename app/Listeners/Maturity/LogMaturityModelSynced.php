<?php

namespace App\Listeners\Maturity;

use App\Events\Maturity\MaturityModelWasSynced;
use Debugbar;

class LogMaturityModelSynced
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MaturityModelWasSynced $event
     *
     * @return void
     */
    public function handle(MaturityModelWasSynced $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("MATURITY MODEL: [{$event->id}]{$event->name} has been successfully updated!");
        }
    }
}
