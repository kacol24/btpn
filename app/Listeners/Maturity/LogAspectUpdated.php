<?php

namespace App\Listeners\Maturity;

use App\Events\Maturity\AspectWasUpdated;
use Debugbar;

class LogAspectUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AspectWasUpdated $event
     *
     * @return void
     */
    public function handle(AspectWasUpdated $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("ASPECT: [{$event->id}]{$event->name} has been successfully updated!");
        }
    }
}
