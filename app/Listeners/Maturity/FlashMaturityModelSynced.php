<?php

namespace App\Listeners\Maturity;

use App\Events\Maturity\MaturityModelWasSynced;
use Toastr;

class FlashMaturityModelSynced
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MaturityModelWasSynced $event
     *
     * @return void
     */
    public function handle(MaturityModelWasSynced $event)
    {
        Toastr::success("MATURITY MODEL: [{$event->id}]{$event->name} has been successfully updated!", 'Success!');
    }
}
