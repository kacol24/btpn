<?php

namespace App\Listeners\Maturity;

use App\Events\Maturity\MaturityValueWasDeleted;
use Debugbar;

class LogMaturityValueDeleted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MaturityValueWasDeleted $event
     *
     * @return void
     */
    public function handle(MaturityValueWasDeleted $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("MATURITY VALUE: [{$event->id}]{$event->name} has been successfully deleted!");
        }
    }
}
