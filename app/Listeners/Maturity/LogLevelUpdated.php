<?php

namespace App\Listeners\Maturity;

use App\Events\Maturity\LevelWasUpdated;
use Debugbar;

class LogLevelUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LevelWasUpdated $event
     *
     * @return void
     */
    public function handle(LevelWasUpdated $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("LEVEL: [{$event->id}]{$event->name} has been successfully updated!");
        }
    }
}
