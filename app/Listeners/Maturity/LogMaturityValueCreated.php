<?php

namespace App\Listeners\Maturity;

use App\Events\Maturity\MaturityValueWasCreated;
use Debugbar;

class LogMaturityValueCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MaturityValueWasCreated $event
     *
     * @return void
     */
    public function handle(MaturityValueWasCreated $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("MATURITY VALUE: [{$event->id}]{$event->name} has been successfully created!");
        }
    }
}
