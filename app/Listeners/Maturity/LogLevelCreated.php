<?php

namespace App\Listeners\Maturity;

use App\Events\Maturity\LevelWasCreated;
use Debugbar;

class LogLevelCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LevelWasCreated $event
     *
     * @return void
     */
    public function handle(LevelWasCreated $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("LEVEL: [{$event->id}]{$event->name} has been successfully created!");
        }
    }
}
