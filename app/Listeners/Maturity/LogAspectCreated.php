<?php

namespace App\Listeners\Maturity;

use App\Events\Maturity\AspectWasCreated;
use Debugbar;

class LogAspectCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AspectWasCreated $event
     *
     * @return void
     */
    public function handle(AspectWasCreated $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("ASPECT: [{$event->id}]{$event->name} has been successfully created!");
        }
    }
}
