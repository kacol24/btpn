<?php

namespace App\Listeners\Maturity;

use App\Events\Maturity\LevelWasDeleted;
use Debugbar;

class LogLevelDeleted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LevelWasDeleted $event
     *
     * @return void
     */
    public function handle(LevelWasDeleted $event)
    {
        if (env('APP_ENV') == 'local') {
            Debugbar::info("LEVEL: [{$event->id}]{$event->name} has been successfully deleted!");
        }
    }
}
