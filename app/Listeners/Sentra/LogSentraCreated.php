<?php

namespace App\Listeners\Sentra;

use App\Events\Sentra\SentraWasCreated;
use Debugbar;

class LogSentraCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SentraWasCreated $event
     *
     * @return void
     */
    public function handle(SentraWasCreated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("SENTRA: [{$event->id}]{$event->name} has been successfully created!");
        }
    }
}
