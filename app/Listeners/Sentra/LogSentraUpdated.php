<?php

namespace App\Listeners\Sentra;

use App\Events\Sentra\SentraWasUpdated;
use Debugbar;

class LogSentraUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SentraWasUpdated $event
     *
     * @return void
     */
    public function handle(SentraWasUpdated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("SENTRA: [{$event->id}]{$event->name} has been successfully updated!");
        }
    }
}
