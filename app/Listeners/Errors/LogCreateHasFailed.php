<?php

namespace App\Listeners\Errors;

use App\Events\Errors\CreateHasFailed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogCreateHasFailed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateHasFailed  $event
     * @return void
     */
    public function handle(CreateHasFailed $event)
    {
        //
    }
}
