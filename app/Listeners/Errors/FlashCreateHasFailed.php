<?php

namespace App\Listeners\Errors;

use App\Events\Errors\CreateHasFailed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Kamaln7\Toastr\Facades\Toastr;

class FlashCreateHasFailed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateHasFailed  $event
     * @return void
     */
    public function handle(CreateHasFailed $event)
    {
        Toastr::error('Something went wrong when trying to save your data to the database.', 'Whoops!');
    }
}
