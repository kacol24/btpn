<?php

namespace App\Listeners\Community;

use App\Events\Community\CommunityWasDeactivated;
use Debugbar;

class LogCommunityDeactivated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommunityWasDeactivated $event
     *
     * @return void
     */
    public function handle(CommunityWasDeactivated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("COMMUNITY: [{$event->id}]{$event->name} has been successfully deactivated!");
        }
    }
}
