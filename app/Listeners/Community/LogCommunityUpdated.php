<?php

namespace App\Listeners\Community;

use App\Events\Community\CommunityWasUpdated;
use Debugbar;

class LogCommunityUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommunityWasUpdated $event
     *
     * @return void
     */
    public function handle(CommunityWasUpdated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("COMMUNITY: [{$event->id}]{$event->name} has been successfully updated!");
        }
    }
}
