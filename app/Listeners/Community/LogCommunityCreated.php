<?php

namespace App\Listeners\Community;

use App\Events\Community\CommunityWasCreated;
use Debugbar;

class LogCommunityCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommunityWasCreated $event
     *
     * @return void
     */
    public function handle(CommunityWasCreated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("COMMUNITY: [{$event->id}]{$event->name} has been successfully created!");
        }
    }
}
