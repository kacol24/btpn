<?php

namespace App\Listeners\Facilitator;

use App\Events\Facilitator\FacilitatorWasUpdated;
use Debugbar;

class LogFacilitatorUpdated
{
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FacilitatorWasUpdated $event
     *
     * @return void
     */
    public function handle(FacilitatorWasUpdated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("FACILITATOR: [{$event->id}]{$event->name} has been successfully updated!");
        }
    }
}
