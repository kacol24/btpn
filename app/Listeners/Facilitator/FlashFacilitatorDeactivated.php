<?php

namespace App\Listeners\Facilitator;

use App\Events\Facilitator\FacilitatorWasDeactivated;
use Toastr;

class FlashFacilitatorDeactivated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FacilitatorWasDeactivated $event
     *
     * @return void
     */
    public function handle(FacilitatorWasDeactivated $event)
    {
        Toastr::success("FACILITATOR: [{$event->id}] {$event->name} has been successfully deactivated!",
            'Success!');
    }
}
