<?php

namespace App\Listeners\Facilitator;

use App\Events\Facilitator\FacilitatorWasCreated;
use Debugbar;

class LogFacilitatorCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FacilitatorWasCreated $event
     *
     * @return void
     */
    public function handle(FacilitatorWasCreated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("FACILITATOR: [{$event->id}]{$event->name} has been successfully created!");
        }
    }
}
