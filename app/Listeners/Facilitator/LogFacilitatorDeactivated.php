<?php

namespace App\Listeners\Facilitator;

use App\Events\Facilitator\FacilitatorWasDeactivated;
use Debugbar;

class LogFacilitatorDeactivated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FacilitatorWasDeactivated $event
     *
     * @return void
     */
    public function handle(FacilitatorWasDeactivated $event)
    {
        // TODO swap with persistent logging
        if (env('APP_ENV') == 'local') {
            Debugbar::info("FACILITATOR: [{$event->id}]{$event->name} has been successfully deactivated!");
        }
    }
}
