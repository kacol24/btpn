var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
var bower = '../../../bower_components/';
var asset = './resources/assets/';

elixir(function (mix) {
    /*
     |--------------------------------------------------------------------------
     | Compile all bower assets
     |--------------------------------------------------------------------------
     |
     | Compile all bower assets to manageable pieces
     |
     */
    mix.less([ // compile theme styles
        bower + 'AdminLTE/build/less/AdminLTE.less',
        bower + 'AdminLTE/build/less/skins/skin-yellow-light.less'
    ], asset + 'css/theme.css');
    mix.less(bower + 'bootstrap-timepicker/css/timepicker.less', asset + 'css/timepicker.css');

    mix.sass([ // compile plugins styles
        bower + 'bootstrap-sass/assets/stylesheets/bootstrap.scss',
        bower + 'font-awesome/scss/font-awesome.scss',
        bower + 'Ionicons/scss/ionicons.scss',
        bower + 'iCheck/skins/minimal/minimal.css',
        bower + 'datatables/media/css/dataTables.bootstrap.css',
        bower + 'datatables-responsive/css/responsive.bootstrap.scss',
        bower + 'select2/src/scss/core.scss',
        bower + 'bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
        asset + 'css/timepicker.css',
        bower + 'AdminLTE/plugins/pace/pace.css',
        bower + 'sweetalert/dist/sweetalert.css',
        bower + 'toastr/toastr.css'
    ], asset + 'css/vendor.css');

    mix.scripts(bower + 'jquery/dist/jquery.js', 'public/js/jquery.min.js'); // copy jquery
    mix.scripts(bower + 'AdminLTE/plugins/pace/pace.min.js', 'public/js/pace.min.js'); // copy pace

    // copy fonts
    mix.copy('bower_components/font-awesome/fonts', 'public/fonts/font-awesome');
    mix.copy('bower_components/bootstrap-sass/assets/fonts/bootstrap', 'public/fonts/bootstrap');
    mix.copy('bower_components/Ionicons/fonts', 'public/fonts/ionicons');

    mix.scripts([ // compile plugins` scripts
        bower + 'bootstrap-sass/assets/javascripts/bootstrap.min.js',
        bower + 'select2/dist/js/select2.full.js',
        bower + 'datatables/media/js/jquery.dataTables.js',
        bower + 'datatables/media/js/dataTables.bootstrap.js',
        bower + 'datatables-responsive/js/dataTables.responsive.js',
        bower + 'datatables-responsive/js/responsive.bootstrap.js',
        bower + 'slimScroll/jquery.slimscroll.min.js',
        bower + 'iCheck/icheck.js',
        bower + 'bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
        bower + 'bootstrap-timepicker/js/bootstrap-timepicker.js',
        bower + 'fastclick/lib/fastclick.js',
        bower + 'AdminLTE/dist/js/app.min.js',
        bower + 'laravel-bootstrap-modal-form/src/laravel-bootstrap-modal-form.js',
        'eModal.js',
        bower + 'chained/jquery.chained.remote.js',
        bower + 'sweetalert/dist/sweetalert.min.js',
        bower + 'ChartNew.js/ChartNew.js',
        bower + 'toastr/toastr.min.js',
        bower + 'jquery.dirtyforms/jquery.dirtyforms.js',
        bower + 'highcharts/highcharts.js',
        bower + 'highcharts/modules/no-data-to-display.js',
    ], 'public/js/plugins.js');

    // copy required icheck skin
    mix.copy('bower_components/iCheck/skins/minimal/minimal.png', 'public/images/icheck');
    mix.copy('bower_components/iCheck/skins/minimal/minimal@2x.png', 'public/images/icheck');
    // ==================================================

    /*
     |--------------------------------------------------------------------------
     | Compile application assets
     |--------------------------------------------------------------------------
     |
     | Compile all assets that is specific for the app
     |
     */
    mix.styles([
        'vendor.css',
        'theme.css'
    ], 'public/css/vendor.css');
    mix.sass('app.scss');
    mix.scripts('app.js');
});
