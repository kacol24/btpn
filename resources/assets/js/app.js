// Application namespace object
var BTPN = {
    chainContainer: [],
    remoteChained: function (child, parent, url) {
        this.chainContainer.push({
            child: child,
            parent: parent,
            url: url
        });
        $(child).remoteChained({
            parents: parent,
            url: url,
            loading: 'Loading...'
        });
    },
    initSelect2: function (selector) {
        if (selector === undefined) {
            selector = '.select2';
        }
        $(selector).css('width', '100%').select2({
            allowClear: true,
            placeholder: 'Please select..',
            minimumResultsForSearch: 5
        });
    },
    initIcheck: function (selector) {
        if (selector === undefined) {
            selector = 'input[type="checkbox"].icheck, input[type="radio"].iradio';
        }
        $(selector).iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });
    },
    setupAjaxModal: function () {
        eModal.setEModalOptions({
            loadingHtml: '<div class="center-block text-center"><div class="fa fa-circle-o-notch fa-spin fa-3x text-primary"></div><div class="h4">Loading</div></div>',
            buttons: false
        });
    },
    ajaxModal: function (selector, size) {
        if (selector === undefined) {
            selector = '.ajax-modal';
        }
        if (size === undefined) {
            size = false;
        }

        $(document).on('click', selector, function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            var title = $(this).data('title');

            eModal
                .ajax({
                    url: url,
                    title: title,
                    size: size
                })
                .then(function () {
                    this.modalAutofocus;

                    var $modal = $('.modal');

                    $modal.find('.select2').css('width', '100%').select2({
                        placeholder: 'Please select..',
                        minimumResultsForSearch: 5
                    });
                    $modal.find('input[type="checkbox"].icheck, input[type="radio"].iradio').iCheck({
                        checkboxClass: 'icheckbox_minimal',
                        radioClass: 'iradio_minimal'
                    });
                    $modal.find('.monthpicker').datepicker({
                        format: "yyyy-mm",
                        startView: 1,
                        minViewMode: 1,
                        orientation: "bottom auto",
                        autoclose: true
                    });
                    $modal.find('.dobpicker').datepicker({
                        format: "yyyy-mm-dd",
                        endDate: "-1d",
                        startView: 2,
                        orientation: "bottom auto",
                        autoclose: true,
                        endDate: "today"
                    });
                    $modal.find('form:not(.dirtyignore)').each(function () {
                        $(this).dirtyForms();
                        $(this).find('[type="reset"],[type="submit"]').attr('disabled', 'disabled');
                    });

                    $.each(BTPN.chainContainer, function (index, value) {
                        $(value.child).remoteChained({
                            parents: value.parent,
                            url: value.url,
                            loading: 'Loading...'
                        });
                    });

                    this.datePicker;
                });
        });
    },
    modalAutofocus: function () {
        $('.modal').find('[autofocus]').focus();
    },
    datePicker: function (selector) {
        if (selector === undefined) {
            selector = '.datepicker';
        }

        $(selector).datepicker({
            format: "yyyy-mm-dd",
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            todayBtn: "linked"
        });
    },
    yearPicker: function (selector) {
        if (selector === undefined) {
            selector = '.yearpicker';
        }

        $(selector).datepicker({
            format: "yyyy",
            startView: 1,
            minViewMode: 2,
            orientation: "bottom auto",
            autoclose: true
        });
    },
    monthPicker: function (selector) {
        if (selector === undefined) {
            selector = '.monthpicker';
        }

        $(selector).datepicker({
            format: "yyyy-mm",
            startView: 1,
            minViewMode: 1,
            orientation: "bottom auto",
            autoclose: true
        });
    },
    dobPicker: function (selector) {
        if (selector === undefined) {
            selector = '.dobpicker';
        }

        $(selector).datepicker({
            format: "yyyy-mm-dd",
            endDate: "-1d",
            startView: 2,
            orientation: "bottom auto",
            autoclose: true,
            endDate: "today"
        });
    },
    setupDatatable: function () {
        $.extend(true, $.fn.dataTable.defaults, {
            stateSave: true,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            oLanguage: {
                sProcessing: '<i class="fa fa-spin fa-refresh"></i> Please wait..'
            }
        });
    },
    defaultTable: function (selector) {
        if (selector === undefined) {
            selector = '.dt-default';
        }

        $(selector).DataTable({
            processing: false,
            serverSide: false,
            deferRender: false
        });
    },
    simpleTable: function (selector) {
        if (selector === undefined) {
            selector = '.dt-simple';
        }

        $(selector).DataTable({
            processing: false,
            serverSide: false,
            deferRender: false,
            ordering: false,
            paging: false,
            info: false
        });
    },
    dirtyForms: function () {
        $('form').dirtyForms({
            dialog: false,
            fieldSelector: "input:not([type='button'],[type='image'],[type='submit'],[type='reset'],[type='search']),select,textarea"
        });
        $.DirtyForms.dialog = false;

        $(document).on('dirty.dirtyforms clean.dirtyforms', 'form', function (ev) {
            var $form = $(ev.target);
            var $submitResetButtons = $form.find('[type="reset"],[type="submit"]');
            if (ev.type === 'dirty') {
                $submitResetButtons.removeAttr('disabled');
            } else {
                $submitResetButtons.attr('disabled', 'disabled');
            }
        });
    },
    timepicker: function (selector) {
        if (selector === undefined) {
            selector = '.timepicker';
        }

        $(selector).timepicker({
            showInputs: false,
            showMeridian: false,
            disableFocus: true
        });
    },
    pieChart: function (selector, data, chartTitle) {
        var $element = $(selector);

        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $element.get(0).getContext("2d");
        var PieData = data;
        var pieChart = new Chart(pieChartCanvas);
        var pieOptions = {
            graphTitle: chartTitle || "",
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 2,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 0, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - Whether to animate the chart
            animation: false,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
            inGraphDataShow: true
        };
        pieChart.Pie(PieData, pieOptions);

        return pieChart;
    },
    barChart: function (selector, data, chartTitle) {
        var $element = $(selector);

        var barChartCanvas = $element.get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = data;
        var barChartOptions = {
            graphTitle: chartTitle || "",
            //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
            scaleBeginAtZero: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - If there is a stroke on each bar
            barShowStroke: true,
            //Number - Pixel width of the bar stroke
            barStrokeWidth: 2,
            //Number - Spacing between each of the X value sets
            barValueSpacing: 5,
            //Number - Spacing between data sets within X values
            barDatasetSpacing: 1,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
            responsive: true,
            animation: false
        };

        barChartOptions.datasetFill = false;
        barChart.Bar(barChartData, barChartOptions);

        return barChart;
    },
    lineChart: function (selector, data, chartTitle) {
        var $element = $(selector);

        var lineChartCanvas = $element.get(0).getContext("2d");
        var lineChart = new Chart(lineChartCanvas);
        var lineChartData = data;
        var lineChartOptions = {
            graphTitle: chartTitle || "",
            //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
            scaleBeginAtZero: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - If there is a stroke on each bar
            barShowStroke: true,
            //Number - Pixel width of the bar stroke
            barStrokeWidth: 2,
            //Number - Spacing between each of the X value sets
            barValueSpacing: 5,
            //Number - Spacing between data sets within X values
            barDatasetSpacing: 1,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
            legend: true,
            logarithmic: true,
            responsive: true,
            animation: false
        };

        lineChartOptions.datasetFill = false;
        lineChart.Line(lineChartData, lineChartOptions);

        return lineChart;
    },
    chartRefresh: (function () {
        // the overlay
        var $overlay = $('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>');

        function start($box) {
            $box.append($overlay);
        }

        function done($box) {
            $box.find($overlay).remove();
        }

        function refresh(selector, originalData, $box, url, title) {
            start($box);

            var ctx = $(selector).get(0).getContext("2d");
            $.ajax({
                type: 'GET',
                url: url
            }).done(function (data) {
                originalData.datasets[0].data = data;
                updateChart(ctx, originalData, {responsive: true, graphTitle: title || ''}, true);
                done($box);
            });
        }

        return {
            refresh: refresh
        };
    })(),
    eventListeners: function () {
        $(document).on('shown.bs.modal', function () {
            this.modalAutofocus;
            this.initSelect2;
            this.initIcheck;
            this.monthPicker;
            this.datePicker;
            $('.modal').find('form:not(.dirtyignore)').each(function () {
                $(this).find('[type="reset"],[type="submit"]').attr('disabled', 'disabled');
                $(this).dirtyForms('setClean');
            });
        });
        $(document).on('hidden.bs.modal', function () {
            var $modal = $('.modal');
            $modal.find('form').each(function () {
                if ($(this).dirtyForms('isDirty')) {
                    $modal.find('.select2').each(function () {
                        $(this).val(null).trigger('change');
                    });
                }
                this.reset();
                $(this).dirtyForms('setClean');
            });
            $('.modal [data-toggle="buttons"]').find('label').each(function () {
                $(this).removeClass('active');
            });
        });
        $(document).ajaxStart(function () {
            Pace.restart();
        });
        $(document).on('click', "form[data-confirm] [type='submit']", function (e) {
            e.preventDefault();

            // cache the dom
            var $button = $(this);
            var $form = $button.closest('form');
            var msg = $form.data('confirm');

            swal({
                title: msg || 'Are you sure?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Continue",
                closeOnConfirm: true
            }, function () {
                $form.submit();
            });
        });
    }
};

// Setting defaults for datatables
BTPN.setupDatatable();

// Setting defaults for eModal
BTPN.setupAjaxModal();

// Attach event listeners
BTPN.eventListeners();

$(function () {
    // Initialize datatables for default settings
    BTPN.defaultTable();
    BTPN.simpleTable();

    // Initialize select2
    BTPN.initSelect2();

    // Initialize Bootstrap datepicker
    BTPN.monthPicker();
    BTPN.datePicker();
    BTPN.dobPicker();
    BTPN.yearPicker();

    // Initialize iCheck
    BTPN.initIcheck();

    // Initialize eModal binds and trigger
    BTPN.ajaxModal();
    BTPN.ajaxModal('.ajax-modal-lg', 'lg');
    BTPN.ajaxModal('.ajax-modal-xl', 'xl');

    // Initialize DirtyForms
    BTPN.dirtyForms();

    // Initialize Timepicer
    BTPN.timepicker();

    $('.dirtyignore').find('[type="reset"],[type="submit"]').removeAttr('disabled');
});