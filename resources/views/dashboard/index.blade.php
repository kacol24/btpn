@extends('layouts.default')

@section('page-title', 'Dashboard')

@section('content')
    {{--GENERAL INFORMATION--}}
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-home"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Communities</span>
                    <span class="info-box-number">{{ $communities->count() }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-sitemap"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Sentra</span>
                    <span class="info-box-number">{{ $sentra->count() }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-bar-chart"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Program Update<br>Reports</span>
                    <span class="info-box-number">{{ $programUpdateReports->count() }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-bar-chart"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Member Status<br>Report</span>
                    <span class="info-box-number">{{ $memberStatusReports->count() }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>

    @if(Auth::user()->hasRole('headquarter'))
        {{--COMMUNITY GENERAL INFORMATION--}}
        @include('dashboard.partials._commuinty-general-information')

        {{--ACTIVITY INFORMATION--}}
        @include('dashboard.partials._activity-information')

        {{--PARTICIPANT INFORMATION--}}
        @include('dashboard.partials._participants-information')
    @endif
@endsection