<div class="box box-solid" id="annual_community_members">
    <div class="box-header">
        <div class="row">
            <div class="col-md-12">
                <div class="input-group">
                    {!! AdamForm::text('year')->addClass('form-control yearpicker')->id('annual_community_members_year')->value(Carbon::now()->year) !!}
                    <div class="input-group-btn">
                        <button id="annual_community_members_button" class="btn btn-default"
                                data-loading-text="Loading...">
                            <i class="fa fa-refresh"></i> Refresh
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="chart">
            <div id="annual_community_members_chart"></div>
        </div>
    </div>
</div>

@section('footer-scripts')
    <script>
        $(function () {
            var annual_community_members = {};

            annual_community_members.chartSelector = '#annual_community_members_chart';
            annual_community_members.settings = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Community Members',
                    x: -20
                },
                xAxis: {
                    categories: monthsOfTheYear
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total members'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                            style: {
                                textShadow: '0 0 3px black'
                            }
                        }
                    }
                },
                series: annualCommunityMembers,
                credits: {
                    enabled: false
                }
            };
            annual_community_members.chart = $(annual_community_members.chartSelector).highcharts(annual_community_members.settings);
            annual_community_members.$button = $('#annual_community_members_button');
            annual_community_members.$year = $('#annual_community_members_year');

            annual_community_members.$button.click(function (e) {
                e.preventDefault();

                var chart = $(annual_community_members.chartSelector).highcharts();
                var $button = $(this);
                var year = annual_community_members.$year.val();
                var url = "{!! route('dashboard.community-members-chart') !!}";
                chart.showLoading();
                $button.button('loading');
                annual_community_members.$year.attr('disabled', true);

                $.ajax({
                    url: url,
                    data: {
                        year: year
                    }
                }).done(function (response) {
//                    chart.series[0].setData(response);
                    $.each(response, function (index, value) {
                        chart.series[index].setData(value.data);
                    });
                    chart.hideLoading();
                    $button.button('reset');
                    annual_community_members.$year.removeAttr('disabled');
                });
            });
        });
    </script>
@append