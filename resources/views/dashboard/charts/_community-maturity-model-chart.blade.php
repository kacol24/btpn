<div class="chart">
    <canvas id="community_maturity_progress" style="height:250px"></canvas>
</div>

@section('footer-scripts')
    <script>
        $(function () {
            var communityMaturityProgressData = {
                labels: monthsOfTheYear,
                datasets: [
                    {
                        label: "Electronics",
                        fillColor: "rgba(210, 214, 222, 1)",
                        strokeColor: "rgba(210, 214, 222, 1)",
                        pointColor: "rgba(210, 214, 222, 1)",
                        pointStrokeColor: "#c1c7d1",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: communityMaturityData.graph.y
                    }
                ]
            }

            BTPN.barChart('#community_maturity_progress', communityMaturityProgressData, 'Community Maturity Progress')
        })
    </script>

@append