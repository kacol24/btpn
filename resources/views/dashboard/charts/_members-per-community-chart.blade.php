<div class="box box-solid" id="members_per_community">
    <div class="box-body">
        <div class="chart">
            {{--<canvas id="members_per_community_chart" style="height:300px"></canvas>--}}
            <div id="members_per_community_chart"></div>
        </div>
    </div>
</div>

@section('footer-scripts')
    <script>
        $(function () {
            var members_per_community = {};

            members_per_community.chartSelector = '#members_per_community_chart';
            members_per_community.settings = {
                title: {
                    text: 'Member(s) per Community',
                    x: -20
                },
                xAxis: {
                    categories: monthsOfTheYear
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: membersPerCommunityData,
                credits: {
                    enabled: false
                }
            };
            $(members_per_community.chartSelector).highcharts(members_per_community.settings);
        });
    </script>
@append