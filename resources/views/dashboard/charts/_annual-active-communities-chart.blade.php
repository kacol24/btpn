<div class="box box-solid" id="active_communities_information">
    <div class="box-header">
        <div class="row">
            <div class="col-md-12">
                <div class="input-group">
                    {!! AdamForm::text('year')->addClass('form-control yearpicker')->id('active_communities_year')->value(Carbon::now()->year) !!}
                    <div class="input-group-btn">
                        <button id="active_communities_button" class="btn btn-default" data-loading-text="Loading...">
                            <i class="fa fa-refresh"></i> Refresh
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="chart">
            {{--<canvas id="annual_active_community_chart" style="height:300px"></canvas>--}}
            <div id="annual_active_community_chart"></div>
        </div>
    </div>
</div>

@section('footer-scripts')
    <script>
        $(function () {
            var active_communities_information = {};

            active_communities_information.chartSelector = '#annual_active_community_chart';
            active_communities_information.settings = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Annual Detail View',
                    x: -20
                },
                xAxis: {
                    categories: monthsOfTheYear
                },
                series: [
                    {
                        name: 'Active Communities',
                        data: annualActiveCommunityData
                    }
                ],
                credits: {
                    enabled: false
                }
            };
            active_communities_information.chart = $(active_communities_information.chartSelector).highcharts(active_communities_information.settings);
            active_communities_information.$button = $('#active_communities_button');
            active_communities_information.$year = $('#active_communities_year');

            active_communities_information.$button.click(function (e) {
                e.preventDefault();

                var chart = $(active_communities_information.chartSelector).highcharts();
                var $button = $(this);
                var year = active_communities_information.$year.val();
                var url = "{!! route('dashboard.active-communities-chart') !!}";
                chart.showLoading();
                $button.button('loading');
                active_communities_information.$year.attr('disabled', true);

                $.ajax({
                    url: url,
                    data: {
                        year: year
                    }
                }).done(function (response) {
                    chart.series[0].setData(response);
                    chart.hideLoading();
                    $button.button('reset');
                    active_communities_information.$year.removeAttr('disabled');
                });
            });
        });
    </script>
@append