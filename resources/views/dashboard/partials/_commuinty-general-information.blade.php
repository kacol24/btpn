<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Community General Information</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                @include('dashboard.charts._annual-active-communities-chart')
            </div>
            <div class="col-md-6">
                @include('dashboard.charts._annual-community-members-chart')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('dashboard.charts._members-per-community-chart')
            </div>
        </div>
    </div>
</div>