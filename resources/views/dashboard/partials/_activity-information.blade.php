<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Activity Information</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-6">
                        <div class="input-group">
                            {!! AdamForm::text('year')->addClass('form-control yearpicker')->id('activity_information_year')->value(Carbon::now()->year) !!}
                            <div class="input-group-btn">
                                <button id="activity_information_button"
                                        class="btn btn-default"
                                        data-loading-text="Loading...">
                                    <i class="fa fa-refresh"></i> Refresh
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @include('dashboard.charts._total-community-activities')
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @include('dashboard.charts._community-activity-types')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('footer-scripts')
    <script>
        $(function () {
            var total_community_activities = {};

            total_community_activities.chartSelector = '#total_community_activities_chart';
            total_community_activities.settings = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Total Community Activities',
                    x: -20
                },
                xAxis: {
                    categories: monthsOfTheYear
                },
                series: [
                    {
                        name: 'Activities',
                        data: totalCommunityActivities
                    }
                ],
                credits: {
                    enabled: false
                }
            };
            total_community_activities.chart = $(total_community_activities.chartSelector).highcharts(total_community_activities.settings);

            var community_activity_types = {};

            community_activity_types.chartSelector = '#community_activity_types';
            community_activity_types.settings = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Community Activity Types',
                    x: -20
                },
                xAxis: {
                    categories: monthsOfTheYear
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: communityActivityTypes,
                credits: {
                    enabled: false
                }
            };
            community_activity_types.chart = $(community_activity_types.chartSelector).highcharts(community_activity_types.settings);

            var community_activity_information = {};
            community_activity_information.$button = $('#activity_information_button');
            community_activity_information.$year = $('#activity_information_year');

            community_activity_information.$button.click(function (e) {
                e.preventDefault();

                var totalChart = $(total_community_activities.chartSelector).highcharts();
                var typeChart = $(community_activity_types.chartSelector).highcharts();
                var $button = $(this);
                var year = community_activity_information.$year.val();
                var totalUrl = "{!! route('dashboard.total-community-activities') !!}";
                var typeUrl = "{!! route('dashboard.community-activity-types') !!}";

                totalChart.showLoading();
                typeChart.showLoading();
                $button.button('loading');
                community_activity_information.$year.attr('disabled', true);

                // total activities
                $.ajax({
                    url: totalUrl,
                    data: {
                        year: year
                    }
                }).done(function (response) {
                    totalChart.series[0].setData(response);
                });

                // acivity types
                $.ajax({
                    url: typeUrl,
                    data: {
                        year: year
                    }
                }).done(function (response) {
                    $.each(response, function (index, value) {
                        typeChart.series[index].setData(value.data);
                    });
                });

                totalChart.hideLoading();
                typeChart.hideLoading();
                $button.button('reset');
                community_activity_information.$year.removeAttr('disabled');
            });
        });
    </script>
@append