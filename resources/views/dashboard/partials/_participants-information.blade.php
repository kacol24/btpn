<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Participant Information</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-6">
                        <div class="input-group">
                            {!! AdamForm::text('year')->addClass('form-control yearpicker')->id('participant_information_year')->value(Carbon::now()->year) !!}
                            <div class="input-group-btn">
                                <button id="participant_information_button"
                                        class="btn btn-default"
                                        data-loading-text="Loading...">
                                    <i class="fa fa-refresh"></i> Refresh
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @include('dashboard.charts._total-community-activity-participant-chart')
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @include('dashboard.charts._participants-per-activity-type-chart')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('footer-scripts')
    <script>
        $(function () {
            var total_community_activity_participant = {};
            total_community_activity_participant.chartSelector = '#total_community_activity_participant_chart';
            total_community_activity_participant.settings = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Total Community Activity Participants',
                    x: -20
                },
                xAxis: {
                    categories: monthsOfTheYear
                },
                series: [
                    {
                        name: 'Activities',
                        data: communityActivityParticipants
                    }
                ],
                credits: {
                    enabled: false
                }
            };
            total_community_activity_participant.chart = $(total_community_activity_participant.chartSelector).highcharts(total_community_activity_participant.settings);

            var participants_per_activity_type = {};
            participants_per_activity_type.chartSelector = '#participants_per_activity_type_chart';
            participants_per_activity_type.settings = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Participants per Activity Type',
                    x: -20
                },
                xAxis: {
                    categories: monthsOfTheYear
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: participantsPerActivity,
                credits: {
                    enabled: false
                }
            };
            participants_per_activity_type.chart = $(participants_per_activity_type.chartSelector).highcharts(participants_per_activity_type.settings);

            var participants_information = {};

            participants_information.$button = $('#participant_information_button');
            participants_information.$year = $('#participant_information_year');
            participants_information.$button.click(function (e) {
                e.preventDefault();

                var totalChart = $(total_community_activity_participant.chartSelector).highcharts();
                var typeChart = $(participants_per_activity_type.chartSelector).highcharts();
                var $button = $(this);
                var year = participants_information.$year.val();
                var totalUrl = "{!! route('dashboard.community-activity-participants') !!}";
                var typeUrl = "{!! route('dashboard.participants-per-activity-type') !!}";

                totalChart.showLoading();
                typeChart.showLoading();
                $button.button('loading');
                participants_information.$year.attr('disabled', true);

                // total activities
                $.ajax({
                    url: totalUrl,
                    data: {
                        year: year
                    }
                }).done(function (response) {
                    totalChart.series[0].setData(response);
                });

                // acivity types
                $.ajax({
                    url: typeUrl,
                    data: {
                        year: year
                    }
                }).done(function (response) {
                    $.each(response, function (index, value) {
                        typeChart.series[index].setData(value.data);
                    });
                });

                totalChart.hideLoading();
                typeChart.hideLoading();
                $button.button('reset');
                participants_information.$year.removeAttr('disabled');
            });
        });
    </script>
@append