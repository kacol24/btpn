@extends('layouts.default')

@section('content')
    <h1>TODO</h1>
    <ul>
        <li>Clean up more code</li>
        <li>
            Solve issues that exists/appears.
            <small>Report issues to <a href="https://bitbucket.org/kacol24/btpn/issues">bitbucket</a>.</small>
        </li>
    </ul>

    <h1>Roadmap</h1>
    <ul>
        <li>
            <del>Finish all the modules</del>
        </li>
        <li>
            <del>Add authentication and authorization</del>
        </li>
        <li>Extract all end points to an API</li>
        <li>
            <del>Refactor database to be more streamlined</del>
        </li>
        <li>
            <del>Refactor code to follow SOLID principle (?)</del>
        </li>
        <li>
            <del>Utilize DB Transactions for saving relationship data</del>
        </li>
        <li>Add testing to all modules</li>
        <li>Add try catch to commands</li>
        <li>Added seeding for dummy data (?)</li>
        <li>Show personalized dashboard for each role</li>
    </ul>

    <h1>Changelog</h1>
    <ul class="timeline">
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.10.0] Added flash notification for users</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.10.0</li>
                        <li>Added pdf generation for report using barryvdh/dompdf
                            <small>[0.9.1]</small>
                        </li>
                        <li>Debugbar logging only enabled for local development
                            <small>[0.9.2]</small>
                        </li>
                        <li>New models are refactored to use the repository pattern
                            <small>[0.9.3]</small>
                        </li>
                        <li>Refactor new modules to use command/events/listeners pattern</li>
                        <li>Member status report is now persisted to database, before it was using data from members
                            table
                            <small>[0.9.4]</small>
                        </li>
                        <li>Clean up some comments on view</li>
                        <li>Clean up unused controller methods and routes</li>
                        <li>Fix: communities dropdown for facilitator is now displayed correctly when creating a
                            report
                        </li>
                        <li>Added confirmation dialog on each operations to confirm the action
                            <small>[0.9.5]</small>
                        </li>
                        <li>Optimized members' table structure in database</li>
                        <li>Added activities counter on single community view</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.9.0] Finished report generation</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.9.0</li>
                        <li>Added reports generation module</li>
                        <li>Fix MMS was not being created</li>
                        <li>Fix active class not being applied to reports navigation menu</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.8.0] Finished Community Maturity</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.8.0</li>
                        <li>Added revision for all reports
                            <small>[0.7.1] - [0.7.2] - [0.7.3]</small>
                        </li>
                        <li>Fix declined report status</li>
                        <li>Fix wrong redirection after revision</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.7.0] Finished Maturity Model</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.7.0</li>
                        <li>Finished CRUD for aspects on CMM</li>
                        <li>Finished CRUD for levels on CMM</li>
                        <li>Finished CRUD for maturity value on CMM</li>
                        <li>Login errors are now displayed as a list</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.6.0] Finished reports verification on headquarter's role</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.6.0</li>
                        <li>Reports verification and decline is now working for all 3 report types</li>
                        <li>Added notification/indicator badge on sidebar navigation for pending reports for review</li>
                        <li>Void a report before being verified/declined is now working.</li>
                        <li>Added sweet alert js plugin for reason of decline</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.5.0] Partially finish check document status for facilitator's role</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.5.0</li>
                        <li>Refactor code to use polymorphic relationship for reports</li>
                        <li>Report entity is now prepared for use with verification</li>
                        <li>Fix a couple of errors because of new refactored code and database</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.4.0] Finished MMS view member status report and view program update
                    report</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.4.0</li>
                        <li>Added view member status report for MMS role</li>
                        <li>Added view program update report for MMS role</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.3.0] Finished facilitator's monthly member status mpdule</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.3.0</li>
                        <li>Added facilitator's monthly member status module</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.2.0] Finished facilitator's monthly program update mpdule</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.2.0</li>
                        <li>Added facilitator's monthly program update module</li>
                        <li>Add singleton pattern to datatables on report pages
                            <small>[0.1.1]</small>
                        </li>
                        <li>Member's attendance is being save in the database
                            <small>[0.1.2]</small>
                        </li>
                        <li>Added form validation to prevent duplicate entries on daily report
                            <small>[0.1.3]</small>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.1.0] Finished (partially) facilitator's submit daily report module</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.1.0 to indicate that the core is stable enough but not yet ready for
                            production
                        </li>
                        <li>Codes now follow SOLID principle (repository pattern, commands, event-listeners)</li>
                        <li>Profile image uploading is now functional</li>
                        <li>Authentication and authoirzation on each request is applied</li>
                        <li>Fix: form with chained selects now only reloads when dirty (added dirtyforms plugin)</li>
                        <li>Fix: some chained dropdowns wasn't working inside ajax loaded modals</li>
                        <li>Improvement: added laravel-caffeine to prevent csrf token from timing out</li>
                        <li>Cleaned up more code</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.0.8] Finished user authentication and authorization</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.0.8</li>
                        <li>Authentication is now finished</li>
                        <li>Authoirzation for different roles are finished</li>
                        <li>Fix javascript ux for date of birth field</li>
                        <li>Clean up a little bit of controller code</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.0.7] Finished most of the basic modules for each role</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.0.7</li>
                        <li>Completed communities module for mms and facilitators</li>
                        <li>Completed sentra module for mms role</li>
                        <li>Fix simple datatable not being initialized in modals</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.0.6] Finished members module for headquarter role</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.0.6</li>
                        <li>Completed members module</li>
                        <li>Completed facilitators module</li>
                        <li>Completed communities module</li>
                        <li>Fix some javascripts reagrding ajax submit and plugins</li>
                        <li>Fix select2 sometimes not being initialized in modal</li>
                        <li>Some application API/end point have been refactored</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.0.5] Partially finished communities module for headquarter</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.0.5</li>
                        <li>Communities module is partially finished, missing view and deactivate community</li>
                        <li>Fix some missing DOM elements</li>
                        <li>Using view composers to serve some of the data</li>
                        <li>Fix select2 sometimes not being initialized in modal</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.0.4] Partially finished facilitators module for headquarter role</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.0.4</li>
                        <li>Facilitators module is partially finished, missing faciliator's responsibility</li>
                        <li>Add seeder files for some models: City, Facilitator, MMS, Sentra</li>
                        <li>Modify composer to seed database after install</li>
                        <li>Removed search box on all datatables</li>
                        <li>Fix: Updating MMS was not working</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.0.3] Finished Sentra module for headquarter role</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.0.3</li>
                        <li>Disable un-needed routes for the moment</li>
                        <li>Add migration after deploying to heroku</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.0.2] Finished MMS module for headquarter role</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Bump app version to 0.0.2</li>
                        <li>Temporary fix for select2 dropdown on ajax modal</li>
                        <li>Added relationships between models</li>
                        <li>Fix datatables to be responsive</li>
                    </ul>
                </div>
            </div>
        </li>
        <li>
            <!-- timeline icon -->
            <i class="fa fa-arrow-up bg-blue"></i>

            <div class="timeline-item">
                <h3 class="timeline-header">[0.0.1] Finished 'Cities' module for headquarter role</h3>

                <div class="timeline-body">
                    <ul>
                        <li>Added application versioning</li>
                        <li>Make homepage/dashboard as changelog list temporarily</li>
                    </ul>
                </div>
            </div>
        </li>
        <!-- END timeline item -->
        <li>
            <i class="fa fa-tasks bg-gray"></i>
        </li>
    </ul>
@endsection