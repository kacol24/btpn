@extends('layouts.master')

@section('layout')

@include('includes._header')
@include('includes._sidebar')

        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            @yield('page-title', 'Page Header')
        </h1>
        {!! Breadcrumbs::render() !!}
    </section>

    <!-- Main content -->
    <section class="{{ isset($preview) && $preview ? 'invoice' : 'content' }}">

        @yield('content')

    </section>
    <!-- /.content -->
    {!! isset($preview) && $preview ? '<div class="clearfix"></div>' : '' !!}
</div>
<!-- /.content-wrapper -->

@include('includes._footer')
@endsection