@extends('layouts.master')

@section('layout')

    <section class="invoice">
        @yield('content')
    </section>

    @include('includes._footer')
@endsection