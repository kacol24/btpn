<!DOCTYPE html>
<html>
<head>
    <script src="{{ asset('js/pace.min.js') }}"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('page-title', 'Daya Tumbuh Komunitas') | BTPN Bank </title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Vendor + theme styles -->
    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <!-- Custom styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-yellow-light sidebar-mini fixed">
<div class="wrapper">

    @yield('layout')

</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 2.1.4 -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- Plugins -->
<script src="{{ asset('js/plugins.js') }}"></script>
<!-- App -->
<script src="{{ asset('js/app.js') }}"></script>
@yield('footer-scripts')
{!! Toastr::render() !!}
</body>
</html>