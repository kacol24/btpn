<div class="row">
    <div class="col-sm-6">
        <fieldset>
            <legend>Community Leader</legend>
            {!! BootForm::open() !!}
            {!! BootForm::text('Name', 'leader_name')->value(isset($community_leader->name) ? $community_leader->name : '-')->readOnly() !!}
            {!! BootForm::text('Customer No.', 'account_number')->value(isset($community_leader->account_number) ? $community_leader->account_number : '-')->readOnly() !!}
            {!! BootForm::text('Phone', 'leader_phone')->value(isset($community_leader->phone) ? $community_leader->phone : '-')->readOnly() !!}
            {!! BootForm::textarea('Address', 'leader_address')->value(isset($community_leader->address) ? $community_leader->address : '-')->readOnly()->rows(3) !!}
            {!! BootForm::close() !!}
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <fieldset>
            <legend>Community Members</legend>
            <table class="table table-condensed table-striped table-bordered dt-simple dt-responsive">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Customer No.</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($community->members as $member)
                    <tr>
                        <td class="text-right">
                            {{ $member->id }}
                        </td>
                        <td>
                            {{ $member->name }}
                        </td>
                        <td>
                            {{ $member->account_number }}
                        </td>
                        <td>
                            @if($member->is_active)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">Inactive</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <fieldset>
            <legend>Activity History</legend>
            <table class="table table-condensed table-striped table-bordered dt-simple dt-responsive">
                <thead>
                <tr>
                    <th>Total Meeting</th>
                    <th>Total Visit</th>
                    <th>Total Training</th>
                    <th>Total Exhibition</th>
                    <th>Total Activities</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-right">
                        <?php
                        $meeting = $reports->filter(function ($item) {
                            return $item->report->activity_type_id == 1;
                        });
                        ?>
                        {{ $meeting->count() }}
                    </td>
                    <td class="text-right">
                        <?php
                        $visit = $reports->filter(function ($item) {
                            return $item->report->activity_type_id == 2;
                        });
                        ?>
                        {{ $visit->count() }}
                    </td>
                    <td class="text-right">
                        <?php
                        $training = $reports->filter(function ($item) {
                            return $item->report->activity_type_id == 3;
                        });
                        ?>
                        {{ $training->count() }}
                    </td>
                    <td class="text-right">
                        <?php
                        $exhibition = $reports->filter(function ($item) {
                            return $item->report->activity_type_id == 4;
                        });
                        ?>
                        {{ $exhibition->count() }}
                    </td>
                    <td class="text-right">
                        <strong>{{ $reports->count() }}</strong>
                    </td>
                </tr>
                </tbody>
            </table>
        </fieldset>
    </div>
</div>
<script>
    $(function () {
        BTPN.simpleTable();
    });
</script>