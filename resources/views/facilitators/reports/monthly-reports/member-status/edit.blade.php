@extends('layouts.default')

@section('page-title', 'Edit Member Status Report')

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-push-8">
            <div id="report_parameter" class="box">
                <div class="box-header">
                    <h3 class="box-title">Report Parameter</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <?php $columnSizes = ['md' => [5, 7], 'sm' => [4, 8]]; ?>
                    <div class="row">
                        <div class="col-xs-12">
                            {!! BootForm::openHorizontal($columnSizes)->id('report_parameter_form')->multipart()->put()->action(route('facilitators.reports.monthly-report.member-status.update', $report))->data('confirm', 'Update the mmeber status report?') !!}
                            {!! BootForm::bind($report) !!}
                            {!! BootForm::text('Period', 'period')->addClass('monthpicker')->id('time_period')->value($period) !!}
                            {!! BootForm::select('Community','community_id')->options($communitiesDropdown)->addClass('select2')->id('community_id') !!}
                            {!! BootForm::text('City', 'city')->readOnly()->value(Auth::user()->facilitator->city->name) !!}
                            {!! BootForm::text('Facilitator', 'facilitator')->readOnly()->value(Auth::user()->name) !!}
                            <button type="button" id="refresh_button"
                                    class="pull-right btn btn-default"
                                    data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading...">
                                Refresh
                            </button>
                            {!! BootForm::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-pull-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Member Status</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box-body">
                        <p>Note: please check the box for active member</p>
                        <table id="members_table"
                               class="table table-bordered table-hover table-striped dt-responsive nowrap"
                               cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Customer No.</th>
                                <th>Name</th>
                                <th>Sentra</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $active_members = 0; ?>
                            @foreach($members as $member)
                                <tr>
                                    <td>{{ $member->account_number }}</td>
                                    <td>{{ $member->name }}</td>
                                    <td>{{ $member->sentra->name }}</td>
                                    <td class="text-center">
                                        @if($member->is_active)
                                            <?php $active_members++; ?>
                                            <input form="report_parameter_form" type="checkbox"
                                                   name="active[{{ $member->id }}]" value="1" checked>
                                        @else
                                            <input form="report_parameter_form" type="checkbox"
                                                   name="active[{{ $member->id }}]" value="1">
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <p class="text-right">
                            Total Active Member: <span id="active_members_count">{{ $active_members }}</span>
                        </p>
                        <p>
                            F4 Document:
                            <a href="{{ asset(config('btpn.document_upload_directory')). '/'.$report->community_id.'/'.$report->report->f4_document_url }}"
                               target="_blank">
                                {{ $report->report->f4_document_url }}
                            </a>
                        </p>
                        <div class="form-group {!! $errors->has('f4_document_url') ? 'has-error' : '' !!}">
                            <label class="control-label" for="f4_document_url">
                                F4 Document
                            </label>
                            <input type="file" name="f4_document_url" id="f4_document_url" form="report_parameter_form">
                            <p class="help-block">(.jpg, .pdf)</p>
                        </div>
                        <button id="submit_button" form="report_parameter_form" type="submit"
                                class="btn btn-primary pull-right">
                            <i class="fa fa-send"></i> Submit
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            // cache the dom
            var $container = $('#report_parameter')
            var $button = $('#refresh_button')
            var $time_period = $('#time_period')
            var $community_id = $('#community_id')
            var $overlay = $('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>')
            var $form = $('#report_parameter_form')
            var $submit_button = $('#submit_button')
            var $active_members_count = $('#active_members_count')
            var $form_community_id = $('#form_community_id')

            var $members_table = $('#members_table')
            var members_table
            var requestUrl = '{!! route('api.member-report.meta') !!}'
            var datatableUrl = '{!! route('api.member-report.data') !!}?'

            function resetFormErrors() {
                $('.form-group').removeClass('has-error')
                $('.form-group').find('.help-block').remove()
            }

            function refreshTable(period, community_id) {
                var param = {
                    period: period,
                    community_id: community_id
                }
                var queryString = $.param(param)


                if (!members_table) {
                    members_table = $members_table.DataTable({
                        ordering: false,
                        info: false,
                        ajax: datatableUrl + queryString,
                        columns: [
                            {data: 'account_number', name: 'account_number'},
                            {data: 'name', name: 'name'},
                            {data: 'sentra', name: 'sentra'},
                            {data: 'status', name: 'status', className: "text-center"},
                        ]
                    })
                } else {
                    members_table.ajax.url(datatableUrl + queryString).load()
                }
            }

            $button.click(function (e) {
                e.preventDefault()
                resetFormErrors()

                $container.append($overlay)
                $button.button('loading')

                var period = $time_period.val()
                var community_id = $community_id.val()

                $.post(requestUrl, {
                    period: period,
                    community_id: community_id
                }).always(function (response, status) {
                    if (response.status == 422) {
                        var errors = $.parseJSON(response.responseText)
                        $.each(errors, function (field, message) {
                            var formGroup = $('[name= ' + field + ']', $form).closest('.form-group')
                            formGroup.addClass('has-error').find('.form-control').parent().append('<p class="help-block">' + message + '</p>')
                        })
                    } else {
                        refreshTable(period, community_id)
                        $active_members_count.html(response.data.active_members_count)
                        $form_community_id.val(community_id)
                        $submit_button.removeClass('hidden')
                    }

                    $button.button('reset')
                    $container.find($overlay).remove()
                })
            })
        });
    </script>
@append