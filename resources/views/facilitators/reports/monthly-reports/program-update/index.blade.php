@extends('layouts.default')

@section('page-title', 'Program Update Report')

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-push-8">
            <div id="report_parameter" class="box">
                <div class="box-header">
                    <h3 class="box-title">Report Parameter</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <?php $columnSizes = ['md' => [5, 7], 'sm' => [4, 8]]; ?>
                    <div class="row">
                        <div class="col-xs-12">
                            {!! BootForm::openHorizontal($columnSizes)->id('report_parameter_form') !!}
                            <input type="hidden" name="report_type" value="program_update">
                            {!! BootForm::text('Period', 'period')->addClass('monthpicker')->id('time_period') !!}
                            {!! BootForm::select('Community','community_id')->options($communitiesDropdown)->addClass('select2')->id('community_id') !!}
                            {!! BootForm::text('City', 'city')->readOnly()->value(Auth::user()->facilitator->city->name) !!}
                            {!! BootForm::text('Facilitator', 'facilitator')->readOnly()->value(Auth::user()->name) !!}
                            {!! BootForm::text('Community Leader', 'community_leader')->readOnly()->id('community_leader') !!}
                            {!! BootForm::text('Business Field', 'business_field')->readOnly()->id('business_field') !!}
                            {!! BootForm::text('No. of Members', 'member_count')->readOnly()->id('member_count') !!}
                            <button type="button" id="refresh_button"
                                    class="pull-right btn btn-default"
                                    data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading...">
                                Refresh
                            </button>
                            {!! BootForm::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-pull-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Activity Summary</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box-body">
                        @foreach($activityTypes as $activityType)
                            <p>
                                <strong>{{ $activityType->name }}</strong>
                            </p>
                            <table id="activity_table_{{ $activityType->id }}"
                                   class="table table-bordered table-hover table-striped dt-responsive nowrap"
                                   cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>Activity Name</th>
                                    <th>Location</th>
                                    <th>Date</th>
                                    <th>Duration (min)</th>
                                    <th>Participants</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <hr>
                        @endforeach
                        <p>
                            Total Activities: <span id="total_activities"></span>
                        </p>

                        <p>
                            Total Participants: <span id="total_participants"></span>
                        </p>
                        {!! BootForm::open()->multipart()->post()->action(route('facilitators.reports.monthly-report.program-update.store'))->data('confirm', 'Submit the program update report?')->id('program_update_form') !!}
                        <fieldset class="hidden">
                            {!! BootForm::file('F1 Document', 'f1_document_url')->helpBlock('(.jpg, .pdf)') !!}
                            <input type="hidden" name="period" id="report_period">
                            <input type="hidden" name="community_id" id="report_community_id">
                            <button id="submit_button" type="submit"
                                    class="btn btn-primary pull-right">
                                <i class="fa fa-send"></i> Submit
                            </button>
                        </fieldset>
                        {!! BootForm::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            // cache the dom
            var $container = $('#report_parameter')
            var $form = $('#report_parameter_form')
            var $time_period = $('#time_period')
            var $community_id = $('#community_id')
            var $community_leader = $('#community_leader')
            var $business_field = $('#business_field')
            var $member_count = $('#member_count')
            var $total_activities = $('#total_activities')
            var $total_participants = $('#total_participants')
            var $button = $('#refresh_button')
            var $meeting_table = $('#activity_table_1')
            var $visit_table = $('#activity_table_2')
            var $training_table = $('#activity_table_3')
            var $exhibition_table = $('#activity_table_4')
            var $submit_button = $('#submit_button')
            var $period_field = $('#report_period')
            var $community_field = $('#report_community_id')
            var $report_form = $('#program_update_form')

            var meeting_table
            var visit_table
            var training_table
            var exhibition_table

            var period, community_id

            var requestUrl = '{!! route('api.report.meta') !!}'
            var datatableUrl = '{!! route('api.report.data') !!}?'
            var overlay = $('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>')

            $button.click(function (e) {
                e.preventDefault()
                resetModalFormErrors()

                $container.append(overlay)
                $button.button('loading')

                period = $time_period.val()
                community_id = $community_id.val()

                $.post(requestUrl, {
                    community_id: community_id,
                    period: period
                }).always(function (response, status) {
                    switch (response.status) {
                        case 422:
                            var errors = $.parseJSON(response.responseText)
                            $.each(errors, function (field, message) {
                                var formGroup = $('[name= ' + field + ']', $form).closest('.form-group')
                                formGroup.addClass('has-error').find('.form-control').parent().append('<p class="help-block">' + message + '</p>')
                            })
                            break
                        default:
                            // if no status code is present, that means we got the data
                            refreshTable(period, community_id)
                            populateField(response.data)
                            $report_form.find('fieldset').removeClass('hidden')
                            break
                    }

                    $button.button('reset')
                    $container.find(overlay).remove()
                })
            })

            function resetModalFormErrors() {
                $('.form-group').removeClass('has-error')
                $('.form-group').find('.help-block').remove()
            }

            function populateField(data) {
                $business_field.val(data.business_field.name)
                $member_count.val(data.member_count)
                $community_leader.val(data.community_leader.name)
                $total_activities.html(data.total_activities)
                $total_participants.html(data.total_participants)
                $period_field.val(period)
                $community_field.val(community_id)
            }

            function refreshTable(period, community_id) {
                var param = {
                    period: period,
                    community_id: community_id
                }

                refreshMeetingTable(param)
                refreshVisitTable(param)
                refreshTrainingTable(param)
                refreshExhibitionTable(param)
            }

            function refreshMeetingTable(param) {
                param.activity_type_id = activity_types[0].id
                var queryString = $.param(param)

                if (!meeting_table) {
                    meeting_table = $meeting_table.DataTable({
                        ordering: false,
                        info: false,
                        ajax: datatableUrl + queryString,
                        columns: [
                            {data: 'report.activity_name', name: 'report.activity_name'},
                            {data: 'report.location', name: 'report.location'},
                            {data: 'period', name: 'period'},
                            {data: 'duration', name: 'duration', className: "text-right"},
                            {data: 'participant_count', name: 'participant_count', className: "text-right"}
                        ]
                    })
                } else {
                    meeting_table.ajax.url(datatableUrl + queryString).load()
                }
            }

            function refreshVisitTable(param) {
                param.activity_type_id = activity_types[1].id
                var queryString = $.param(param)

                if (!visit_table) {
                    visit_table = $visit_table.DataTable({
                        ordering: false,
                        info: false,
                        ajax: datatableUrl + queryString,
                        columns: [
                            {data: 'report.activity_name', name: 'report.activity_name'},
                            {data: 'report.location', name: 'report.location'},
                            {data: 'period', name: 'period'},
                            {data: 'duration', name: 'duration', className: "text-right"},
                            {data: 'participant_count', name: 'participant_count', className: "text-right"}
                        ]
                    })
                } else {
                    visit_table.ajax.url(datatableUrl + queryString).load()
                }
            }

            function refreshTrainingTable(param) {
                param.activity_type_id = activity_types[2].id
                var queryString = $.param(param)

                if (!training_table) {
                    training_table = $training_table.DataTable({
                        ordering: false,
                        info: false,
                        ajax: datatableUrl + queryString,
                        columns: [
                            {data: 'report.activity_name', name: 'report.activity_name'},
                            {data: 'report.location', name: 'report.location'},
                            {data: 'period', name: 'period'},
                            {data: 'duration', name: 'duration', className: "text-right"},
                            {data: 'participant_count', name: 'participant_count', className: "text-right"}
                        ]
                    })
                } else {
                    training_table.ajax.url(datatableUrl + queryString).load()
                }
            }

            function refreshExhibitionTable(param) {
                param.activity_type_id = activity_types[3].id
                var queryString = $.param(param)

                if (!exhibition_table) {
                    exhibition_table = $exhibition_table.DataTable({
                        ordering: false,
                        info: false,
                        ajax: datatableUrl + queryString,
                        columns: [
                            {data: 'report.activity_name', name: 'report.activity_name'},
                            {data: 'report.location', name: 'report.location'},
                            {data: 'period', name: 'period'},
                            {data: 'duration', name: 'duration', className: "text-right"},
                            {data: 'participant_count', name: 'participant_count', className: "text-right"}
                        ]
                    })
                } else {
                    exhibition_table.ajax.url(datatableUrl + queryString).load()
                }
            }
        })
    </script>
@append