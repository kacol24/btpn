@extends('layouts.default')

@section('page-title', 'Check Documents Status')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Declined Documents</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped dt-responsive dt-simple nowrap"
                           cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Document ID</th>
                            <th>Type</th>
                            <th>Community</th>
                            <th>Submit Date</th>
                            <th>Declined Date</th>
                            <th>Reason</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($declinedReports as $report)
                            <tr>
                                <td>{{ $report->report_id }}</td>
                                <td>{{ $report->report->report_type }}</td>
                                <td>{{ $report->community->name }}</td>
                                <td>{{ $report->submitted_at }}</td>
                                <td>{{ $report->declined_at->toDateString() }}</td>
                                <td>{{ $report->declined_reason }}</td>
                                <td><a href="{{ route('facilitators.reports.edit', $report->id) }}"
                                       class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Revise</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Pending Documents</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped dt-responsive dt-simple nowrap"
                           cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Document ID</th>
                            <th>Type</th>
                            <th>Community</th>
                            <th>Submit Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pendingReports as $report)
                            <tr>
                                <td>{{ $report->report_id }}</td>
                                <td>{{ $report->report->report_type }}</td>
                                <td>{{ $report->community->name }}</td>
                                <td>{{ $report->submitted_at }}</td>
                                <td>
                                    @if(!$report->is_verified && !$report->verified_at)
                                        {!! BootForm::open()->delete()->action(route('facilitators.reports.destroy', $report))->data('confirm', 'Void this report?') !!}
                                        <button type="submit" class="btn btn-xs btn-danger">
                                            <i class="fa fa-ban"></i> Void
                                        </button>
                                        {!! BootForm::close() !!}
                                    @endif
                                </td>
                                {{--if it was declined, then verified date should not be empty and no void button should be shown--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Verified Documents</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped dt-responsive dt-simple nowrap"
                           cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Document ID</th>
                            <th>Type</th>
                            <th>Community</th>
                            <th>Submit Date</th>
                            <th>Verified Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($verifiedReports as $report)
                            <tr>
                                <td>{{ $report->report_id }}</td>
                                <td>{{ $report->report->report_type }}</td>
                                <td>{{ $report->community->name }}</td>
                                <td>{{ $report->submitted_at }}</td>
                                <td>{{ $report->verified_at->toDateString() }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection