@extends('layouts.default')

@section('page-title', 'Daily Reports')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <?php $columnSizes = ['sm' => [4, 8]]; ?>
                    <div class="row">
                        <div class="col-sm-7">
                            {!! BootForm::openHorizontal($columnSizes)->action(route('facilitators.reports.daily-report.store'))->post()->multipart()->data('confirm', 'Submit the daily report?') !!}
                            {!! BootForm::hidden('facilitator_id')->value(Auth::user()->facilitator->id) !!}
                            <fieldset>
                                <legend>General Information</legend>
                                {!! BootForm::select('Community','community_id')->addClass('select2')->id('community_dropdown')->options($communities)->select(Input::get('community_id', '')) !!}
                                {!! BootForm::text('Date', 'period')->addClass('datepicker') !!}
                                <div class="form-group {!! $errors->has('start_time') || $errors->has('end_time') ? 'has-error' : '' !!}">
                                    <label for="start_time" class="col-sm-4 control-label">Time</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-xs-5 no-padding-right">
                                                {!! AdamForm::text('start_time')->addClass('form-control timepicker')->id('start_time') !!}
                                                {!! $errors->first('start_time', '<p class="help-block">:message</p>') !!}
                                            </div>
                                            <div class="col-xs-2 text-center no-padding">
                                                <p class="form-control-static">to</p>
                                            </div>
                                            <div class="col-xs-5 no-padding-left">
                                                {!! AdamForm::text('end_time')->addClass('form-control timepicker')->id('end_time') !!}
                                                {!! $errors->first('end_time', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {!! BootForm::text('Location', 'location') !!}
                                {!! BootForm::select('Activity Type', 'activity_type_id')->options($activity_types)->addClass('select2') !!}
                                {!! BootForm::text('Activity Name', 'activity_name') !!}
                            </fieldset>
                            <fieldset>
                                <legend>Participants</legend>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <table id="members_table"
                                               class="table dt-responsive table-bordered table-striped nowrap"
                                               cellspacing="0"
                                               width="100%">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Status</th>
                                                <th>Attendance</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Reporting</legend>
                                {!! BootForm::textarea('Achievements', 'achievements')->rows(4) !!}
                                {!! BootForm::textarea('Problems', 'problems')->rows(4) !!}
                                {!! BootForm::textarea('Future Plan', 'plans')->rows(4) !!}
                            </fieldset>
                            <fieldset>
                                <legend>Documents</legend>
                                {!! BootForm::file('F2 Document', 'f2_document_url')->helpBlock('(.jpg, .pdf)') !!}
                                {!! BootForm::file('F3 Document', 'f3_document_url')->helpBlock('(.jpg, .pdf)') !!}
                                {!! BootForm::file('Supporting Document', 'supporting_document_url')->helpBlock('(.jpg, .pdf, .zip)') !!}
                            </fieldset>
                            <button type="submit" class="btn btn-primary pull-right" disabled>
                                <i class="fa fa-save"></i> Save
                            </button>
                            {!! BootForm::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            var $communityDropdown = $('#community_dropdown');
            var $table = $('#members_table');
            var datatable;
            var community_id;
            var url = '{!! route('api.facilitators.members') !!}';

            $communityDropdown.on('change', function () {
                community_id = ($(this).is("select") ? $(":selected", this) : $(this)).val();

                if (!datatable) {
                    datatable = $table.DataTable({
                        ordering: false,
                        paging: false,
                        info: false,
                        ajax: url + '/?community_id=' + community_id,
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'name', name: 'name'},
                            {data: 'phone', name: 'phone'},
                            {data: 'status', name: 'status', className: "text-center"},
                            {data: 'attendance', name: 'attendance', className: "text-center"}
                        ]
                    });
                } else {
                    datatable.ajax.url(url + '/?community_id=' + community_id).load();
                }
            });
        });
    </script>
@append