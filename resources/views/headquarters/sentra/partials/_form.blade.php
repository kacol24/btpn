{!! BootForm::text('Name', 'name') !!}
{!! BootForm::select('MMS', 'mms_id')->addClass('select2')->options($mmsDropdown) !!}
{!! BootForm::text('Phone', 'phone') !!}
{!! BootForm::email('Email', 'email') !!}
{!! BootForm::textarea('Address', 'address')->rows(3) !!}
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save</button>