{!! BootForm::open()->put()->action(route('sentra.update', $sentra->id))->addClass('bootstrap-modal-form')->data('confirm', 'Edit the sentra?') !!}
{!! BootForm::bind($sentra) !!}
@include('headquarters.sentra.partials._form')
{!! BootForm::close() !!}