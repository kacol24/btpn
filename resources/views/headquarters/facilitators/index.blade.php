@extends('layouts.default')

@section('page-title', 'Facilitators')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <button class="btn btn-primary bootstrap-modal-form-open" data-toggle="modal"
                            data-target="#create_modal"><i
                                class="fa fa-plus-circle"></i> New
                    </button>
                </div>
                <div class="box-body">
                    <table id="facilitators_table" class="table table-bordered dt-responsive nowrap"
                           cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Date of Birth</th>
                            <th>Gender</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            document.datatable = $('#facilitators_table').DataTable({
                ajax: '{!! route('facilitators.datatables') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'user.name', name: 'name'},
                    {data: 'dob', name: 'dob'},
                    {
                        data: 'gender',
                        name: 'gender',
                        render: function (data, type, row) {
                            if (data === 'm') {
                                return 'Male'
                            }
                            return 'Female';
                        }
                    },
                    {data: 'user.phone', name: 'phone'},
                    {data: 'user.email', name: 'email'},
                    {data: 'address', name: 'address'},
                    {data: 'actions', name: 'actions', orderable: false, searchable: false}
                ]
            });
            BTPN.remoteChained('.community_dropdown', '.city_dropdown', '{!! route('api.city.communities') !!}');
        });
    </script>
    @include('headquarters.facilitators.create')
@endsection