{!! BootForm::open()->multipart()->put()->action(route('facilitators.update', $facilitator))->addClass('bootstrap-modal-form')->data('confirm', 'Edit the facilitator?') !!}
{!! BootForm::bind($facilitator) !!}
@include('headquarters.facilitators.partials._form')
{!! BootForm::close() !!}