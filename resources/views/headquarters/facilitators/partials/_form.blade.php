<div class="row">
    <div class="col-xs-12">
        <fieldset>
            <legend>Personal Information</legend>
            <div class="row">
                <div class="col-sm-6 col-sm-push-6">
                    @if(isset($facilitator->picture))
                        <img src="{{ asset($facilitator->picture) }}"
                             alt="user picture" class="img-responsive full-width">
                    @endif
                    {!! BootForm::file('Upload Image', 'picture') !!}
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                    {!! BootForm::text('Name', 'name')->autofocus() !!}
                    {!! BootForm::text('Date of Birth', 'dob')->addClass('dobpicker') !!}
                    <div class="form-group {!! $errors->has('gender') ? 'has-error' : '' !!}">
                        <label class="control-label">Gender</label>

                        <div>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default {{ isset($facilitator->gender) && $facilitator->gender == 'm' ? 'active' : '' }}">
                                    {!! AdamForm::radio('gender', 'm') !!} Male
                                </label>
                                <label class="btn btn-default {{ isset($facilitator->gender) && $facilitator->gender == 'f' ? 'active' : '' }}">
                                    {!! AdamForm::radio('gender', 'f') !!} Female
                                </label>
                            </div>
                        </div>
                        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                    </div>
                    {!! BootForm::text('Phone', 'phone') !!}
                    {!! BootForm::email('Email', 'email') !!}
                    {!! BootForm::textarea('Address', 'address')->rows(3) !!}
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <fieldset>
            <legend>Account Details</legend>
            {!! BootForm::hidden('user_id') !!}
            {!! BootForm::text('Username', 'username') !!}
            {!! BootForm::password('Password', 'password') !!}
            {!! BootForm::password('Confirm Password', 'password_confirmation') !!}
        </fieldset>
    </div>
    <div class="col-sm-6">
        <fieldset>
            <legend>Responsibility</legend>
            {!! BootForm::select('City', 'city_id')->options($cities)->addClass('select2 city_dropdown') !!}
            {!! BootForm::select('Community', 'community_id')->options($communities)->addClass('select2 community_dropdown')->multiple() !!}
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save</button>
    </div>
</div>