{!! BootForm::text('Name', 'name')->autofocus() !!}
{!! BootForm::select('City', 'city_id')->options($cities)->addClass('select2 city_dropdown') !!}
{!! BootForm::select('MMS', 'mms_id')->options($mmsDropdown)->addClass('select2 mms_dropdown') !!}
{!! BootForm::select('Business Field', 'business_field_id')->options($businessFields)->addClass('select2') !!}
{!! BootForm::text('Activation Date', 'activation_date')->addClass('monthpicker') !!}
<div class="row">
    <div class="col-sm-12">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save</button>
    </div>
</div>

@section('footer-scripts')
    <script>
        $(function () {
            BTPN.remoteChained('.mms_dropdown', '.city_dropdown', '{!! route('api.city.mms') !!}');
        });
    </script>
@append