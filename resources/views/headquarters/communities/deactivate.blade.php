<div class="row">
    <div class="col-sm-12">
        {!! BootForm::open()->delete()->action(route('communities.deactivate', $community))->data('confirm', 'Deactivate the community?') !!}
        {!! BootForm::textarea('Reason for deactivation', 'deactivation_reason')->rows(3)->autofocus() !!}
        <button type="submit" class="btn btn-danger pull-right">Deactivate</button>
        {!! BootForm::close() !!}
    </div>
</div>