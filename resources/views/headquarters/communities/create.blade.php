<div class="modal fade" id="create_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create Community</h4>
            </div>
            <div class="modal-body">
                {!! BootForm::open()->action(route('communities.store'))->addClass('bootstrap-modal-form')->data('confirm', 'Create the community?') !!}
                @include('headquarters.communities.partials._form')
                {!! BootForm::close() !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>