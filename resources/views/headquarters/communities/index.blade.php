@extends('layouts.default')

@section('page-title', 'Communities')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <button class="btn btn-primary bootstrap-modal-form-open" data-toggle="modal"
                            data-target="#create_modal"><i
                                class="fa fa-plus-circle"></i> New
                    </button>
                </div>
                <div class="box-body">
                    <table id="communities_table" class="table table-bordered dt-responsive nowrap"
                           cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Community ID</th>
                            <th>Name</th>
                            <th>MMS</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            document.datatable = $('#communities_table').DataTable({
                ajax: '{!! route('communities.datatables') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'mms.user.name', name: 'mms.user.name'},
                    {data: 'actions', name: 'actions', searchable: false, orderable: false}
                ]
            });
        });
    </script>
    @include('headquarters.communities.create')
@append