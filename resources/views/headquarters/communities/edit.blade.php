{!! BootForm::open()->put()->action(route('communities.update', $community))->addClass('bootstrap-modal-form')->data('confirm', 'Edit the community?') !!}
{!! BootForm::bind($community) !!}
@include('headquarters.communities.partials._form')
{!! BootForm::close() !!}