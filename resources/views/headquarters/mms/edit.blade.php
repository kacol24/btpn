{!! BootForm::open()->put()->action(route('mms.update', $mms))->addClass('bootstrap-modal-form')->data('confirm', 'Edit the MMS?') !!}
{!! BootForm::bind($mms) !!}
{!! BootForm::hidden('mms_id')->value($mms->user->id) !!}
<div class="row">
    <div class="col-sm-6">
        <fieldset>
            <legend>Personal Information</legend>
            {!! BootForm::text('Name', 'name')->autofocus()->value($mms->user->name) !!}
            {!! BootForm::select('City', 'city_id')->options($cities)->addClass('select2') !!}
            {!! BootForm::text('Phone', 'phone')->value($mms->user->phone) !!}
            {!! BootForm::email('Email', 'email')->value($mms->user->email) !!}
            {!! BootForm::textarea('Address', 'address')->rows(3) !!}
        </fieldset>
    </div>
    <div class="col-sm-6">
        <fieldset>
            <legend>Account Details</legend>
            {!! BootForm::text('Username', 'username')->value($mms->user->username) !!}
            {!! BootForm::password('New Password', 'password') !!}
            {!! BootForm::password('Confirm New Password', 'password_confirmation') !!}
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save</button>
    </div>
</div>
{!! BootForm::close() !!}