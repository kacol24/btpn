@extends('layouts.default')

@section('page-title', 'MMS')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <button class="btn btn-primary bootstrap-modal-form-open" data-toggle="modal"
                            data-target="#create_modal"><i
                                class="fa fa-plus-circle"></i> New
                    </button>
                </div>
                <div class="box-body">
                    <table id="mms_table" class="table table-bordered table-striped dt-responsive nowrap"
                           cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>MMS ID</th>
                            <th>Name</th>
                            <th>City</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            document.datatable = $('#mms_table').DataTable({
                ajax: '{!! route('mms.datatables') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'user.name', name: 'name'},
                    {data: 'city.name', name: 'city'},
                    {data: 'actions', name: 'actions', orderable: false, searchable: false}
                ]
            });
        });
    </script>
    @include('headquarters.mms.create')
@endsection