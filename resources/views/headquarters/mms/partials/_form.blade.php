<div class="row">
    <div class="col-sm-6">
        <fieldset>
            <legend>Personal Information</legend>
            {!! BootForm::text('Name', 'name')->autofocus() !!}
            {!! BootForm::select('City', 'city_id')->options($cities)->addClass('select2') !!}
            {!! BootForm::text('Phone', 'phone') !!}
            {!! BootForm::email('Email', 'email') !!}
            {!! BootForm::textarea('Address', 'address')->rows(3) !!}
        </fieldset>
    </div>
    <div class="col-sm-6">
        <fieldset>
            <legend>Account Details</legend>
            {!! BootForm::text('Username', 'username') !!}
            {!! BootForm::password('Password', 'password') !!}
            {!! BootForm::password('Confirm Password', 'password_confirmation') !!}
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save</button>
    </div>
</div>