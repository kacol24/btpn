{!! BootForm::open()->put()->action(route('cities.update', $city))->addClass('bootstrap-modal-form')->data('confirm', 'Edit the city?') !!}
{!! BootForm::bind($city) !!}
@include('headquarters.cities.partials._form')
{!! BootForm::close() !!}