@extends('layouts.default')

@section('page-title', 'Cities')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <button class="btn btn-primary bootstrap-modal-form-open" data-toggle="modal"
                            data-target="#create_modal"><i
                                class="fa fa-plus-circle"></i> New
                    </button>
                </div>
                <div class="box-body">
                    <table id="cities_table"
                           class="table table-bordered table-striped dt-responsive" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>City ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            document.datatable = $('#cities_table').DataTable({
                ajax: '{!! route('cities.datatables') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'actions', name: 'actions', orderable: false, searchable: false}
                ]
            });
        });
    </script>
    @include('headquarters.cities.create')
@endsection