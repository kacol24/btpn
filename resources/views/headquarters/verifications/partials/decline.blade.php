{!! BootForm::open()->post()->action(route('verifications.daily-report.verify', $report))->data('confirm', 'Are you sure?') !!}
<div class="form-group">
    <div class="col-sm-12">
        <button id="decline_button" class="btn btn-danger"><i class="fa fa-times"></i> Decline</button>
        <button class="btn btn-success pull-right" type="submit"><i class="fa fa-check"></i> Verify</button>
    </div>
</div>
{!! BootForm::close() !!}

@section('footer-scripts')
    <script>
        $(function () {
            // cache the dom
            var $button = $('#decline_button')
            var declineUrl = '{!! route('verifications.daily-report.decline', $report) !!}'

            $button.on('click', function (e) {
                e.preventDefault()
                swal({
                    title: "Provide a reason",
                    text: "Reason of decline:",
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonColor: "#d73925",
                    confirmButtonText: "Finalize Decline",
                    animation: "slide-from-top",
                    inputPlaceholder: "Reason",
                    showLoaderOnConfirm: true
                }, function (inputValue) {
                    // submit
                    $.post(declineUrl, {
                        reason: inputValue
                    }).always(function (response, status) {
                        window.location.href = response.data.url
                    })
                })
            })
        })
    </script>
@append