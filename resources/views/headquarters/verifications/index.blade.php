@extends('layouts.default')

@section('page-title', 'Verifications')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Pending Documents</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped dt-responsive dt-simple nowrap" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Document ID</th>
                            <th>Type</th>
                            <th>Community</th>
                            <th>Submitter</th>
                            <th>Submit Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pendingReports as $report)
                            <tr>
                                <td>{{ $report->report_id }}</td>
                                <td>{{ $report->report->report_type }}</td>
                                <td>{{ $report->community->name }}</td>
                                <td>{{ $report->facilitator->name }}</td>
                                <td>{{ $report->submitted_at }}</td>
                                <td>
                                    <a href="{{ route('verifications.daily-report.show', $report) }}"
                                       class="btn btn-xs btn-primary"><i class="fa fa-search"></i>
                                        Verify</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Verification Log</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped dt-responsive dt-simple nowrap" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Document ID</th>
                            <th>Type</th>
                            <th>Community</th>
                            <th>Submitter</th>
                            <th>Submit Date</th>
                            <th>Status</th>
                            <th>Notes</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($verifiedReports as $report)
                            <tr>
                                <td>{{ $report->report_id }}</td>
                                <td>{{ $report->report->report_type }}</td>
                                <td>{{ $report->community->name }}</td>
                                <td>{{ $report->facilitator->name }}</td>
                                <td>{{ $report->submitted_at }}</td>
                                <td class="text-center">
                                    @if($report->is_verified && $report->declined_at == null)
                                        <span class="label label-success">Verified</span>
                                    @else
                                        <span class="label label-danger">Declined</span>
                                    @endif
                                </td>
                                <td>{{ $report->declined_reason }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection