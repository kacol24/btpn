@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body dirtyignore">
                    <?php $columnSizes = ['sm' => [4, 8]]; ?>
                    <div class="row">
                        <div class="col-sm-7">
                            {!! BootForm::openHorizontal($columnSizes) !!}
                            <fieldset>
                                <legend>General Information</legend>
                                {!! BootForm::text('Community', 'community_id')->value($report->community->name)->readOnly() !!}
                                {!! BootForm::text('Date', 'period')->addClass('datepicker')->value($report->period)->readOnly() !!}
                                <div class="form-group {!! $errors->has('start_time') || $errors->has('end_time') ? 'has-error' : '' !!}">
                                    <label for="start_time" class="col-sm-4 control-label">Time</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-xs-5 no-padding-right">
                                                {!! AdamForm::text('start_time')->addClass('form-control')->value(Carbon::parse($report->report->start_time)->format('H:i'))->readOnly() !!}
                                                {!! $errors->first('start_time', '<p class="help-block">:message</p>') !!}
                                            </div>
                                            <div class="col-xs-2 text-center no-padding">
                                                <p class="form-control-static">to</p>
                                            </div>
                                            <div class="col-xs-5 no-padding-left">
                                                {!! AdamForm::text('end_time')->addClass('form-control')->value(Carbon::parse($report->report->end_time)->format('H:i'))->readOnly() !!}
                                                {!! $errors->first('end_time', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {!! BootForm::text('Location', 'location')->value($report->report->location)->readOnly() !!}
                                {!! BootForm::text('Activity Type', 'activity_type_id')->value($report->report->activity_type->name)->readOnly() !!}
                                {!! BootForm::text('Activity Name', 'activity_name')->value($report->report->activity_name)->readOnly() !!}
                            </fieldset>
                            <fieldset>
                                <legend>Participants</legend>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <table id="members_table"
                                               class="table dt-responsive table-bordered nowrap"
                                               cellspacing="0"
                                               width="100%">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Status</th>
                                                <th>Attendance</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($report->report->members as $member)
                                                <tr>
                                                    <td>{{ $member->id }}</td>
                                                    <td>{{ $member->name }}</td>
                                                    <td>{{ $member->phone }}</td>
                                                    <td class="text-center">
                                                        @if($member->is_active)
                                                            <span class="label label-success">Active</span>
                                                        @else
                                                            <span class="label label-danger">Inactive</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox"
                                                               class="icheck"
                                                               {{ $member->pivot->attended ? 'checked' : '' }} disabled>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Reporting</legend>
                                {!! BootForm::textarea('Achievements', 'achievements')->rows(4)->value($report->report->achievements)->readOnly() !!}
                                {!! BootForm::textarea('Problems', 'problems')->rows(4)->value($report->report->problems)->readOnly() !!}
                                {!! BootForm::textarea('Future Plan', 'plans')->rows(4)->value($report->report->plans)->readOnly() !!}
                            </fieldset>
                            <fieldset>
                                <legend>Documents</legend>
                                <p>
                                    F2 Document:
                                    <a href="{{ asset(config('btpn.document_upload_directory')). '/'.$report->community_id.'/'.$report->report->f2_document_url }}"
                                       target="_blank">
                                        {{ $report->report->f2_document_url }}
                                    </a>
                                </p>
                                <p>
                                    F3 Document:
                                    @if($report->report->f3_document_url != '')
                                        <a href="{{ asset(config('btpn.document_upload_directory')). '/'.$report->community_id.'/'.$report->report->f3_document_url }}"
                                           target="_blank">
                                            {{ $report->report->f3_document_url }}
                                        </a>
                                    @else
                                        -
                                    @endif
                                </p>
                                <p>
                                    F3 Document:
                                    @if($report->report->supporting_document_url != '')
                                        <a href="{{ asset(config('btpn.document_upload_directory')). '/'.$report->community_id.'/'.$report->report->supporting_document_url }}"
                                           target="_blank">
                                            {{ $report->report->supporting_document_url }}
                                        </a>
                                    @else
                                        -
                                    @endif
                                </p>
                            </fieldset>
                            {!! BootForm::close() !!}
                            @include('headquarters.verifications.partials.decline')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection