@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-push-8">
            <div id="report_parameter" class="box">
                <div class="box-header">
                    <h3 class="box-title">Report Parameter</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <?php $columnSizes = ['md' => [5, 7], 'sm' => [4, 8]]; ?>
                    <div class="row">
                        <div class="col-xs-12">
                            {!! BootForm::openHorizontal($columnSizes) !!}
                            {!! BootForm::text('Period', 'period')->readOnly()->value(Carbon::parse($report->report->period)->format('Y-m')) !!}
                            {!! BootForm::text('Community','community_id')->readOnly()->value($report->community->name) !!}
                            {!! BootForm::text('City', 'city')->readOnly()->value($report->facilitator->city->name) !!}
                            {!! BootForm::text('Facilitator', 'facilitator')->readOnly()->value($report->facilitator->name) !!}
                            {!! BootForm::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-pull-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Member Status</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box-body">
                        <p>Note: please check the box for active member</p>
                        <table id="members_table"
                               class="table table-bordered table-hover table-striped dt-responsive dt-simple nowrap"
                               cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Customer No.</th>
                                <th>Name</th>
                                <th>Sentra</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($members as $member)
                                <tr>
                                    <td>{{ $member->account_number }}</td>
                                    <td>{{ $member->name }}</td>
                                    <td>{{ $member->sentra->name }}</td>
                                    <td class="text-center">
                                        @if($member->is_active)
                                            <span class="label label-success">Active</span>
                                        @else
                                            <span class="label label-danger">Inactive</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <p class="text-right">
                            Total Active Member: {{ $activeMembers }}
                        </p>
                        <p>
                            F4 Document:
                            <a href="{{ asset(config('btpn.document_upload_directory')). '/'.$report->community_id.'/'.$report->report->f4_document_url }}"
                               target="_blank">
                                {{ $report->report->f4_document_url }}
                            </a>
                        </p>
                        <div class="row">
                            @include('headquarters.verifications.partials.decline')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection