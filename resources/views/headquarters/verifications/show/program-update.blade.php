@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-push-8">
            <div id="report_parameter" class="box">
                <div class="box-header">
                    <h3 class="box-title">Report Parameter</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <?php $columnSizes = ['md' => [5, 7], 'sm' => [4, 8]]; ?>
                    <div class="row">
                        <div class="col-xs-12">
                            {!! BootForm::openHorizontal($columnSizes) !!}
                            {!! BootForm::text('Period', 'period')->readOnly()->value(Carbon::parse($report->period)->format('Y-m')) !!}
                            {!! BootForm::text('Community','community_id')->readOnly()->value($report->community->name) !!}
                            {!! BootForm::text('City', 'city')->readOnly()->value($report->facilitator->city->name) !!}
                            {!! BootForm::text('Facilitator', 'facilitator')->readOnly()->value($report->facilitator->name) !!}
                            {!! BootForm::text('Community Leader', 'community_leader')->readOnly()->value($report->community->leader()->name) !!}
                            {!! BootForm::text('Business Field', 'business_field')->readOnly()->value($report->community->businessField->name) !!}
                            {!! BootForm::text('No. of Members', 'member_count')->readOnly()->value($report->community->members->count()) !!}
                            {!! BootForm::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-pull-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Activity Summary</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box-body">
                        @foreach($activityTypes as $activityType)
                            <p>
                                <strong>{{ $activityType->name }}</strong>
                            </p>
                            <table id="activity_table_{{ $activityType->id }}"
                                   class="table table-bordered table-hover table-striped dt-responsive nowrap"
                                   cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>Activity Name</th>
                                    <th>Location</th>
                                    <th>Date</th>
                                    <th>Duration (min)</th>
                                    <th>Participants</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $totalActivities = 0;
                                $totalParticipants = 0;
                                ?>
                                @foreach($dailyReports as $dailyReport)
                                    <?php
                                    $totalActivities++;
                                    $totalParticipants += $dailyReport->report->participants;
                                    ?>
                                    @if($dailyReport->report->activity_type_id == $activityType->id)
                                        <tr>
                                            <td>{{ $dailyReport->report->activity_name }}</td>
                                            <td>{{ $dailyReport->report->location }}</td>
                                            <td>{{ $dailyReport->period }}</td>
                                            <td>{{ $dailyReport->report->duration }}</td>
                                            <td>{{ $dailyReport->report->participants }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                            <hr>
                        @endforeach
                        <p>
                            Total Activities: {{ $totalActivities }}
                        </p>

                        <p>
                            Total Participants: {{ $totalParticipants }}
                        </p>
                        <p>
                            F1 Document:
                            <a href="{{ asset(config('btpn.document_upload_directory')). '/'.$report->community_id.'/'.$report->report->f1_document_url }}"
                               target="_blank">
                                {{ $report->report->f1_document_url }}
                            </a>
                        </p>
                        <div class="row">
                            @include('headquarters.verifications.partials.decline')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection