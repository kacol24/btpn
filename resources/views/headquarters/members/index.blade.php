@extends('layouts.default')

@section('page-title', 'Members')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <button class="btn btn-primary bootstrap-modal-form-open" data-toggle="modal"
                            data-target="#create_modal"><i class="fa fa-plus-circle"></i> New
                    </button>
                </div>
                <div class="box-body">
                    <table id="members_table" class="table table-bordered table-striped dt-responsive nowrap"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Customer No.</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            document.datatable = $('#members_table').DataTable({
                ajax: '{!! route('members.datatables') !!}',
                columns: [
                    {data: 'account_number', name: 'account_number'},
                    {data: 'name', name: 'name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'address', name: 'address'},
                    {data: 'actions', name: 'actions', orderable: false, searchable: false}
                ]
            });
            BTPN.remoteChained('.community_dropdown', '.mms_dropdown', '{!! route('api.mms.communities') !!}');
            BTPN.remoteChained('.sentra_dropdown', '.mms_dropdown', '{!! route('api.mms.sentra') !!}');
            BTPN.remoteChained('.mms_dropdown', '.city_dropdown', '{!! route('api.city.mms') !!}');
        });
    </script>
    @include('headquarters.members.create')
@endsection