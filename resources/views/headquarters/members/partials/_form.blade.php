{!! BootForm::select('City', 'city_id')->options($cities)->addClass('select2 city_dropdown') !!}
{!! BootForm::select('MMS', 'mms_id')->options($mms)->addClass('select2 mms_dropdown') !!}
{!! BootForm::select('Sentra', 'sentra_id')->options($sentra)->addClass('select2 sentra_dropdown') !!}
{!! BootForm::select('Community', 'community_id')->options($communities)->addClass('select2 community_dropdown') !!}
{!! BootForm::text('Customer No.', 'account_number') !!}
{!! BootForm::text('Name', 'name') !!}
{!! BootForm::text('Phone', 'phone') !!}
{!! BootForm::textarea('Address', 'address')->rows(3) !!}
<div class="form-group {!! $errors->has('is_leader') ? 'has-error' : '' !!}">
    <label for="is_leader" class="control-label">Community Leader</label>

    <div>
        {!! AdamForm::checkbox('is_leader', '1')->addClass('')->id('is_leader') !!}
    </div>
    {!! $errors->first('is_leader', '<p class="help-block">:message</p>') !!}
</div>
<div class="row">
    <div class="col-sm-12">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save</button>
    </div>
</div>