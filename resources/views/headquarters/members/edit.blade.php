{!! BootForm::open()->put()->action(route('members.update', $member))->addClass('bootstrap-modal-form')->data('confirm', 'Edit the member?') !!}
{!! BootForm::bind($member) !!}
@include('headquarters.members.partials._form')
{!! BootForm::close() !!}