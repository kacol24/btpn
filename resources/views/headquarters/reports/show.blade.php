@extends('layouts.default')

@section('page-title', 'Document Preview')

@section('content')
    @if($preview)
        <div class="row no-print">
            <div class="col-xs-12">
                {!! BootForm::open()->post()->action(route('reports.generate')) !!}
                {!! BootForm::hidden('community_id')->value($community->id) !!}
                {!! BootForm::hidden('period')->value($period->format('Y-m')) !!}
                <button class="btn btn-default" name="report_type" value="download">
                    <i class="fa fa-download"></i> Download
                </button>
                {!! BootForm::close() !!}
            </div>
        </div>
    @endif
    @include('headquarters.reports.partials._data')
@endsection