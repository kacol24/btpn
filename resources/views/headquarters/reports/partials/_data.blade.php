@unless($preview)
    <div class="row no-print">
        <div class="col-md-12">
            <button class="btn btn-default" onclick="window.print();"><i class="fa fa-download"></i> Download</button>
        </div>
    </div>
    @endunless
            <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                Monthly Report of Community Empowerment Program
                <small class="pull-right">Date: {{ Carbon::now()->format('Y-m-d') }}</small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-xs-8">
            <div class="row">
                <div class="col-xs-12">
                    <h3>Report Parameters</h3>
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered">
                            <tbody>
                            <tr>
                                <th>City</th>
                                <td>{{ $community->mms->city->name }}</td>
                            </tr>
                            <tr>
                                <th>MMS</th>
                                <td>{{ $community->mms->name }}</td>
                            </tr>
                            <tr>
                                <th>Community</th>
                                <td>{{ $community->name }}</td>
                            </tr>
                            <tr>
                                <th>Period</th>
                                <td>{{ $period->format('M. Y') }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xs-12">
                    <h3>General Information</h3>
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed">
                            <tbody>
                            <tr>
                                <th>Community Leader</th>
                                <td>{{ $community->leader()->name }}</td>
                            </tr>
                            <tr>
                                <th>Active Since</th>
                                <td>{{ $community->activation_date }}</td>
                            </tr>
                            <tr>
                                <th>Total Members</th>
                                <td>{{ $community->members->count() }}</td>
                            </tr>
                            <tr>
                                <th>Business Field</th>
                                <td>{{ $community->businessField->name }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
            <h3>Facilitator</h3>
            <figure>
                <img src="{{ asset($facilitator->picture) }}" alt="{{ $facilitator->name }}"
                     class="img-responsive img-thumbnail full-width">
                <figcaption class="text-center">
                    <strong>{{ $facilitator->name }}</strong>
                </figcaption>
            </figure>
        </div>
    </div>
    <!-- /.row -->
    <div class="page-break"></div>
    <div class="row">
        <div class="col-xs-12">
            <h3>Activity Summary</h3>
            <div class="row">
                <div class="col-sm-4">
                    <table class="table table-bordered table-condensed">
                        <tbody>
                        <?php $activity_count; ?>
                        @foreach($activity_types as $activity_type)
                            <tr>
                                <td>Total {{ $activity_type->name }}</td>
                                <td class="text-right">
                                    <?php
                                    $counter = array_where($activities->toArray(),
                                            function ($key, $value) use ($activity_type) {
                                                return $value['report']['activity_type_id'] == $activity_type->id;
                                            });
                                    $activity_count[] = count($counter);
                                    ?>
                                    {{ count($counter) }}
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <th>Total Activities</th>
                            <td class="text-right"><strong>{{ $activities->count() }}</strong></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div id="pieChart"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div id="barChart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-break"></div>
    <div class="row">
        <div class="col-xs-12">
            <h3>Activity Details</h3>
            <div class="table-responsive">
                <table class="table table-bordered table-condensed">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Date</th>
                        <th>Location</th>
                        <th>Participants</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($activities as $activity)
                        <tr>
                            <td>{{ $activity->report->activity_name }}</td>
                            <td>{{ $activity->report->activity_type->name }}</td>
                            <td>{{ $activity->period }}</td>
                            <td>{{ $activity->report->location }}</td>
                            <td>
                                <?php
                                $members = $activity->report->members;
                                $member_count = 0;
                                foreach ($members as $member) {
                                    if ($member->pivot->attended) {
                                        $member_count++;
                                    }
                                }
                                ?>
                                {{ $member_count }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3>Members</h3>
            <table class="table table-bordered table-condensed">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Customer No.</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($members as $member)
                    <tr>
                        <td>{{ $member->name }}</td>
                        <td>{{ $member->account_number }}</td>
                        <td>{{ $member->phone }}</td>
                        <td>{{ $member->address }}</td>
                        <td class="text-center">
                            @if($member->is_active)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">Inactive</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@section('footer-scripts')
    <script>
        $(function () {
            var activity_pie = {};

            activity_pie.chartSelector = '#pieChart';
            activity_pie.settings = {
                title: {
                    text: 'Activities',
                    x: -20
                },
                chart: {
                    type: 'pie'
                },
                credits: {
                    enabled: false
                },
                series: [
                    {
                        data: activities.pieData
                    }
                ]
            };
            $(activity_pie.chartSelector).highcharts(activity_pie.settings);

            var activity_bar = {};

            activity_bar.chartSelector = '#barChart';
            activity_bar.settings = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Activities',
                    x: -20
                },
                xAxis: {
                    categories: [
                        'Activities'
                    ]
                },
                credits: {
                    enabled: false
                },
                series: activities.barData
            };
            $(activity_bar.chartSelector).highcharts(activity_bar.settings);
        });
    </script>
@append