@extends('layouts.print')

@section('page-title', 'Document Preview')

@section('content')
    <style>
        .page-break {
            page-break-after: always;
        }
    </style>
    @include('headquarters.reports.partials._data')
@endsection