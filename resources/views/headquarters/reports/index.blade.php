@extends('layouts.default')

@section('page-title', 'Reports')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <?php $columnSizes = ['sm' => [4, 8]]; ?>
                    <div class="row">
                        <div class="col-sm-7">
                            {!! BootForm::openHorizontal($columnSizes)->post()->action(route('reports.generate')) !!}
                            {!! BootForm::select('City', 'city_id')->options($cities)->addClass('select2')->id('city_dropdown') !!}
                            {!! BootForm::select('MMS','mms_id')->addClass('select2')->id('mms_dropdown') !!}
                            {!! BootForm::select('Community','community_id')->addClass('select2')->id('community_dropdown') !!}
                            {!! BootForm::text('Period', 'period')->addClass('monthpicker') !!}
                            <div class="form-group">
                                <div class="col-sm-{{ $columnSizes['sm'][1] }} col-sm-offset-{{ $columnSizes['sm'][0] }}">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <button class="btn btn-default" name="report_type" value="download">
                                                <i class="fa fa-download"></i> Download
                                            </button>
                                        </div>
                                        <div class="col-xs-6">
                                            <button class="btn btn-primary pull-right" name="report_type"
                                                    value="generate"><i
                                                        class="fa fa-eye"></i> View
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! BootForm::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            BTPN.remoteChained('#community_dropdown', '#mms_dropdown', '{!! route('api.mms.communities') !!}')
            BTPN.remoteChained('#mms_dropdown', '#city_dropdown', '{!! route('api.city.mms') !!}')
        })
    </script>
@append