<table class="table table-bordered table-hover dt-responsive table-striped nowrap"
       cellspacing="0"
       width="100%">
    <thead>
    <tr>
        <th>Aspect</th>
        @foreach($levels as $level)
            <th>
                {{ $level->name }}
                </a>
            </th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($aspects as $aspect)
        <tr>
            <td>
                {{ $aspect->name }}
            </td>
            @foreach($levels as $level)
                <?php
                $value = $level->values()->where('aspect_id', $aspect->id)->first();
                ?>
                <td>
                    @if(isset($value->value) && !empty($value->value))
                        <label>
                            <input type="radio"
                                   name="value[{{ $aspect->id }}]"
                                   value="{{ $value->id }}" {{ $maturity->contains('id', $value->id) ? 'checked="checked"' : '' }}> {{ $value->value }}
                        </label>
                    @else
                        -
                    @endif
                </td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>