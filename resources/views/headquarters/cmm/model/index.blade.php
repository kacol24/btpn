@extends('layouts.default')

@section('page-title', 'Community Maturity')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! BootForm::open()->post()->action(route('cmm.model.show')) !!}
                            {!! BootForm::select('City', 'city_id')->addClass('select2')->id('city_dropdown')->options($cities)->select(isset($city_id) ? $city_id : '') !!}
                            {!! BootForm::select('MMS','mms_id')->addClass('select2')->id('mms_dropdown')->options(isset($mms) ? $mms : [])->select(isset($mms_id) ? $mms_id : '') !!}
                            {!! BootForm::select('Community', 'community_id')->addClass('select2')->id('community_dropdown')->options(isset($communities) ? $communities : [])->select(isset($community_id) ? $community_id : '') !!}
                            <button class="btn btn-default">
                                <i class="fa fa-refresh"></i> Refresh
                            </button>
                            {!! BootForm::close() !!}
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    @if(isset($aspects) && isset($levels) && isset($values))
                        {!! BootForm::open()->put()->action(route('cmm.model.sync', $community_id))->data('confirm', 'Save the maturity model?') !!}
                        @include('headquarters.cmm.model.partials._model')
                        <button type="submit" class="btn btn-primary pull-right">
                            <i class="fa fa-save"></i> Save
                        </button>
                        {!! BootForm::close() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            BTPN.remoteChained('#community_dropdown', '#mms_dropdown', '{!! route('api.mms.communities') !!}')
            BTPN.remoteChained('#mms_dropdown', '#city_dropdown', '{!! route('api.city.mms') !!}')
        })
    </script>
@append