{!! BootForm::select('Aspect', 'aspect_id')->options($aspectsDropdown)->addClass('select2') !!}
{!! BootForm::select('Level', 'level_id')->options($levelsDropdown)->addClass('select2') !!}
{!! BootForm::text('Value', 'value')->autofocus() !!}
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Save</button>