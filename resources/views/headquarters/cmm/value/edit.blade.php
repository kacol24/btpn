<div class="row margin-bottom">
    <div class="col-xs-12">
        {!! BootForm::open()->put()->action(route('cmm.value.update', $value))->addClass('bootstrap-modal-form')->data('reload', true)->data('confirm', 'Edit the maturity value?') !!}
        {!! BootForm::bind($value) !!}
        @include('headquarters.cmm.value.partials._form')
        {!! BootForm::close() !!}
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        {!! BootForm::open()->delete()->action(route('cmm.value.destroy', $value))->addClass('bootstrap-modal-form dirtyignore')->data('reload', true)->data('confirm', 'Delete the maturity value?') !!}
        <button type="submit" class="btn btn-danger">
            <i class="fa fa-trash"></i> Delete
        </button>
        {!! BootForm::close() !!}
    </div>
</div>