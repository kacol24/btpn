<div class="modal fade" id="aspect_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create Aspect</h4>
            </div>
            <div class="modal-body">
                {!! BootForm::open()->action(route('cmm.aspect.store'))->addClass('bootstrap-modal-form')->data('reload', true)->data('confirm', 'Create the aspect?') !!}
                @include('headquarters.cmm.aspect.partials._form')
                {!! BootForm::close() !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>