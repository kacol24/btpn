<div class="row margin-bottom">
    <div class="col-xs-12">
        {!! BootForm::open()->put()->action(route('cmm.aspect.update', $aspect))->addClass('bootstrap-modal-form')->data('reload', true)->data('confirm', 'Edit the aspect?') !!}
        {!! BootForm::bind($aspect) !!}
        @include('headquarters.cmm.aspect.partials._form')
        {!! BootForm::close() !!}
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        {!! BootForm::open()->delete()->action(route('cmm.aspect.destroy', $aspect))->addClass('bootstrap-modal-form dirtyignore')->data('reload', true)->data('confirm','Delete the aspect?') !!}
        <button type="submit" class="btn btn-danger">
            <i class="fa fa-trash"></i> Delete
        </button>
        {!! BootForm::close() !!}
    </div>
</div>