{!! BootForm::text('Name', 'name')->autofocus() !!}
{!! BootForm::textarea('Description', 'description')->rows(3) !!}
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
<button type="submit" class="btn btn-primary pull-right">
    <i class="fa fa-save"></i> Save
</button>