@extends('layouts.default')

@section('page-title', 'CMM')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <button class="btn btn-primary bootstrap-modal-form-open" data-toggle="modal"
                            data-target="#aspect_modal"><i
                                class="fa fa-plus-circle"></i> Aspect
                    </button>
                    <button class="btn btn-primary bootstrap-modal-form-open" data-toggle="modal"
                            data-target="#level_modal"><i
                                class="fa fa-plus-circle"></i> Level
                    </button>
                    <button class="btn btn-primary bootstrap-modal-form-open" data-toggle="modal"
                            data-target="#value_modal"><i
                                class="fa fa-plus-circle"></i> Value
                    </button>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped dt-responsive dt-simple nowrap"
                           cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Aspect</th>
                            @foreach($levels as $level)
                                <th>
                                    {{ $level->name  }} ({{ $level->score }}) <a href="{{ route('cmm.level.edit', $level) }}"
                                                          class="ajax-modal bootstrap-modal-form-open"
                                                          data-title="{{ $level->name }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($aspects as $aspect)
                            <tr>
                                <td>
                                    {{ $aspect->name }} <a href="{{ route('cmm.aspect.edit', $aspect->id) }}"
                                                           class="ajax-modal bootstrap-modal-form-open"
                                                           data-title="{{ $aspect->name }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                                @foreach($levels as $level)
                                    <?php
                                    $value = $level->values()->where('aspect_id', $aspect->id)->first();
                                    ?>
                                    <td>
                                        @if(isset($value->value) && !empty($value->value))
                                            {{ $value->value }} <a href="{{ route('cmm.value.edit', $value) }}"
                                                                   class="ajax-modal bootstrap-modal-form-open"
                                                                   data-title="{{ $value->value}}"><i
                                                        class="fa fa-edit"></i></a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    @include('headquarters.cmm.aspect.create')
    @include('headquarters.cmm.level.create')
    @include('headquarters.cmm.value.create')
@append