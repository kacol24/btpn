<div class="row margin-bottom">
    <div class="col-xs-12">
        {!! BootForm::open()->put()->action(route('cmm.level.update', $level))->addClass('bootstrap-modal-form')->data('reload', true)->data('confirm','Edit the level?') !!}
        {!! BootForm::bind($level) !!}
        @include('headquarters.cmm.level.partials._form')
        {!! BootForm::close() !!}
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        {!! BootForm::open()->delete()->action(route('cmm.level.destroy', $level))->addClass('bootstrap-modal-form dirtyignore')->data('reload', true)->data('confirm', 'Delete the level?') !!}
        <button type="submit" class="btn btn-danger">
            <i class="fa fa-trash"></i> Delete
        </button>
        {!! BootForm::close() !!}
    </div>
</div>