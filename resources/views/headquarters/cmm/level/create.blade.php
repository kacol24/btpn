<div class="modal fade" id="level_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create Level</h4>
            </div>
            <div class="modal-body">
                <div class="row margin-bottom">
                    <div class="col-xs-12">
                        {!! BootForm::open()->action(route('cmm.level.store'))->addClass('bootstrap-modal-form')->data('reload', true)->data('confirm', 'Create the level?') !!}
                        @include('headquarters.cmm.level.partials._form')
                        {!! BootForm::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>