@extends('layouts.default')

@section('page-title')
    Communities in {{ Auth::user()->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="communities_table" class="table table-bordered table-striped dt-responsive nowrap"
                           cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Community ID</th>
                            <th>Name</th>
                            <th>MMS</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            document.datatable = $('#communities_table').DataTable({
                ajax: '{!! route('mms.communities.datatables') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'mms.user.name', name: 'mms.user.name'},
                    {data: 'actions', name: 'actions', searchable: false, orderable: false}
                ]
            });
        });
    </script>
@append