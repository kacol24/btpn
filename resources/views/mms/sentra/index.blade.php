@extends('layouts.default')

@section('page-title')
    Sentra in {{ Auth::user()->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="sentra-table" class="table table-bordered table-striped dt-responsive nowrap"
                           cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Sentra ID</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Address</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(function () {
            document.datatable = $('#sentra-table').DataTable({
                ajax: '{!! route('mms.sentra.datatables') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'email', name: 'email'},
                    {data: 'address', name: 'address'}
                ]
            });
        });
    </script>
@endsection