<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset(auth()->user()->picture) }}"
                     class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <small>{{ Auth::user()->roles()->first()->display_name }}</small>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="{{ Active::controller('App\Http\Controllers\Dashboard') }}">
                <a href="{{ route('dashboard.index') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            {{--DTK Section--}}
            @role('headquarter')
            <li class="{{ Active::controller('App\Http\Controllers\Headquarters\Cities') }}">
                <a href="{{ route('cities.index') }}">
                    <i class="fa fa-globe"></i> <span>Cities</span>
                </a>
            </li>
            <li class="{{ Active::controller('App\Http\Controllers\Headquarters\Mms') }}">
                <a href="{{ route('mms.index') }}">
                    <i class="fa fa-user-plus"></i> <span>MMS</span>
                </a>
            </li>
            <li class="{{ Active::controller('App\Http\Controllers\Headquarters\Sentra') }}">
                <a href="{{ route('sentra.index') }}">
                    <i class="fa fa-sitemap"></i> <span>Sentra</span>
                </a>
            </li>
            <li class="{{ Active::route('communities.index') }}"><a href="{{ route('communities.index') }}">
                    <i class="fa fa-home"></i> <span>Communities</span>
                </a>
            </li>
            <li class="{{ Active::controller('App\Http\Controllers\Headquarters\Members') }}">
                <a href="{{ route('members.index') }}">
                    <i class="fa fa-users"></i> <span>Members</span>
                </a>
            </li>
            <li class="{{ Active::controller('App\Http\Controllers\Headquarters\Facilitators') }}">
                <a href="{{ route('facilitators.index') }}">
                    <i class="fa fa-user"></i> <span>Facilitators</span>
                </a>
            </li>
            <li class="{{ Active::controller('App\Http\Controllers\Headquarters\Verifications') }}">
                <a href="{{ route('verifications.index') }}">
                    <i class="fa fa-search"></i> <span>Verification</span>
                    @if($pendingReportsCount)
                        <span class="label label-primary pull-right">{{ $pendingReportsCount }}</span>
                    @endif
                </a>
            </li>
            <li class="{{ Active::controller('App\Http\Controllers\Headquarters\Reports') }}">
                <a href="{{ route('reports.index') }}">
                    <i class="fa fa-bar-chart"></i> <span>Reports</span>
                </a>
            </li>
            <li class="{{ Active::route('cmm.index') }}">
                <a href="{{ route('cmm.index') }}">
                    <i class="fa fa-table"></i> <span>CMM</span>
                </a>
            </li>
            <li class="{{ Active::routePattern('cmm.model.*') }}">
                <a href="{{ route('cmm.model.index') }}">
                    <i class="fa fa-line-chart"></i> <span>Community Maturity</span>
                </a>
            </li>
            @endrole

            {{--Facilitator Section--}}
            @role('facilitator')
            <li class="{{ Active::route('facilitators.reports.daily-report.index') }}">
                <a href="{{ route('facilitators.reports.daily-report.index') }}">
                    <i class="fa fa-tasks"></i> <span>Submit Daily Report</span>
                </a>
            </li>
            <li class="tree-view {{ Active::routePattern('facilitators.reports.monthly*') }}">
                <a href="#">
                    <i class="fa fa-calendar"></i> <span>Submit Monthly Report</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Active::route('facilitators.reports.monthly-report.program-update.index') }}">
                        <a href="{{ route('facilitators.reports.monthly-report.program-update.index') }}">
                            Program Update Report
                        </a>
                    </li>
                    <li class="{{ Active::route('facilitators.reports.monthly-report.member-status.index') }}">
                        <a href="{{ route('facilitators.reports.monthly-report.member-status.index') }}">
                            Member Status Report
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ Active::route('facilitators.reports.check-document-status.index') }}">
                <a href="{{ route('facilitators.reports.check-document-status.index') }}">
                    <i class="fa fa-file"></i> <span>Document Status</span>
                    @if($declinedReportsCount)
                        <span class="label label-primary pull-right">{{ $declinedReportsCount }}</span>
                    @endif
                </a>
            </li>
            <li class="{{ Active::route('facilitators.communities.index') }}">
                <a href="{{ route('facilitators.communities.index') }}">
                    <i class="fa fa-home"></i> <span>Communities</span>
                </a>
            </li>
            @endrole

            {{--MMS Section--}}
            @role('mms')
            <li class="{{ Active::controller('App\Http\Controllers\Mms\Sentra') }}">
                <a href="{{ route('mms.sentra.index') }}">
                    <i class="fa fa-sitemap"></i> <span>Sentra</span>
                </a>
            </li>
            <li class="{{ Active::controller('App\Http\Controllers\Mms\Communities') }}">
                <a href="{{ route('mms.communities.index') }}">
                    <i class="fa fa-home"></i> <span>Communities</span>
                </a>
            </li>
            <li class="treeview {{ Active::controller('App\Http\Controllers\Mms\Reports') }}">
                <a href="#">
                    <i class="fa fa-bar-chart"></i> <span>Reports</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Active::route('mms.reports.member-status.index') }}">
                        <a href="{{ route('mms.reports.member-status.index') }}">
                            Member Status Report
                        </a>
                    </li>
                    <li class="{{ Active::route('mms.reports.program-update.index') }}">
                        <a href="{{ route('mms.reports.program-update.index') }}">
                            Program Update Report
                        </a>
                    </li>
                </ul>
            </li>
            @endrole
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>