@if((isset($preview) && $preview == true) || ! isset($preview))
        <!-- Main Footer -->
<footer class="main-footer no-print">
    <!-- To the right -->
    <div class="pull-right">
        <strong>v</strong>
        {{ config('btpn.app_version') }}
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ Carbon::now()->year }} Daya Tumbuh Komunitas.</strong> All rights reserved.
</footer>
@endif