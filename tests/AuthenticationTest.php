<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthenticationTest extends TestCase
{
    use DatabaseTransactions;

    public function test_guest_being_redirected_to_login()
    {
        $this->visit('/')
             ->see('Sign In');
    }

    public function test_login_using_headquarter()
    {
        $this->visit('/auth/login')
             ->type('headquarter', 'username')
             ->type('password', 'password')
             ->press('Sign In')
             ->seePageIs('/')
             ->see('Headquarter');
    }

    public function test_wrong_password_login_using_headquarter()
    {
        $this->visit('/auth/login')
             ->type('headquarter', 'username')
             ->type('1234567890', 'password')
             ->press('Sign In')
             ->seePageIs('/auth/login');
    }
}
