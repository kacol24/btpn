# BTPN Daya Tumbuh Komunitas
BTPN DTK (Daya Tumbuh Komunitas) is a PHP web application using utilizing Laravel Framework 5.1 LTS. This software is intended to be used along with a MySQL database. BTPN DTK is a user management and reporting software for PT Bank Tabungan Pensiunan Nasional Tbk.

## System Requirements 
Because this software is built on top of Laravel PHP Framework, so all of Laravel's system requirements apply (see laravel's requirements [here](https://laravel.com/docs/5.1#installation)) with some application specific requirements. System requirements for Laravel 5.1.28 LTS:

 - PHP >= 5.5.9 
 - OpenSSL PHP Extension 
 - PDO PHP Extension 
 - Mbstring PHP Extension 
 - Tokenizer PHP Extension
 - GD Library PHP Extension
 - Fileinfo PHP Extension

## Installation
Before installation, make sure your machine has the following requirements met:

 - [Composer](https://getcomposer.org/download/)
 - [Git](https://git-scm.com/) (if you prefer to use the git method, which is recommended)
 - [Node/NPM/Gulp](https://nodejs.org/en/) (for development)
 - [Bower](http://bower.io/#install-bower) (for development)

To install the software you need to first:

 1. Download the project from the repository and extract to your web root under the folder btpn (or any of your choice, this tutorial assumes you chose folder name `btpn`)
    - If you are using the git method, instead of downloading the repository, do a `git clone https://kacol24@bitbucket.org/kacol24/btpn.git`
 2. After you extract the project (or clone), go into the folder `cd btpn` and do a `composer install --no-dev ` to fetch all the dependencies
 3. At this point, you should create your database using any method you prefer. Take note of the database name, username, and password
 4. You can now create a `.env` file, you can use the `.env.example` as a template, copy and rename it to `.env`
 5. Modify the `.env` file to fill in your database credentials on `DB_DATABASE` `DB_USERNAME` and `DB_PASSWORD` fields
 6. Change the `APP_ENV` and `APP_DEBUG` value to `production` and `false` respectively
 7. Now go to your terminal/command prompt and type in `php artisan key:generate` to generate your 32 character encryption key. This encryption key will be used to encrypt all sessions and cokies that are stored
 8. You should now migrate your database using the command `php artisan migrate`
 9. For production environment, it is recommended to cache all config and routes to speed up the runtime of the software `php artisan app:cache`
	 -- Note: if you did an update to the code after doing a cache, you need to run `php artisan app:recache`  to refresh the cache of the application
 10. If no errors occurred during the installation steps, then you are ready to run the application using `php artisan serve`or deploy to the server
	 -- If you are using `php artisan serve` on your local machine, you can visit the app (defaults to) on [localhost:8000](http://localhost:8000)

If you manage to see the login screen, congratulations! You have successfully installed the app.

## Security
If you discover any security related issues, please email [kacol.bot@gmail.com](mailto:kacol.bot@gmail.com) instead of using the issue tracker.

## License
This software is licensed specifically for PT Bank Tabungan Pensiunan Nasional Tbk. Permission is hereby granted for the `licensee` to use/modify the software/source code with no warranty from the `licensor`. Permission is not granted for the `licensee` to redistribute the software/source code without prior notice or permission from the `licensor`. Please see the `LICENSE` file distributed with the software for more information.